#include "back.h"
#include <SDL2/SDL.h>
#include <iostream>
#include <cmath>

SDL_Window * window;
SDL_Renderer * renderer;
int posX = 400;
int posY = 400;
int sizeX = 600;
int sizeY = 800;

bool InitSDL()
{
        if ( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
        {
                std::cout << " Failed to initialize SDL : " << SDL_GetError() << std::endl;
                return false;
        }

        return true;
}
bool CreateWindow()
{
        window = SDL_CreateWindow( "Server", posX, posY, sizeX, sizeY, 0 );

        if ( window == NULL )
        {
                std::cout << "Failed to create window : " << SDL_GetError();
                return false;
        }

        return true;
}
bool CreateRenderer()
{
        renderer = SDL_CreateRenderer( window, -1, 0 );

        if ( renderer == NULL )
        {
                std::cout << "Failed to create renderer : " << SDL_GetError();
                return false;
        }

        return true;
}
void SetupRenderer()
{
        // Set size of renderer to the same as window
        SDL_RenderSetLogicalSize( renderer, sizeX, sizeY );
        
        // Set color of renderer to black 
        SDL_SetRenderDrawColor( renderer, 0, 0, 0, 255 );
}

bool InitEverything()
{
        if ( !InitSDL() )
                return false;

        if ( !CreateWindow() )
                return false;

        if ( !CreateRenderer() )
                return false;

        SetupRenderer();

        return true;
}


int main( int argc, char* args[] )
{
	if (!InitEverything()) {
		return -1;
	}

	back back;

	back.setRenderer(renderer);
	back.Render();
	SDL_Delay(5000);

	return 0;
}

