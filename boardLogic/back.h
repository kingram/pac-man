#ifndef back_H
#define back_H

#include <SDL2/SDL.h>
#include <iostream>


class back {
  public:
	back();
	~back();

	void setPosX(int);
	void setPosY(int);
	void setNPIXEL(int);
	
	void Render();
	void setRenderer(SDL_Renderer *);
	void setWindow(SDL_Window *);
	void setMultiplier(int, int, int);

	int ifDot[18][21];

  private:
	int posX;
	int posY;
	int NPIXEL;
	SDL_Renderer * renderer;
	SDL_Window * window;
	SDL_Rect line1,line2,line3,line4,line5,line6,line7,line8,line9,line10;
SDL_Rect line11,line12,line13,line14,line15,line16,line17,line18,line19,line20;
SDL_Rect line21,line22,line23,line24,line25,line26,line27,line28,line29,line30;
SDL_Rect line31,line32,line33,line34,line35,line36,line37,line38,line39,line40;
SDL_Rect line41,line42,line43,line44,line45;

	SDL_Rect p1, p2, p3, p4;
	SDL_Rect d[18][21];
};

#endif
