/* block.h */

#ifndef BLOCK_H
#define BLOCK_H

#include <SDL2/SDL.h>
#include "dot.h"
#include "pwrup.h"

typedef enum {
    GHOST_HOME,
    POWER_UP,
    REG_DOT,
    REG_NODOT,
} BlockType;

typedef enum {
    UP, 
    DOWN,
    LEFT,
    RIGHT,
} direction;

typedef enum {
    BLINKY,
    PINKY,
    INKY,
    CLYDE,
    NONE,
} ghostName;

struct block {
    bool pacValid;
    bool isPwrup;
    bool isDot;

    bool visited;
    bool isVisit;

    block * up;
    block * down;
    block * right;
    block * left;

    int x;
    int y;

    dot * d;
    Pwrup * p;
};

#endif
