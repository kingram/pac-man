/* board.h */

#ifndef BOARD_H
#define BOARD_H

#include "block.h"
#include "dot.h"
#include "pwrup.h"

class Board {
    public:
	// Constructors
	Board();
	Board(const Board &);
	~Board();
	void delBoard(block *);

	block * block_r(block *, BlockType, block *, block *, block *);
	block * block_l(block *, BlockType, block *, block *, block *);
	block * block_u(block *, BlockType, block *, block *, block *);
	block * block_d(block *, BlockType, block *, block *, block *);
	block * genBlock(block *, block *, block *, block *, bool, bool, bool);

	void initBoard();

	void play();

	// Access Functions
	void setVal(block *, bool, bool, bool);
	void setDir(block *, block *, block *, block *, block *);
	void setXY(block *, int, int);

	void setScore(int);
	int getScore();

	// Utility Functions
	bool canMove(block *, direction);
	block * move(block *, direction);
	ghostName isDead();
	int getDot();
	block * getThing(ghostName);

	void renderDots(block *, bool);
	void setRenderer(SDL_Renderer *);
	void setMatrix();

	void printXY(block *);
	void getXY(block *,int&, int&);

	int	ifDot[18][21];

    private:
	block * pacStart;
	block * blinkyStart;
	block * pinkyStart;
	block * inkyStart;
	block * clydeStart;
	block * telR;
	block * telL;

	block * pacman;
	block * blinky;
	block * pinky;
	block * inky;
	block * clyde;

	int	nPwr;
	int	nDot;
	int	score;

	SDL_Renderer * renderer;
};

#endif
