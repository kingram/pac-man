#ifndef DOT_H
#define DOT_H

#include <SDL2/SDL.h>

class dot
{
	public:
		// Constructor and Deconstructor
		dot();
		dot(int, int);
		~dot();

		void setPosX(int);
		void setPosY(int);
		void setPIXEL(int);
		void setRenderer(SDL_Renderer *);
		void Render();
		void UpdateDot();
	private:
		int posX;
		int posY;
		int PIXEL;
		SDL_Rect Dot;
		SDL_Renderer * renderer;
};

#endif
