/* ghost.cpp
 *
 * This file contains method definitions for the ghost class.
 *
 */

#include "ghost.h"
#include <SDL2/SDL.h>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <ctime>

ghost::ghost()
{
	setPosX(0);
	setPosY(0);
	setPIXEL(2);

	setBodyR(255);
	setBodyG(0);
	setBodyB(0);

	setOriginalBodyR(255);
	setOriginalBodyG(0);
	setOriginalBodyB(0);

	setScleraR(255);
	setScleraG(255);
	setScleraB(255);

	setPupilR(0);
	setPupilG(0);
	setPupilB(255);

	setOriginalPupilR(0);
	setOriginalPupilG(0);
	setOriginalPupilB(255);

	setMouthR(255);
	setMouthG(165);
	setMouthB(0);

	setSpeed(0);
	setDirection(UP);
	setMode(ALIVE);
	i = 0;

	UpdateValues();
}

ghost::~ghost() {}

void ghost::setPosX(int x)
{
	posX = x;
	UpdateValues();
	return;
}

void ghost::setPosY(int y)
{
	posY = y;
	UpdateValues();
	return;
}

void ghost::setPIXEL(int p)
{
	PIXEL = p;
	UpdateValues();
	return;
}

void ghost::setBodyR(int r)
{
	bodyR = r;
	return;
}

void ghost::setBodyG(int b)
{
	bodyG = b;
	return;
}

void ghost::setBodyB(int g)
{
	bodyB = g;
	return;
}

void ghost::setOriginalBodyR(int r)
{
	originalBodyR = r;
	return;
}

void ghost::setOriginalBodyG(int g)
{
	originalBodyG = g;
	return;
}

void ghost::setOriginalBodyB(int b)
{
	originalBodyB = b;
	return;
}

void ghost::setScleraR(int r)
{
	scleraR = r;
	return;
}

void ghost::setScleraG(int g)
{
	scleraG = g;
	return;
}

void ghost::setScleraB(int b)
{
	scleraB = b;
	return;
}

void ghost::setPupilR(int r)
{
	pupilR = r;
	return;
}

void ghost::setPupilG(int g)
{
	pupilG = g;
	return;
}

void ghost::setPupilB(int b)
{
	pupilB = b;
	return;
}

void ghost::setOriginalPupilR(int r)
{
	originalPupilR = r;
	return;
}

void ghost::setOriginalPupilG(int g)
{
	originalPupilG = g;
	return;
}

void ghost::setOriginalPupilB(int b)
{
	originalPupilB = b;
	return;
}

void ghost::setMouthR(int r)
{
	mouthR = r;
	return;
}

void ghost::setMouthG(int g)
{
	mouthG = g;
	return;
}

void ghost::setMouthB(int b)
{
	mouthB = b;
	return;
}

void ghost::setSpeed(int s)
{
	speed = s;
	return;
}

void ghost::setDirection(direction dir)
{
	d = dir;
	return;
}

void ghost::setMode(ghostMode m)
{
	mode = m;
	UpdateMouth();
	return;
}

void ghost::setRenderer(SDL_Renderer * r)
{
	renderer = r;
	return;
}

void ghost::setWindow(SDL_Window * w)
{
	window = w;
	return;
}

void ghost::UpdateMouth()
{
	if (line4a.h != PIXEL && mode == EDIBLE)
	{
		line4a.x = line2.x;
		line4a.y = line2.y + 7*PIXEL;
		line4a.w = PIXEL;
		line4a.h = PIXEL;

		line5a.x = line3.x;
		line5a.y = line2.y + 6*PIXEL;
		line5a.w = 2*PIXEL;
		line5a.h = PIXEL;

		line6a.x = line5.x;
		line6a.y = line2.y + 7*PIXEL;
		line6a.w = 2*PIXEL;
		line6a.h = PIXEL;

		line7a.x = line7.x;
		line7a.y = line2.y + 6*PIXEL;
		line7a.w = 2*PIXEL;
		line7a.h = PIXEL;

		line10a.x = line9.x;
		line10a.y = line2.y + 7*PIXEL;
		line10a.w = 2*PIXEL;
		line10a.h = PIXEL;

		line11a.x = line11.x;
		line11a.y = line2.y + 6*PIXEL;
		line11a.w = 2*PIXEL;
		line11a.h = PIXEL;

		line12a.x = line13a.x;
		line12a.y = line2.y + 7*PIXEL;
		line12a.w = PIXEL;
		line12a.h = PIXEL;

		line13a.w = 0;
		line13a.h = 0;

		setBodyR(0);
		setBodyG(0);
		setBodyB(255);

		setPupilR(mouthR);
		setPupilG(mouthG);
		setPupilB(mouthB);

		pupil1.x = line5.x;
		pupil1.y = line5.y + 4*PIXEL;
		pupil2.x = line9.x;
		pupil2.y = line9.y + 5*PIXEL;
	}
	else if (line4a.h == PIXEL && (mode == ALIVE || mode == DEAD))
	{
		line4a.h = 3*PIXEL;
		line5a.h = 5*PIXEL;
		line5a.w = PIXEL;
		line6a.h = 5*PIXEL;
		line6a.w = PIXEL;
		line7a.h = 3*PIXEL;
		line7a.w = PIXEL;
		line10a.h = 3*PIXEL;
		line10.w = PIXEL;
		line11a.h = 5*PIXEL;
		line11a.w = PIXEL;
		line12a.h = 5*PIXEL;
		line13a.h = 3*PIXEL;
		line13a.w = PIXEL;
		bodyR = originalBodyR;
		bodyG = originalBodyG;
		bodyB = originalBodyB;
		pupilR = originalPupilR;
		pupilG = originalPupilG;
		pupilB = originalPupilB;
	}
	return;
}

void ghost::UpdateLegs()
{
	if (line1.h == 7*PIXEL)
	{
		line1.h = 8*PIXEL;
		line2.h = 10*PIXEL;
		line3.h = 10*PIXEL;
		line4.h = 12*PIXEL;
		line5.h = 13*PIXEL;
		line6.h = 14*PIXEL;
		line7.h = 12*PIXEL;
		line8.h = 12*PIXEL;
		line9.h = 14*PIXEL;
		line10.h = 13*PIXEL;
		line11.h = 12*PIXEL;
		line12.h = 10*PIXEL;
		line13.h = 10*PIXEL;
		line14.h = 8*PIXEL;
	}
	else
	{
		line1.h = 7*PIXEL;
		line2.h = 11*PIXEL;
		line3.h = 12*PIXEL;
		line4.h = 12*PIXEL;
		line5.h = 11*PIXEL;
		line6.h = 13*PIXEL;
		line7.h = 14*PIXEL;
		line8.h = 14*PIXEL;
		line9.h = 13*PIXEL;
		line10.h = 11*PIXEL;
		line11.h = 12*PIXEL;
		line12.h = 12*PIXEL;
		line13.h = 11*PIXEL;
		line14.h = 7*PIXEL;
	}

	return;
}

void ghost::UpdateGhost(int DELAY)
{
	if (mode == EDIBLE)
	{
		UpdateMouth();
		if (i < 4000 || (i / 100) % 2 == 0)
		{
			setBodyR(0);
			setBodyG(0);
			setBodyB(255);

			setPupilR(255);
			setPupilG(165);
			setPupilB(0);

			setMouthR(255);
			setMouthG(165);
			setMouthB(0);
		}

		else if (i < 5000)
		{
			setBodyR(255);
			setBodyG(255);
			setBodyB(255);

			setPupilR(208);
			setPupilG(62);
			setPupilB(25);

			setMouthR(208);
			setMouthG(62);
			setMouthB(25);
		}

		else
		{
			setMode(ALIVE);
		}

		i += DELAY;
	}

	if (mode == DEAD)
	{
		setBodyR(0);
		setBodyG(0);
		setBodyB(0);
		i = 0;
	}

	if (mode == GONE)
 	{
		setPIXEL(0);
		i = 0;
		return;
	}

	if (mode == ALIVE)
	{
		setBodyR(originalBodyR);
		setBodyG(originalBodyG);
		setBodyB(originalBodyB);
		i = 0;
	}


	if (d == UP)
	{
		line1.y -= speed;
		line2.y -= speed;
		line3.y -= speed;
		line4.y -= speed;
		line5.y -= speed;
		line6.y -= speed;
		line7.y -= speed;
		line8.y -= speed;
		line9.y -= speed;
		line10.y -= speed;
		line11.y -= speed;
		line12.y -= speed;
		line13.y -= speed;
		line14.y -= speed;
		if (mode == ALIVE || mode == DEAD)
		{
			line4a.x = line3.x;
			line4a.y = line3.y;
			line5a.x = line4.x;
			line5a.y = line4.y;
			line6a.x = line5.x;
			line6a.y = line5.y;
			line7a.x = line6.x;
			line7a.y = line6.y + 2*PIXEL;
			line10a.x = line9.x;
			line10a.y = line9.y + 2*PIXEL;
			line11a.x = line10.x;
			line11a.y = line10.y;
			line12a.x = line11.x;
			line12a.y = line11.y;
			line13a.x = line12.x;
			line13a.y = line12.y;
			pupil1.x = line5a.x;
			pupil1.y = line5a.y;
			pupil2.x = line11a.x;
			pupil2.y = line11a.y;
		}
		else if (mode == EDIBLE)
		{
			UpdateMouth();
			line4a.y -= speed;
			line5a.y -= speed;
			line6a.y -= speed;
			line7a.y -= speed;
			line10a.y -= speed;
			line11a.y -= speed;
			line12a.y -= speed;
			line13a.y -= speed;
			pupil1.y -= speed;
			pupil2.y -= speed;
		}
	}
	if (d == LEFT)
	{
		line1.x -= speed;
		line2.x -= speed;
		line3.x -= speed;
		line4.x -= speed;
		line5.x -= speed;
		line6.x -= speed;
		line7.x -= speed;
		line8.x -= speed;
		line9.x -= speed;
		line10.x -= speed;
		line11.x -= speed;
		line12.x -= speed;
		line13.x -= speed;
		line14.x -= speed;
		if (mode == ALIVE || mode == DEAD)
		{
			line4a.x = line2.x;
			line4a.y = line2.y + PIXEL;
			line5a.x = line3.x;
			line5a.y = line3.y + PIXEL;
			line6a.x = line4.x;
			line6a.y = line4.y + 2*PIXEL;
			line7a.x = line5.x;
			line7a.y = line5.y + 3*PIXEL;
			line10a.x = line8.x;
			line10a.y = line8.y + 4*PIXEL;
			line11a.x = line9.x;
			line11a.y = line9.y + 3*PIXEL;
			line12a.x = line10.x;
			line12a.y = line10.y + 2*PIXEL;
			line13a.x = line11.x;
			line13a.y = line11.y + 3*PIXEL;
			pupil1.x = line4a.x;
			pupil1.y = line4a.y + PIXEL;
			pupil2.x = line10a.x;
			pupil2.y = line10a.y + PIXEL;
		}
		else if (mode == EDIBLE)
		{
			line4a.x -= speed;
			line5a.x -= speed;
			line6a.x -= speed;
			line7a.x -= speed;
			line10a.x -= speed;
			line11a.x -= speed;
			line12a.x -= speed;
			line13a.x -= speed;
			pupil1.x -= speed;
			pupil2.x -= speed;
		}
	}
	if (d == DOWN)
	{
		line1.y += speed;
		line2.y += speed;
		line3.y += speed;
		line4.y += speed;
		line5.y += speed;
		line6.y += speed;
		line7.y += speed;
		line8.y += speed;
		line9.y += speed;
		line10.y += speed;
		line11.y += speed;
		line12.y += speed;
		line13.y += speed;
		line14.y += speed;
		if (mode == ALIVE || mode == DEAD)
		{
			line4a.x = line3.x;
			line4a.y = line3.y + 3*PIXEL;
			line5a.x = line4.x;
			line5a.y = line4.y + 3*PIXEL;
			line6a.x = line5.x;
			line6a.y = line5.y + 3*PIXEL;
			line7a.x = line6.x;
			line7a.y = line6.y + 5*PIXEL;
			line10a.x = line9.x;
			line10a.y = line9.y + 5*PIXEL;
			line11a.x = line10.x;
			line11a.y = line10.y + 3*PIXEL;
			line12a.x = line11.x;
			line12a.y = line11.y + 3*PIXEL;
			line13a.x = line12.x;
			line13a.y = line12.y + 3*PIXEL;
			pupil1.x = line5a.x;
			pupil1.y = line5a.y + 3*PIXEL;
			pupil2.x = line11a.x;
			pupil2.y = line11a.y + 3*PIXEL;
		}
		else if (mode == EDIBLE)
		{
			line4a.y += speed;
			line5a.y += speed;
			line6a.y += speed;
			line7a.y += speed;
			line10a.y += speed;
			line11a.y += speed;
			line12a.y += speed;
			line13a.y += speed;
			pupil1.y += speed;
			pupil2.y += speed;
		}
	}
	if (d == RIGHT)
	{
		line1.x += speed;
		line2.x += speed;
		line3.x += speed;
		line4.x += speed;
		line5.x += speed;
		line6.x += speed;
		line7.x += speed;
		line8.x += speed;
		line9.x += speed;
		line10.x += speed;
		line11.x += speed;
		line12.x += speed;
		line13.x += speed;
		line14.x += speed;
		if (mode == ALIVE || mode == DEAD)
		{
			line4a.x = line4.x;
			line4a.y = line4.y + 3*PIXEL;
			line5a.x = line5.x;
			line5a.y = line5.y + 2*PIXEL;
			line6a.x = line6.x;
			line6a.y = line6.y + 3*PIXEL;
			line7a.x = line7.x;
			line7a.y = line7.y + 4*PIXEL;
			line10a.x = line10.x;
			line10a.y = line10.y + 3*PIXEL;
			line11a.x = line11.x;
			line11a.y = line11.y + 2*PIXEL;
			line12a.x = line12.x;
			line12a.y = line12.y + PIXEL;
			line13a.x = line13.x;
			line13a.y = line13.y + PIXEL;
			pupil1.x = line6a.x;
			pupil1.y = line6a.y + 2*PIXEL;
			pupil2.x = line12a.x;
			pupil2.y = line12a.y + 2*PIXEL;
		}
		else if (mode == EDIBLE)
		{
			line4a.x += speed;
			line5a.x += speed;
			line6a.x += speed;
			line7a.x += speed;
			line10a.x += speed;
			line11a.x += speed;
			line12a.x += speed;
			line13a.x += speed;
			pupil1.x += speed;
			pupil2.x += speed;
		}
	}

	if (mode == ALIVE || mode == EDIBLE)
	{
		UpdateLegs();
	}

	return;
}

void ghost::Render()
{
	// Change color to specified body color
	SDL_SetRenderDrawColor(renderer, bodyR, bodyG, bodyB, 255);

	// Render out the ghost body
	SDL_RenderFillRect(renderer, &line1);
	SDL_RenderFillRect(renderer, &line2);
	SDL_RenderFillRect(renderer, &line3);
	SDL_RenderFillRect(renderer, &line4);
	SDL_RenderFillRect(renderer, &line5);
	SDL_RenderFillRect(renderer, &line6);
	SDL_RenderFillRect(renderer, &line7);
	SDL_RenderFillRect(renderer, &line8);
	SDL_RenderFillRect(renderer, &line9);
	SDL_RenderFillRect(renderer, &line10);
	SDL_RenderFillRect(renderer, &line11);
	SDL_RenderFillRect(renderer, &line12);
	SDL_RenderFillRect(renderer, &line13);
	SDL_RenderFillRect(renderer, &line14);

	// Change color to specified sclera/mouth color
	if (mode == EDIBLE)
	{
		SDL_SetRenderDrawColor(renderer, mouthR, mouthG, mouthB, 255);
	}
	else
	{
		SDL_SetRenderDrawColor(renderer, scleraR, scleraG, scleraB, 255);
	}

	// Render out the ghost sclera/mouth
	SDL_RenderFillRect(renderer, &line4a);
	SDL_RenderFillRect(renderer, &line5a);
	SDL_RenderFillRect(renderer, &line6a);
	SDL_RenderFillRect(renderer, &line7a);
	SDL_RenderFillRect(renderer, &line10a);
	SDL_RenderFillRect(renderer, &line11a);
	SDL_RenderFillRect(renderer, &line12a);
	SDL_RenderFillRect(renderer, &line13a);
	
	// Change color to specified pupil color
	SDL_SetRenderDrawColor(renderer, pupilR, pupilG, pupilB, 255);

	// Render out the ghost pupils
	SDL_RenderFillRect(renderer, &pupil1);
	SDL_RenderFillRect(renderer, &pupil2);

	// Change color to black
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);

	// Render the changes above
	SDL_RenderPresent(renderer);
}

void ghost::UpdateValues() // STILL IN SPACES
{
        line1.x = PIXEL + posX;
        line1.y = 7*PIXEL + posY;
        line1.w = PIXEL;
        line1.h = 8*PIXEL;

        line2.x = 2*PIXEL + posX;
        line2.y = 4*PIXEL + posY;
        line2.w = PIXEL;
        line2.h = 10*PIXEL;

        line3.x = 3*PIXEL + posX;
        line3.y = 3*PIXEL + posY;
        line3.w = PIXEL;
        line3.h = 10*PIXEL;

        line4.x = 4*PIXEL + posX;
        line4.y = 2*PIXEL + posY;
        line4.w = PIXEL;
        line4.h = 12*PIXEL;

        line5.x = 5*PIXEL + posX;
        line5.y = 2*PIXEL + posY;
        line5.w = PIXEL;
        line5.h = 13*PIXEL;

        line6.x = 6*PIXEL + posX;
        line6.y = PIXEL + posY;
        line6.w = PIXEL;
        line6.h = 14*PIXEL;

        line7.x = 7*PIXEL + posX;
        line7.y = PIXEL + posY;
        line7.w = PIXEL;
        line7.h = 12*PIXEL;

        line8.x = 8*PIXEL + posX;
        line8.y = PIXEL + posY;
        line8.w = PIXEL;
        line8.h = 12*PIXEL;

        line9.x = 9*PIXEL + posX;
        line9.y = PIXEL + posY;
        line9.w = PIXEL;
        line9.h = 14*PIXEL;

        line10.x = 10*PIXEL + posX;
        line10.y = 2*PIXEL + posY;
        line10.w = PIXEL;
        line10.h = 13*PIXEL;

        line11.x = 11*PIXEL + posX;
        line11.y = 2*PIXEL + posY;
        line11.w = PIXEL;
        line11.h = 12*PIXEL;

        line12.x = 12*PIXEL + posX;
        line12.y = 3*PIXEL + posY;
        line12.w = PIXEL;
        line12.h = 10*PIXEL;

        line13.x = 13*PIXEL + posX;
        line13.y = 4*PIXEL + posY;
        line13.w = PIXEL;
        line13.h = 10*PIXEL;

        line14.x = 14*PIXEL + posX;
        line14.y = 7*PIXEL + posY;
        line14.w = PIXEL;
        line14.h = 8*PIXEL;

        line4a.x = 4*PIXEL + posX;
        line4a.y = 5*PIXEL + posY;
        line4a.w = PIXEL;
        line4a.h = 3*PIXEL;

        line5a.x = 5*PIXEL + posX;
        line5a.y = 4*PIXEL + posY;
        line5a.w = PIXEL;
        line5a.h = 5*PIXEL;

        line6a.x = 6*PIXEL + posX;
        line6a.y = 4*PIXEL + posY;
        line6a.w = PIXEL;
        line6a.h = 5*PIXEL;

        line7a.x = 7*PIXEL + posX;
        line7a.y = 5*PIXEL + posY;
        line7a.w = PIXEL;
        line7a.h = 3*PIXEL;

        line10a.x = 10*PIXEL + posX;
        line10a.y = 5*PIXEL + posY;
        line10a.w = PIXEL;
        line10a.h = 3*PIXEL;

        line11a.x = 11*PIXEL + posX;
        line11a.y = 4*PIXEL + posY;
        line11a.w = PIXEL;
        line11a.h = 5*PIXEL;

        line12a.x = 12*PIXEL + posX;
        line12a.y = 4*PIXEL + posY;
        line12a.w = PIXEL;
        line12a.h = 5*PIXEL;

        line13a.x = 13*PIXEL + posX;
        line13a.y = 5*PIXEL + posY;
        line13a.w = PIXEL;
        line13a.h = 3*PIXEL;

        pupil1.x = 6*PIXEL + posX;
        pupil1.y = 6*PIXEL + posY;
        pupil1.w = 2*PIXEL;
        pupil1.h = 2*PIXEL;

        pupil2.x = 12*PIXEL + posX;
        pupil2.y = 6*PIXEL + posY;
        pupil2.w = 2*PIXEL;
        pupil2.h = 2*PIXEL;

	return;
}

void ghost::MoveRandom()
{
	setDirection(RandomDirection());
	return;
}

direction ghost::RandomDirection()
{
	int d = rand() % 4;
	switch (d)
	{
		case 0:
			return UP;
			break;
		case 1:
			return DOWN;
			break;
		case 2:
			return LEFT;
			break;
		case 3:
			return RIGHT;
			break;
	}

}

ghost ghost::initializeClyde(int x, int y, SDL_Renderer * renderer,
                             SDL_Window * window)
{
	ghost clyde;

	clyde.setBodyR(219);
	clyde.setBodyG(133);
	clyde.setBodyB(28);

	clyde.setOriginalBodyR(219);
	clyde.setOriginalBodyG(133);
	clyde.setOriginalBodyB(28);

	clyde.setPosX(x);
	clyde.setPosY(y);

	clyde.setRenderer(renderer);
	clyde.setWindow(window);
	clyde.Render();

	return clyde;
}

ghost ghost::initializeBlinky(int x, int y, SDL_Renderer * renderer,
                              SDL_Window * window)
{
	ghost blinky;

	blinky.setBodyR(208);
	blinky.setBodyG(62);
	blinky.setBodyB(25);

	blinky.setOriginalBodyR(208);
	blinky.setOriginalBodyG(62);
	blinky.setOriginalBodyB(25);

	blinky.setPosX(x);
	blinky.setPosY(y);

	blinky.setRenderer(renderer);
	blinky.setWindow(window);
	blinky.Render();

	return blinky;
}

ghost ghost::initializePinky(int x, int y, SDL_Renderer * renderer,
                             SDL_Window * window)
{
	ghost pinky;

	pinky.setBodyR(234);
	pinky.setBodyG(130);
	pinky.setBodyB(229);

	pinky.setOriginalBodyR(234);
	pinky.setOriginalBodyG(130);
	pinky.setOriginalBodyB(229);

	pinky.setPosX(x);
	pinky.setPosY(y);

	pinky.setRenderer(renderer);
	pinky.setWindow(window);
	pinky.Render();

	return pinky;
}

ghost ghost::initializeInky(int x, int y, SDL_Renderer * renderer,
                            SDL_Window * window)
{
	ghost inky;

	inky.setBodyR(70);
	inky.setBodyG(191);
	inky.setBodyB(238);

	inky.setOriginalBodyR(70);
	inky.setOriginalBodyG(191);
	inky.setOriginalBodyB(238);

	inky.setPosX(x);
	inky.setPosY(y);

	inky.setRenderer(renderer);
	inky.setWindow(window);
	inky.Render();

	return inky;
}

direction ghost::getDirection()
{
	return d;
}

ghostMode ghost::getMode()
{
	return mode;
}

