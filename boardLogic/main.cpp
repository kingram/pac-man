/* main.cpp */

#include <iostream>
#include <SDL2/SDL.h>
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <ctime>

#include "board.h"
#include "pac.h"
#include "back.h"
#include "ghost.h"

#define PIXEL 30
#define DELAY 60

/* GLOBAL VARIABLES */

// Window & General
int posX = 0;
int posY = 0;
int sizeX = 570;
int sizeY = 660;
int j = 0;
//int k = 0;

SDL_Window * window;
SDL_Renderer * renderer;
SDL_Renderer * pacRenderer;

/* FUNCTIONS */

bool InitEverything();
bool InitSDL();
bool CreateWindow();
bool CreateRenderer();
void SetupRenderer();

/* MAIN */

int main(int argc, char * argv[])
{
	srand(time(NULL));
	if (!InitEverything())
		return -1;

	Board board;
	board.setRenderer(renderer);
	block * pman = board.getThing(NONE);
	block * cly = board.getThing(CLYDE);
	block * pin = board.getThing(PINKY);
	block * ink = board.getThing(INKY);
	block * bli = board.getThing(BLINKY);

	back background;
	background.setWindow(window);
	background.setRenderer(renderer);
	board.setMatrix();
	for (int i = 0; i < 18; i++)
	{
		for (int j = 0; j < 21; j++)
		{
			background.setMultiplier(i, j, board.ifDot[i][j]);
		}
	}

	ghost clyde = clyde.initializeClyde(10*PIXEL, 10*PIXEL, renderer, window);
	ghost pinky = pinky.initializePinky(9*PIXEL, 10*PIXEL, renderer, window);
	ghost inky = inky.initializeInky(8*PIXEL, 10*PIXEL, renderer, window);
	ghost blinky = blinky.initializeBlinky(9*PIXEL, 9*PIXEL, renderer, window);

	pac pacman;
	pacman.setWindow(window);
	pacman.setRenderer(renderer);

	background.Render();
	board.renderDots(pman, false);
	clyde.Render();
	pinky.Render();
	inky.Render();
	blinky.Render();
	pacman.Render();

	SDL_Event e;
	bool stop = false;
	int pacX;
	int pacY;

	while (true)
	{
		if (board.isDead() != NONE)
		{
			ghost checking;
			switch (board.isDead())
			{
				case CLYDE:
					checking = clyde;
					break;
				case PINKY:
					checking = pinky;
					break;
				case INKY:
					checking = inky;
					break;
				case BLINKY:
					checking = blinky;
					break;
			}

			if (checking.getMode() == EDIBLE)
			{
				board.setScore(board.getScore() + 800);
				checking.setMode(DEAD);
			}
			else if (checking.getMode() == ALIVE)
			{
				// pacman run death sequence
				clyde.setMode(GONE);
				pinky.setMode(GONE);
				inky.setMode(GONE);
				blinky.setMode(GONE);
				pacman.death();
			}
		}

		while (SDL_PollEvent(&e) != 0)
		{
			if (e.type == SDL_QUIT)
				return 1;
			else if (e.type == SDL_KEYDOWN)
			{
				switch (e.key.keysym.sym)
				{
					case SDLK_UP:
						if (board.canMove(pman, UP))
						{
							stop = false;
							board.getXY(pman, pacX, pacY);
							pacman.setPosX(pacX);
							pacman.setPosY(pacY);	
							pacman.moveU();
						}
						else
							stop = true;
						break;

					case SDLK_DOWN:
						if (board.canMove(pman, DOWN))
						{
							stop = false;
							board.getXY(pman, pacX, pacY);
							pacman.setPosX(pacX);
							pacman.setPosY(pacY);
							pacman.moveD();
						}
						else
							stop = true;
						break;

					case SDLK_LEFT:
						if (board.canMove(pman, LEFT))
						{
							board.getXY(pman, pacX, pacY);
							pacman.setPosX(pacX);
							pacman.setPosY(pacY);
							stop = false;
							pacman.moveL();
						}
						else
							stop = true;
						break;

					case SDLK_RIGHT:
						if (board.canMove(pman, RIGHT))
						{
							board.getXY(pman, pacX, pacY);
							pacman.setPosX(pacX);
							pacman.setPosY(pacY);
							stop = false;
							pacman.moveR();
						}
						else
							stop = true;
						break;

					case SDLK_q:
						SDL_Quit();
						return 0;
						break;

					default:
						break;
				}
			}
		}

	//	board.printXY(pman);
		board.printXY(cly);
	//	board.printXY(bli);
	//	board.printXY(pin);
	//	board.printXY(ink);

		if (!board.canMove(pman, pacman.getDirection()))
			stop = true;

		if (1)
		{
			if (j == 15)
			{
				if (board.canMove(cly, clyde.getDirection()))
				{
					cly = board.move(cly, clyde.getDirection());
				}
				else
				{
					direction dir = clyde.RandomDirection();
					while (!board.canMove(cly, dir))
						dir = clyde.RandomDirection();
					cly = board.move(cly, dir);
					clyde.setDirection(dir);
				}

				direction dir = blinky.getDirection();
				while (!board.canMove(bli, dir))
					dir = blinky.RandomDirection();
				bli = board.move(bli, dir);
				blinky.setDirection(dir);

				dir = pinky.getDirection();
				while (!board.canMove(pin, dir))
					dir = pinky.RandomDirection();
				pin = board.move(pin, dir);
				pinky.setDirection(dir);

				dir = inky.getDirection();
				while (!board.canMove(ink, dir))
					dir = inky.RandomDirection();
				ink = board.move(ink, dir);
				inky.setDirection(dir);

				if (board.canMove(pman, pacman.getDirection()))
					pman = board.move(pman, pacman.getDirection());
				else {
					stop = true;	
				}
				
				j = 0;
			}

			clyde.UpdateGhost(DELAY);
			blinky.UpdateGhost(DELAY);
			pinky.UpdateGhost(DELAY);
			inky.UpdateGhost(DELAY);
			j++;

			SDL_RenderClear(renderer);
			background.Render();
			board.renderDots(pman, true);
			clyde.Render();
			blinky.Render();
			pinky.Render();
			inky.Render();

			if (!stop) {
			pacman.executeMovement();
			} else {
			pacman.stopMovement();
			}

			SDL_Delay(DELAY/2);

			if (pacman.getDirection() == UP)
				background.setMultiplier(pacman.getPosX()/30, 
							 pacman.getPosY()/30, 0);

			if (pacman.getDirection() == DOWN)
				background.setMultiplier(pacman.getPosX()/30,
							 pacman.getPosY()/30, 0);

			if (pacman.getDirection() == LEFT)
				background.setMultiplier(pacman.getPosX()/30,
							 pacman.getPosY()/30, 0);

			if (pacman.getDirection() == RIGHT)
				background.setMultiplier(pacman.getPosX()/30,
							 pacman.getPosY()/30, 0);
		}

		else
		{
			pacman.stopMovement();
		}

		if (!board.canMove(pman, pacman.getDirection()))
			stop = true;

		if (!stop)
		{
			if (j == 15)
			{
				if (board.canMove(cly, clyde.getDirection()))
				{
					cly = board.move(cly, clyde.getDirection());
				}
				else
				{
					direction dir = clyde.RandomDirection();
					while (!board.canMove(cly, dir))
						dir = clyde.RandomDirection();
					cly = board.move(cly, dir);
					clyde.setDirection(dir);
				}

				direction dir = blinky.getDirection();
				while (!board.canMove(bli, dir))
					dir = blinky.RandomDirection();
				bli = board.move(bli, dir);
				blinky.setDirection(dir);

				dir = pinky.getDirection();
				while (!board.canMove(pin, dir))
					dir = pinky.RandomDirection();
				pin = board.move(pin, dir);
				pinky.setDirection(dir);

				dir = inky.getDirection();
				while (!board.canMove(ink, dir))
					dir = inky.RandomDirection();
				ink = board.move(ink, dir);
				inky.setDirection(dir);

				if (board.canMove(pman, pacman.getDirection()))
				{
					pman = board.move(pman, pacman.getDirection());
				}
				else
				{
					stop = true;
				}
				j = 0;
			}

			clyde.UpdateGhost(DELAY);
			blinky.UpdateGhost(DELAY);
			pinky.UpdateGhost(DELAY);
			inky.UpdateGhost(DELAY);
			pacman.midMouth();
			j++;

			SDL_RenderClear(renderer);
			background.Render();
			board.renderDots(pman, false);
			clyde.Render();
			blinky.Render();
			pinky.Render();
			inky.Render();
	//		if (!stop) {
			pacman.Render();
	//		} else {
	//		pacman.stopMovement();
	//		}

			SDL_Delay(DELAY/2);

			if (pacman.getDirection() == UP)
				background.setMultiplier(pacman.getPosX()/30,
							 pacman.getPosY()/30, 0);

			if (pacman.getDirection() == DOWN)
				background.setMultiplier(pacman.getPosX()/30,
							 pacman.getPosY()/30, 0);

			if (pacman.getDirection() == LEFT)
				background.setMultiplier(pacman.getPosX()/30,
							 pacman.getPosY()/30, 0);

			if (pacman.getDirection() == RIGHT)
				background.setMultiplier(pacman.getPosX()/30,
							 pacman.getPosY()/30, 0);
		}

		else
		{
//			pacman.stopMovement();
		}
	}

	SDL_Delay(1000);

	SDL_QuitSubSystem(SDL_INIT_EVERYTHING);
	SDL_Quit();
	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(renderer);

	return 0;
}

/* FUNCTION IMPLEMENTATIONS */

bool InitEverything()
{
	if (!InitSDL())
		return false;
	if (!CreateWindow())
		return false;
	if (!CreateRenderer())
		return false;

	SetupRenderer();

	return true;
}

bool InitSDL()
{
	if (SDL_Init(SDL_INIT_EVERYTHING) == -1)
	{
		std::cout << "Failed to initialize SDL : ";
		std::cout << SDL_GetError() << std::endl;
		return false;
	}

	return true;
}

bool CreateWindow()
{
	window = SDL_CreateWindow("Pacman", posX, posY, sizeX, sizeY, 0);

	if (window == NULL)
	{
		std::cout << "Failed to create window : ";
		std::cout << SDL_GetError() << std::endl;
		return false;
	}

	return true;
}

bool CreateRenderer()
{
	renderer = SDL_CreateRenderer(window, -1, 0);

	if (renderer == NULL)
	{
		std::cout << "Failed to create renderer : ";
		std::cout << SDL_GetError() << std::endl;
		return false;
	}

	return true;
}

void SetupRenderer()
{
	SDL_RenderSetLogicalSize(renderer, sizeX, sizeY);
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
}
