/* main.cpp */

#include <iostream>
#include <SDL2/SDL.h>
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <ctime>

#include "board.h"
#include "pac.h"
#include "back.h"
#include "ghost.h"

#define NPIXEL 15*2
#define BOARDPIXEL 30
#define pacDELAY 75 // I stole this from the pac constructor (somebody confirm) 
#define ghDELAY 60

/* GLOBAL VARIABLES */

// Window & General
int posX = 0;
int posY= 0;
int sizeX = 600;
int sizeY = 800;

SDL_Window* window;
SDL_Renderer* renderer;
SDL_Renderer * pacRenderer;

/* FUNCTIONS */

bool InitEverything();
bool InitSDL();
bool CreateWindow();
bool CreateRenderer();
void SetupRenderer();

void renderAll(back, pac, Board, bool&);
void renderBg(back);
void renderPc(pac);
void renderBo(Board, bool&);
void renderGh();

void pacExecute(pac);

/* MAIN */

int main(int argc, char *argv[]) {
    Board b;
    srand(time(NULL));
    if(!InitEverything())
		return -1; // Failed to initilize

    back background;
    background.setWindow(window);
    background.setRenderer(renderer);
    background.Render();

    pac pacman;
    pacman.setWindow(window);
    pacman.setRenderer(renderer);
    pacman.Render();

    ghost clyde = clyde.initializeClyde(10*BOARDPIXEL, 10*BOARDPIXEL, renderer, window);
    ghost pinky = pinky.initializePinky(9*BOARDPIXEL, 10*BOARDPIXEL, renderer, window);
    ghost inky = inky.initializeInky(8*BOARDPIXEL, 10*BOARDPIXEL, renderer, window);
    ghost blinky = blinky.initializeBlinky(9*BOARDPIXEL, 9*BOARDPIXEL, renderer, window);

    b.setRenderer(renderer);
    block * pman = b.getThing(NONE);
    b.renderDots(pman, false);

    std::cout << "success!" << std::endl;

    SDL_Event e;
    bool stop = false;

    while(b.getDot() != 0) {
		if(b.isDead() != NONE) {
	    // Pacman has attacked one of ghosts (they are same location)
			// if isDead's ghost is in edible mode, add points
			// else kill pacman
			ghost checking;
			switch (b.isDead()) {
				case BLINKY:
					checking = blinky;
				break;
				case INKY:
					checking = inky;
				break;
				case PINKY:
					checking = pinky;
				break;
				case CLYDE:
					checking = clyde;
				break;
			}

			if (checking.getMode() == EDIBLE) {
				b.setScore(b.getScore() + 800);
			} else if (checking.getMode() == ALIVE) {
				//pacman run death sequence
				pacman.death();
			}

		}

	while(SDL_PollEvent(&e) != 0) {
	    if(e.type == SDL_QUIT) {
		return 1;
	    } else if(e.type == SDL_KEYDOWN) {
		switch(e.key.keysym.sym) {
		    case SDLK_UP:
			if(b.canMove(pman, UP)) {
			    stop = false;
			    pman = b.move(pman, UP);
			    pacman.moveU();
			} else {
			    stop = true;
			}
			break;
		    case SDLK_DOWN:
			if(b.canMove(pman, DOWN)) {
			    stop = false;
			    pman = b.move(pman, DOWN);
			    pacman.moveD();
			} else {
			    stop = true;
			}
			break;
		    case SDLK_RIGHT:
			if(b.canMove(pman, RIGHT)) {
			    stop = false;
			    pman = b.move(pman, RIGHT);
			    pacman.moveR();
			} else {
			    stop = true;
			}
			break;
		    case SDLK_LEFT:
			if(b.canMove(pman, LEFT)) {
			    stop = false;
			    pman = b.move(pman, LEFT);
			    pacman.moveL();
			} else {
			    stop = true;
			}
			break;
		    case SDLK_q:
			SDL_Quit();
			return 0;
			break;
		}
	    }
	}
	clyde.MoveRandom();
	blinky.MoveRandom();
	pinky.MoveRandom();
	inky.MoveRandom();
	
	clyde.UpdateGhost(ghDELAY);
	blinky.UpdateGhost(ghDELAY);
	pinky.UpdateGhost(ghDELAY);
	inky.UpdateGhost(ghDELAY);

	if(!stop) {
	    for(int i = 0; i != 8; i++) {
		SDL_RenderClear(renderer);
		clyde.Render();
		blinky.Render();
		pinky.Render();
		inky.Render();
		pacman.executeMovement();
		background.Render();
		SDL_Delay(ghDELAY/2);
		SDL_RenderClear(renderer);
		clyde.Render();
		blinky.Render();
		pinky.Render();
		inky.Render();
		pacman.midMouth();
		pacman.Render();
		background.Render();
		SDL_Delay(ghDELAY/2);
	    }
	} else {
	    pacman.stopMovement();
	}
    }	

    SDL_Delay(10000); 
	SDL_QuitSubSystem(SDL_INIT_EVERYTHING);
	SDL_Quit();
 	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(renderer);
    return 0;
}

/* FUNCTION IMPLEMENTATIONS */

bool InitEverything() {
	if ( !InitSDL() )
		return false;

	if ( !CreateWindow() )
		return false;

	if ( !CreateRenderer() )
		return false;

	SetupRenderer();

	return true;
}
bool InitSDL() {
	if ( SDL_Init( SDL_INIT_EVERYTHING ) == -1 ) {
		std::cout << " Failed to initialize SDL : " << SDL_GetError() << std::endl;
		return false;
	}

	return true;
}
bool CreateWindow() {
	window = SDL_CreateWindow( "Server", posX, posY, sizeX, sizeY, 0 );

	if ( window == NULL ) {
		std::cout << "Failed to create window : " << SDL_GetError();
		return false;
	}

	return true;
}
bool CreateRenderer() {
	renderer = SDL_CreateRenderer( window, -1, 0 );

	if ( renderer == NULL ) {
		std::cout << "Failed to create renderer : " << SDL_GetError();
		return false;
	}

	return true;
}
void SetupRenderer() {
	// Set size of renderer to the same as window
		SDL_RenderSetLogicalSize( renderer, sizeX, sizeY );
	// Set color of renderer to black
		SDL_SetRenderDrawColor( renderer, 0, 0, 0, 255 );
}

void renderAll(back bg, pac pm, Board bo, bool& b) {
    bg.setRenderer(renderer);
    pm.setRenderer(renderer);
    bo.setRenderer(renderer);
    block * pman = bo.getThing(NONE);

    bg.Render();
    pm.Render();
    bo.renderDots(pman, b);
    
    if(b)
	b = false;
    else
	b = true;

}

void renderBg(back bg) {
    bg.setRenderer(renderer);
    bg.Render();
}

void renderPc(pac pm) {
    pm.UpdateValues();
    pm.Render();
}

void renderBo(Board bo, bool& b) {
    block * pman = bo.getThing(NONE);
    printf("%x", pman);
    bo.renderDots(pman, b);
    if(b)
	b = false;
    else
	b = true;
}

void renderGh() {
    // No idea how to render ghosts...
}

void pacExecute(pac p, Board bo, back bg, bool& b) {
    p.UpdateValues();
    SDL_RenderClear(renderer);
    renderBg(bg);
    renderBo(bo, b);
    renderGh();
    p.Render();
    SDL_Delay(pacDELAY);

    p.midMouth();
    SDL_RenderClear(renderer);
    renderBg(bg);
    renderBo(bo, b);
    renderGh();
    p.Render();
    SDL_Delay(pacDELAY);
}
