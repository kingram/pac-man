#include "pac.h"
#include <SDL2/SDL.h>
#include <iostream>

pac::pac() {
	setPosX(9*30); 
	setPosY(16*30); 
	setPIXEL(2); 

	setDir(RIGHT); 
	defspeed = 2; 
	DELAY = 75;
	
	setSpeedx(defspeed);
	setSpeedy(0); 

//	window = SDL_CreateWindow("", posX, posY, 0,0,300);
//	setRenderer(SDL_CreateRenderer(window, -1, 0));

	UpdateValues();
}

pac::~pac() {
	SDL_DestroyWindow(window); 
	SDL_DestroyRenderer(renderer);

}

pac pac::initPac(SDL_Window * w, SDL_Renderer * r) {
    window = w;
    renderer = r;
}

void pac::setPosX(int x) {
	posX = x;
//	UpdateValues();
}

void pac::setPosY(int y) {
	posY = y;
//	UpdateValues();
}

void pac::setPIXEL(int p) {
	PIXEL = p;
//	UpdateValues();
}

void pac::setSpeedx(int xs) {
	xspeed = xs;
}

void pac::setSpeedy(int ys) {
	yspeed = ys;
}

int pac::getSpeedx() {
	return xspeed;
}

int pac::getSpeedy() {
	return yspeed;
}

void pac::setDir(direction dir) {
	d = dir;
}

void pac::setRenderer(SDL_Renderer *r) {
	renderer = r;
}

void pac::setWindow(SDL_Window *w) {
	window = w;
}

void pac::Render() {
//	SDL_SetRenderDrawColor(renderer, 127,127,127,255);
//	SDL_RenderClear(renderer);

	SDL_SetRenderDrawColor(renderer, 253, 255, 0, 255);

	SDL_RenderFillRect(renderer, &line1);
	SDL_RenderFillRect(renderer, &line2);
	SDL_RenderFillRect(renderer, &line3);
	SDL_RenderFillRect(renderer, &line4);
	SDL_RenderFillRect(renderer, &line5);
	SDL_RenderFillRect(renderer, &line6);
	SDL_RenderFillRect(renderer, &line7);
	SDL_RenderFillRect(renderer, &line8);
	SDL_RenderFillRect(renderer, &line9);
	SDL_RenderFillRect(renderer, &line10);
	SDL_RenderFillRect(renderer, &line11);
	SDL_RenderFillRect(renderer, &line12);
	SDL_RenderFillRect(renderer, &line13);

	SDL_RenderPresent(renderer);

}

void pac::moveR() {
	setDir(RIGHT);
	setSpeedx(defspeed);
	setSpeedy(0);
}

void pac::moveL() {
	setDir(LEFT);
	setSpeedx(-defspeed);
	setSpeedy(0);
}

void pac::moveU() {
	setDir(UP);
	setSpeedx(0);
	setSpeedy(-defspeed);
}

void pac::moveD() {
        setDir(DOWN);
        setSpeedx(0);
        setSpeedy(defspeed);
}

void pac::executeMovement() {
	UpdateValues();
	Render();
	SDL_Delay(DELAY);

	midMouth();
	Render();
	SDL_Delay(DELAY);

}

void pac::stopMovement() {
	xspeed = 0;
	yspeed = 0;
	UpdateValues();
	Render();
	SDL_Delay(DELAY);

}

void pac::UpdateValues() {
	posX += xspeed;
	posY += yspeed;

	switch (d) {
		case LEFT:
			line1.x = 8*PIXEL + posX;
			line1.y = PIXEL + posY;
			line1.w = -5*PIXEL;
			line1.h = PIXEL;

			line2.x = 10*PIXEL + posX;
			line2.y = 2*PIXEL + posY;
			line2.w = -9*PIXEL;
			line2.h = PIXEL;

			line3.x = 11*PIXEL + posX;
			line3.y = 3*PIXEL + posY;
			line3.w = -11*PIXEL;
			line3.h = PIXEL;

			line4.x = 11*PIXEL + posX;
			line4.y = 4*PIXEL + posY;
			line4.w = -11*PIXEL;
			line4.h = PIXEL;

			line5.x = 12*PIXEL + posX;
			line5.y = 5*PIXEL + posY;
			line5.w = -10*PIXEL;
			line5.h = PIXEL;

			line6.x = 12*PIXEL + posX;
			line6.y = 6*PIXEL + posY;
			line6.w = -7*PIXEL;
			line6.h = PIXEL;

			line7.x = 12*PIXEL + posX;
			line7.y = 7*PIXEL + posY;
			line7.w = -4*PIXEL;
			line7.h = PIXEL;

			line8.x = 12*PIXEL + posX;
			line8.y = 8*PIXEL + posY;
			line8.w = -7*PIXEL;
			line8.h = PIXEL;

			line9.x = 12*PIXEL + posX;
			line9.y = 9*PIXEL + posY;
			line9.w = -10*PIXEL;
			line9.h = PIXEL;

			line10.x = 11*PIXEL + posX;
			line10.y = 10*PIXEL + posY;
			line10.w = -11*PIXEL;
			line10.h = PIXEL;

			line11.x = 11*PIXEL + posX;
			line11.y = 11*PIXEL + posY;
			line11.w = -11*PIXEL;
			line11.h = PIXEL;

			line12.x = 10*PIXEL + posX;
			line12.y = 12*PIXEL + posY;
			line12.w = -9*PIXEL;
			line12.h = PIXEL;

			line13.x = 8*PIXEL + posX;
			line13.y = 13*PIXEL + posY;
			line13.w = -5*PIXEL;
			line13.h = PIXEL;
			break;

		case RIGHT:
			line1.x = 4*PIXEL + posX;
			line1.y = PIXEL + posY;
			line1.w = 5*PIXEL;
			line1.h = PIXEL;

			line2.x = 2*PIXEL + posX;
			line2.y = 2*PIXEL + posY;
			line2.w = 9*PIXEL;
			line2.h = PIXEL;

			line3.x = 1*PIXEL + posX;
			line3.y = 3*PIXEL + posY;
			line3.w = 11*PIXEL;
			line3.h = PIXEL;

			line4.x = 1*PIXEL + posX;
			line4.y = 4*PIXEL + posY;
			line4.w = 11*PIXEL;
			line4.h = PIXEL;

			line5.x = 0*PIXEL + posX;
			line5.y = 5*PIXEL + posY;
			line5.w = 10*PIXEL;
			line5.h = PIXEL;

			line6.x = 0*PIXEL + posX;
			line6.y = 6*PIXEL + posY;
			line6.w = 7*PIXEL;
			line6.h = PIXEL;

			line7.x = 0*PIXEL + posX;
			line7.y = 7*PIXEL + posY;
			line7.w = 4*PIXEL;
			line7.h = PIXEL;

			line8.x = 0*PIXEL + posX;
			line8.y = 8*PIXEL + posY;
			line8.w = 7*PIXEL;
			line8.h = PIXEL;

			line9.x = 0*PIXEL + posX;
			line9.y = 9*PIXEL + posY;
			line9.w = 10*PIXEL;
			line9.h = PIXEL;

			line10.x = 1*PIXEL + posX;
			line10.y = 10*PIXEL + posY;
			line10.w = 11*PIXEL;
			line10.h = PIXEL;

			line11.x = 1*PIXEL + posX;
			line11.y = 11*PIXEL + posY;
			line11.w = 11*PIXEL;
			line11.h = PIXEL;

			line12.x = 2*PIXEL + posX;
			line12.y = 12*PIXEL + posY;
			line12.w = 9*PIXEL;
			line12.h = PIXEL;

			line13.x = 4*PIXEL + posX;
			line13.y = 13*PIXEL + posY;
			line13.w = 5*PIXEL;
			line13.h = PIXEL;
			break;

		case UP:
			line1.x = 2*PIXEL + posX;
			line1.y = 5*PIXEL + posY;
			line1.w = PIXEL;
			line1.h = 5*PIXEL;

			line2.x = 3*PIXEL + posX;
			line2.y = 3*PIXEL + posY;
			line2.w = PIXEL;
			line2.h = 9*PIXEL;

			line3.x = 4*PIXEL + posX;
			line3.y = 2*PIXEL + posY;
			line3.w = PIXEL;
			line3.h = 11*PIXEL;

			line4.x = 5*PIXEL + posX;
			line4.y = 2*PIXEL + posY;
			line4.w = PIXEL;
			line4.h = 11*PIXEL;

			line5.x = 6*PIXEL + posX;
			line5.y = 4*PIXEL + posY;
			line5.w = PIXEL;
			line5.h = 10*PIXEL;

			line6.x = 7*PIXEL + posX;
			line6.y = 7*PIXEL + posY;
			line6.w = PIXEL;
			line6.h = 7*PIXEL;

			line7.x = 8*PIXEL + posX;
			line7.y = 10*PIXEL + posY;
			line7.w = PIXEL;
			line7.h = 4*PIXEL;

			line8.x = 9*PIXEL + posX;
			line8.y = 7*PIXEL + posY;
			line8.w = PIXEL;
			line8.h = 7*PIXEL;

			line9.x = 10*PIXEL + posX;
			line9.y = 4*PIXEL + posY;
			line9.w = PIXEL;
			line9.h = 10*PIXEL;

			line10.x = 11*PIXEL + posX;
			line10.y = 2*PIXEL + posY;
			line10.w = PIXEL;
			line10.h = 11*PIXEL;

			line11.x = 12*PIXEL + posX;
			line11.y = 2*PIXEL + posY;
			line11.w = PIXEL;
			line11.h = 11*PIXEL;

			line12.x = 13*PIXEL + posX;
			line12.y = 3*PIXEL + posY;
			line12.w = PIXEL;
			line12.h = 9*PIXEL;

			line13.x = 14*PIXEL + posX;
			line13.y = 5*PIXEL + posY;
			line13.w = PIXEL;
			line13.h = 5*PIXEL; 	

			break;

		case DOWN:
			line1.x = 2*PIXEL + posX;
			line1.y = 7*PIXEL + posY;
			line1.w = PIXEL;
			line1.h = -5*PIXEL;

			line2.x = 3*PIXEL + posX;
			line2.y = 9*PIXEL + posY;
			line2.w = PIXEL;
			line2.h = -9*PIXEL;

			line3.x = 4*PIXEL + posX;
			line3.y = 10*PIXEL + posY;
			line3.w = PIXEL;
			line3.h = -11*PIXEL;

			line4.x = 5*PIXEL + posX;
			line4.y = 10*PIXEL + posY;
			line4.w = PIXEL;
			line4.h = -11*PIXEL;

			line5.x = 6*PIXEL + posX;
			line5.y = 8*PIXEL + posY;
			line5.w = PIXEL;
			line5.h = -10*PIXEL;

			line6.x = 7*PIXEL + posX;
			line6.y = 5*PIXEL + posY;
			line6.w = PIXEL;
			line6.h = -7*PIXEL;

			line7.x = 8*PIXEL + posX;
			line7.y = 2*PIXEL + posY;
			line7.w = PIXEL;
			line7.h = -4*PIXEL;

			line8.x = 9*PIXEL + posX;
			line8.y = 5*PIXEL + posY;
			line8.w = PIXEL;
			line8.h = -7*PIXEL;

			line9.x = 10*PIXEL + posX;
			line9.y = 8*PIXEL + posY;
			line9.w = PIXEL;
			line9.h = -10*PIXEL;

			line10.x = 11*PIXEL + posX;
			line10.y = 10*PIXEL + posY;
			line10.w = PIXEL;
			line10.h = -11*PIXEL;

			line11.x = 12*PIXEL + posX;
			line11.y = 10*PIXEL + posY;
			line11.w = PIXEL;
			line11.h = -11*PIXEL;

			line12.x = 13*PIXEL + posX;
			line12.y = 9*PIXEL + posY;
			line12.w = PIXEL;
			line12.h = -9*PIXEL;

			line13.x = 14*PIXEL + posX;
			line13.y = 7*PIXEL + posY;
			line13.w = PIXEL;
			line13.h = -5*PIXEL;
			break;

	}
}

void pac::midMouth() {
	posX += xspeed;
	posY += yspeed;

	switch (d) {
		case RIGHT:
			line1.x = 6*PIXEL + posX;
			line1.y = PIXEL + posY;
			line1.w = 5*PIXEL;
			line1.h = PIXEL;

			line2.x = 4*PIXEL + posX;
			line2.y = 2*PIXEL + posY;
			line2.w = 9*PIXEL;
			line2.h = PIXEL;

			line3.x = 3*PIXEL + posX;
			line3.y = 3*PIXEL + posY;
			line3.w = 11*PIXEL;
			line3.h = PIXEL;

			line4.x = 3*PIXEL + posX;
			line4.y = 4*PIXEL + posY;
			line4.w = 11*PIXEL;
			line4.h = PIXEL;

			line5.x = 2*PIXEL + posX;
			line5.y = 5*PIXEL + posY;
			line5.w = 12*PIXEL;
			line5.h = PIXEL;

			line6.x = 2*PIXEL + posX;
			line6.y = 6*PIXEL + posY;
			line6.w = 10*PIXEL;
			line6.h = PIXEL;

			line7.x = 2*PIXEL + posX;
			line7.y = 7*PIXEL + posY;
			line7.w = 4*PIXEL;
			line7.h = PIXEL;

			line8.x = 2*PIXEL + posX;
			line8.y = 8*PIXEL + posY;
			line8.w = 10*PIXEL;
			line8.h = PIXEL;

			line9.x = 2*PIXEL + posX;
			line9.y = 9*PIXEL + posY;
			line9.w = 12*PIXEL;
			line9.h = PIXEL;

			line10.x = 3*PIXEL + posX;
			line10.y = 10*PIXEL + posY;
			line10.w = 11*PIXEL;
			line10.h = PIXEL;

			line11.x = 3*PIXEL + posX;
			line11.y = 11*PIXEL + posY;
			line11.w = 11*PIXEL;
			line11.h = PIXEL;

			line12.x = 4*PIXEL + posX;
			line12.y = 12*PIXEL + posY;
			line12.w = 9*PIXEL;
			line12.h = PIXEL;

			line13.x = 6*PIXEL + posX;
			line13.y = 13*PIXEL + posY;
			line13.w = 5*PIXEL;
			line13.h = PIXEL;
			break;

		case LEFT:
			line1.x = 8*PIXEL + posX;
			line1.y = PIXEL + posY;
			line1.w = -5*PIXEL;
			line1.h = PIXEL;

			line2.x = 10*PIXEL + posX;
			line2.y = 2*PIXEL + posY;
			line2.w = -9*PIXEL;
			line2.h = PIXEL;

			line3.x = 11*PIXEL + posX;
			line3.y = 3*PIXEL + posY;
			line3.w = -11*PIXEL;
			line3.h = PIXEL;

			line4.x = 11*PIXEL + posX;
			line4.y = 4*PIXEL + posY;
			line4.w = -11*PIXEL;
			line4.h = PIXEL;

			line5.x = 12*PIXEL + posX;
			line5.y = 5*PIXEL + posY;
			line5.w = -12*PIXEL;
			line5.h = PIXEL;

			line6.x = 12*PIXEL + posX;
			line6.y = 6*PIXEL + posY;
			line6.w = -10*PIXEL;
			line6.h = PIXEL;

			line7.x = 12*PIXEL + posX;
			line7.y = 7*PIXEL + posY;
			line7.w = -4*PIXEL;
			line7.h = PIXEL;

			line8.x = 12*PIXEL + posX;
			line8.y = 8*PIXEL + posY;
			line8.w = -10*PIXEL;
			line8.h = PIXEL;

			line9.x = 12*PIXEL + posX;
			line9.y = 9*PIXEL + posY;
			line9.w = -12*PIXEL;
			line9.h = PIXEL;

			line10.x = 11*PIXEL + posX;
			line10.y = 10*PIXEL + posY;
			line10.w = -11*PIXEL;
			line10.h = PIXEL;

			line11.x = 11*PIXEL + posX;
			line11.y = 11*PIXEL + posY;
			line11.w = -11*PIXEL;
			line11.h = PIXEL;

			line12.x = 10*PIXEL + posX;
			line12.y = 12*PIXEL + posY;
			line12.w = -9*PIXEL;
			line12.h = PIXEL;

			line13.x = 8*PIXEL + posX;
			line13.y = 13*PIXEL + posY;
			line13.w = -5*PIXEL;
			line13.h = PIXEL;

			break;

		case UP:
			line1.x = 2*PIXEL + posX;
			line1.y = 5*PIXEL + posY;
			line1.w = PIXEL;
			line1.h = 5*PIXEL;

			line2.x = 3*PIXEL + posX;
			line2.y = 3*PIXEL + posY;
			line2.w = PIXEL;
			line2.h = 9*PIXEL;

			line3.x = 4*PIXEL + posX;
			line3.y = 2*PIXEL + posY;
			line3.w = PIXEL;
			line3.h = 11*PIXEL;

			line4.x = 5*PIXEL + posX;
			line4.y = 2*PIXEL + posY;
			line4.w = PIXEL;
			line4.h = 11*PIXEL;

			line5.x = 6*PIXEL + posX;
			line5.y = 2*PIXEL + posY;
			line5.w = PIXEL;
			line5.h = 12*PIXEL;

			line6.x = 7*PIXEL + posX;
			line6.y = 4*PIXEL + posY;
			line6.w = PIXEL;
			line6.h = 10*PIXEL;

			line7.x = 8*PIXEL + posX;
			line7.y = 10*PIXEL + posY;
			line7.w = PIXEL;
			line7.h = 4*PIXEL;

			line8.x = 9*PIXEL + posX;
			line8.y = 4*PIXEL + posY;
			line8.w = PIXEL;
			line8.h = 10*PIXEL;

			line9.x = 10*PIXEL + posX;
			line9.y = 2*PIXEL + posY;
			line9.w = PIXEL;
			line9.h = 12*PIXEL;

			line10.x = 11*PIXEL + posX;
			line10.y = 2*PIXEL + posY;
			line10.w = PIXEL;
			line10.h = 11*PIXEL;

			line11.x = 12*PIXEL + posX;
			line11.y = 2*PIXEL + posY;
			line11.w = PIXEL;
			line11.h = 11*PIXEL;

			line12.x = 13*PIXEL + posX;
			line12.y = 3*PIXEL + posY;
			line12.w = PIXEL;
			line12.h = 9*PIXEL;

			line13.x = 14*PIXEL + posX;
			line13.y = 5*PIXEL + posY;
			line13.w = PIXEL;
			line13.h = 5*PIXEL;
			break;

		case DOWN:
			line1.x = 2*PIXEL + posX;
			line1.y = 7*PIXEL + posY;
			line1.w = PIXEL;
			line1.h = -5*PIXEL;

			line2.x = 3*PIXEL + posX;
			line2.y = 9*PIXEL + posY;
			line2.w = PIXEL;
			line2.h = -9*PIXEL;

			line3.x = 4*PIXEL + posX;
			line3.y = 10*PIXEL + posY;
			line3.w = PIXEL;
			line3.h = -11*PIXEL;

			line4.x = 5*PIXEL + posX;
			line4.y = 10*PIXEL + posY;
			line4.w = PIXEL;
			line4.h = -11*PIXEL;

			line5.x = 6*PIXEL + posX;
			line5.y = 10*PIXEL + posY;
			line5.w = PIXEL;
			line5.h = -12*PIXEL;

			line6.x = 7*PIXEL + posX;
			line6.y = 8*PIXEL + posY;
			line6.w = PIXEL;
			line6.h = -10*PIXEL;

			line7.x = 8*PIXEL + posX;
			line7.y = 2*PIXEL + posY;
			line7.w = PIXEL;
			line7.h = -4*PIXEL;

			line8.x = 9*PIXEL + posX;
			line8.y = 8*PIXEL + posY;
			line8.w = PIXEL;
			line8.h = -10*PIXEL;

			line9.x = 10*PIXEL + posX;
			line9.y = 10*PIXEL + posY;
			line9.w = PIXEL;
			line9.h = -12*PIXEL;

			line10.x = 11*PIXEL + posX;
			line10.y = 10*PIXEL + posY;
			line10.w = PIXEL;
			line10.h = -11*PIXEL;

			line11.x = 12*PIXEL + posX;
			line11.y = 10*PIXEL + posY;
			line11.w = PIXEL;
			line11.h = -11*PIXEL;

			line12.x = 13*PIXEL + posX;
			line12.y = 9*PIXEL + posY;
			line12.w = PIXEL;
			line12.h = -9*PIXEL;

			line13.x = 14*PIXEL + posX;
			line13.y = 7*PIXEL + posY;
			line13.w = PIXEL;
			line13.h = -5*PIXEL; 
			break;
	}


}

void pac::death() {

	//death1
	line1.x = 2*PIXEL + posX;
	line1.y = 5*PIXEL + posY;
	line1.w = PIXEL;
	line1.h = 5*PIXEL;

	line2.x = 3*PIXEL + posX;
	line2.y = 3*PIXEL + posY;
	line2.w = PIXEL;
	line2.h = 9*PIXEL;

	line3.x = 4*PIXEL + posX;
	line3.y = 2*PIXEL + posY;
	line3.w = PIXEL;
	line3.h = 11*PIXEL;

	line4.x = 5*PIXEL + posX;
	line4.y = 2*PIXEL + posY;
	line4.w = PIXEL;
	line4.h = 11*PIXEL;

	line5.x = 6*PIXEL + posX;
	line5.y = 4*PIXEL + posY;
	line5.w = PIXEL;
	line5.h = 10*PIXEL;

	line6.x = 7*PIXEL + posX;
	line6.y = 7*PIXEL + posY;	Render();
	SDL_Delay(DELAY);
	line6.w = PIXEL;
	line6.h = 7*PIXEL;

	line7.x = 8*PIXEL + posX;
	line7.y = 10*PIXEL + posY;
	line7.w = PIXEL;
	line7.h = 4*PIXEL;

	line8.x = 9*PIXEL + posX;
	line8.y = 7*PIXEL + posY;
	line8.w = PIXEL;
	line8.h = 7*PIXEL;

	line9.x = 10*PIXEL + posX;
	line9.y = 4*PIXEL + posY;
	line9.w = PIXEL;
	line9.h = 10*PIXEL;

	line10.x = 11*PIXEL + posX;
	line10.y = 2*PIXEL + posY;
	line10.w = PIXEL;
	line10.h = 11*PIXEL;

	line11.x = 12*PIXEL + posX;
	line11.y = 2*PIXEL + posY;
	line11.w = PIXEL;
	line11.h = 11*PIXEL;

	line12.x = 13*PIXEL + posX;
	line12.y = 3*PIXEL + posY;
	line12.w = PIXEL;
	line12.h = 9*PIXEL;

	line13.x = 14*PIXEL + posX;
	line13.y = 5*PIXEL + posY;
	line13.w = PIXEL;
	line13.h = 5*PIXEL; 

	Render();
	SDL_Delay(deathDELAY);

	//death2
	line1.x = 2*PIXEL + posX;
	line1.y = 5*PIXEL + posY;
	line1.w = PIXEL;
	line1.h = 5*PIXEL;

	line2.x = 3*PIXEL + posX;
	line2.y = 5*PIXEL + posY;
	line2.w = PIXEL;
	line2.h = 7*PIXEL;

	line3.x = 4*PIXEL + posX;
	line3.y = 6*PIXEL + posY;
	line3.w = PIXEL;
	line3.h = 7*PIXEL;

	line4.x = 5*PIXEL + posX;
	line4.y = 7*PIXEL + posY;
	line4.w = PIXEL;
	line4.h = 6*PIXEL;

	line5.x = 6*PIXEL + posX;
	line5.y = 8*PIXEL + posY;
	line5.w = PIXEL;
	line5.h = 6*PIXEL;

	line6.x = 7*PIXEL + posX;
	line6.y = 9*PIXEL + posY;
	line6.w = PIXEL;
	line6.h = 5*PIXEL;

	line7.x = 8*PIXEL + posX;
	line7.y = 10*PIXEL + posY;
	line7.w = PIXEL;
	line7.h = 4*PIXEL;

	line8.x = 9*PIXEL + posX;
	line8.y = 9*PIXEL + posY;
	line8.w = PIXEL;
	line8.h = 5*PIXEL;

	line9.x = 10*PIXEL + posX;
	line9.y = 8*PIXEL + posY;
	line9.w = PIXEL;
	line9.h = 6*PIXEL;

	line10.x = 11*PIXEL + posX;
	line10.y = 7*PIXEL + posY;
	line10.w = PIXEL;
	line10.h = 6*PIXEL;

	line11.x = 12*PIXEL + posX;
	line11.y = 6*PIXEL + posY;
	line11.w = PIXEL;
	line11.h = 7*PIXEL;

	line12.x = 13*PIXEL + posX;
	line12.y = 5*PIXEL + posY;
	line12.w = PIXEL;
	line12.h = 7*PIXEL;

	line13.x = 14*PIXEL + posX;
	line13.y = 5*PIXEL + posY;
	line13.w = PIXEL;
	line13.h = 5*PIXEL; 

	Render();
	SDL_Delay(deathDELAY);

	//death3
	line1.x = 2*PIXEL + posX;
	line1.y = 7*PIXEL + posY;
	line1.w = PIXEL;
	line1.h = 3*PIXEL;

	line2.x = 3*PIXEL + posX;
	line2.y = 7*PIXEL + posY;
	line2.w = PIXEL;
	line2.h = 5*PIXEL;

	line3.x = 4*PIXEL + posX;
	line3.y = 8*PIXEL + posY;
	line3.w = PIXEL;
	line3.h = 5*PIXEL;

	line4.x = 5*PIXEL + posX;
	line4.y = 8*PIXEL + posY;
	line4.w = PIXEL;
	line4.h = 5*PIXEL;

	line5.x = 6*PIXEL + posX;
	line5.y = 9*PIXEL + posY;
	line5.w = PIXEL;
	line5.h = 5*PIXEL;

	line6.x = 7*PIXEL + posX;
	line6.y = 9*PIXEL + posY;
	line6.w = PIXEL;
	line6.h = 5*PIXEL;

	line7.x = 8*PIXEL + posX;
	line7.y = 10*PIXEL + posY;
	line7.w = PIXEL;
	line7.h = 4*PIXEL;

	line8.x = 9*PIXEL + posX;
	line8.y = 9*PIXEL + posY;
	line8.w = PIXEL;
	line8.h = 5*PIXEL;

	line9.x = 10*PIXEL + posX;
	line9.y = 9*PIXEL + posY;
	line9.w = PIXEL;
	line9.h = 5*PIXEL;

	line10.x = 11*PIXEL + posX;
	line10.y = 8*PIXEL + posY;
	line10.w = PIXEL;
	line10.h = 5*PIXEL;

	line11.x = 12*PIXEL + posX;
	line11.y = 8*PIXEL + posY;
	line11.w = PIXEL;
	line11.h = 5*PIXEL;

	line12.x = 13*PIXEL + posX;
	line12.y = 7*PIXEL + posY;
	line12.w = PIXEL;
	line12.h = 5*PIXEL;

	line13.x = 14*PIXEL + posX;
	line13.y = 7*PIXEL + posY;
	line13.w = PIXEL;
	line13.h = 3*PIXEL; 

	Render();
	SDL_Delay(deathDELAY);

	//death4
	line1.x = 2*PIXEL + posX;
	line1.y = 10*PIXEL + posY;
	line1.w = PIXEL;
	line1.h = 0*PIXEL;

	line2.x = 3*PIXEL + posX;
	line2.y = 10*PIXEL + posY;
	line2.w = PIXEL;
	line2.h = 2*PIXEL;

	line3.x = 4*PIXEL + posX;
	line3.y = 10*PIXEL + posY;
	line3.w = PIXEL;
	line3.h = 3*PIXEL;

	line4.x = 5*PIXEL + posX;
	line4.y = 10*PIXEL + posY;
	line4.w = PIXEL;
	line4.h = 3*PIXEL;

	line5.x = 6*PIXEL + posX;
	line5.y = 10*PIXEL + posY;
	line5.w = PIXEL;
	line5.h = 4*PIXEL;

	line6.x = 7*PIXEL + posX;
	line6.y = 10*PIXEL + posY;
	line6.w = PIXEL;
	line6.h = 4*PIXEL;

	line7.x = 8*PIXEL + posX;
	line7.y = 10*PIXEL + posY;
	line7.w = PIXEL;
	line7.h = 4*PIXEL;

	line8.x = 9*PIXEL + posX;
	line8.y = 10*PIXEL + posY;
	line8.w = PIXEL;
	line8.h = 4*PIXEL;

	line9.x = 10*PIXEL + posX;
	line9.y = 10*PIXEL + posY;
	line9.w = PIXEL;
	line9.h = 4*PIXEL;

	line10.x = 11*PIXEL + posX;
	line10.y = 10*PIXEL + posY;
	line10.w = PIXEL;
	line10.h = 3*PIXEL;

	line11.x = 12*PIXEL + posX;
	line11.y = 10*PIXEL + posY;
	line11.w = PIXEL;
	line11.h = 3*PIXEL;

	line12.x = 13*PIXEL + posX;
	line12.y = 10*PIXEL + posY;
	line12.w = PIXEL;
	line12.h = 2*PIXEL;

	line13.x = 14*PIXEL + posX;
	line13.y = 10*PIXEL + posY;
	line13.w = PIXEL;
	line13.h = 0*PIXEL;

	Render();
	SDL_Delay(deathDELAY);

	//death5
	line1.x = 2*PIXEL + posX;
	line1.y = 10*PIXEL + posY;
	line1.w = PIXEL;
	line1.h = 0*PIXEL;

	line2.x = 3*PIXEL + posX;
	line2.y = 10*PIXEL + posY;
	line2.w = PIXEL;
	line2.h = 0*PIXEL;

	line3.x = 4*PIXEL + posX;
	line3.y = 10*PIXEL + posY;
	line3.w = PIXEL;
	line3.h = 0*PIXEL;

	line4.x = 5*PIXEL + posX;
	line4.y = 13*PIXEL + posY;
	line4.w = PIXEL;
	line4.h = 0*PIXEL;

	line5.x = 6*PIXEL + posX;
	line5.y = 12*PIXEL + posY;
	line5.w = PIXEL;
	line5.h = 2*PIXEL;

	line6.x = 7*PIXEL + posX;
	line6.y = 11*PIXEL + posY;
	line6.w = PIXEL;
	line6.h = 3*PIXEL;

	line7.x = 8*PIXEL + posX;
	line7.y = 10*PIXEL + posY;
	line7.w = PIXEL;
	line7.h = 4*PIXEL;

	line8.x = 9*PIXEL + posX;
	line8.y = 11*PIXEL + posY;
	line8.w = PIXEL;
	line8.h = 3*PIXEL;

	line9.x = 10*PIXEL + posX;
	line9.y = 12*PIXEL + posY;
	line9.w = PIXEL;
	line9.h = 2*PIXEL;

	line10.x = 11*PIXEL + posX;
	line10.y = 13*PIXEL + posY;
	line10.w = PIXEL;
	line10.h = 0*PIXEL;

	line11.x = 12*PIXEL + posX;
	line11.y = 10*PIXEL + posY;
	line11.w = PIXEL;
	line11.h = 0*PIXEL;

	line12.x = 13*PIXEL + posX;
	line12.y = 10*PIXEL + posY;
	line12.w = PIXEL;
	line12.h = 0*PIXEL;

	line13.x = 14*PIXEL + posX;
	line13.y = 10*PIXEL + posY;
	line13.w = PIXEL;
	line13.h = 0*PIXEL; 

	Render();
	SDL_Delay(deathDELAY);

	//death6
	line1.x = 2*PIXEL + posX;
	line1.y = 10*PIXEL + posY;
	line1.w = PIXEL;
	line1.h = 0*PIXEL;

	line2.x = 3*PIXEL + posX;
	line2.y = 10*PIXEL + posY;
	line2.w = PIXEL;
	line2.h = 0*PIXEL;

	line3.x = 4*PIXEL + posX;
	line3.y = 10*PIXEL + posY;
	line3.w = PIXEL;
	line3.h = 0*PIXEL;

	line4.x = 5*PIXEL + posX;
	line4.y = 13*PIXEL + posY;
	line4.w = PIXEL;
	line4.h = 0*PIXEL;

	line5.x = 6*PIXEL + posX;
	line5.y = 12*PIXEL + posY;
	line5.w = PIXEL;
	line5.h = 0*PIXEL;

	line6.x = 7*PIXEL + posX;
	line6.y = 11*PIXEL + posY;
	line6.w = PIXEL;
	line6.h = 0*PIXEL;

	line7.x = 8*PIXEL + posX;
	line7.y = 10*PIXEL + posY;
	line7.w = PIXEL;
	line7.h = 4*PIXEL;

	line8.x = 9*PIXEL + posX;
	line8.y = 11*PIXEL + posY;
	line8.w = PIXEL;
	line8.h = 0*PIXEL;

	line9.x = 10*PIXEL + posX;
	line9.y = 12*PIXEL + posY;
	line9.w = PIXEL;
	line9.h = 0*PIXEL;

	line10.x = 11*PIXEL + posX;
	line10.y = 13*PIXEL + posY;
	line10.w = PIXEL;
	line10.h = 0*PIXEL;

	line11.x = 12*PIXEL + posX;
	line11.y = 10*PIXEL + posY;
	line11.w = PIXEL;
	line11.h = 0*PIXEL;

	line12.x = 13*PIXEL + posX;
	line12.y = 10*PIXEL + posY;
	line12.w = PIXEL;
	line12.h = 0*PIXEL;

	line13.x = 14*PIXEL + posX;
	line13.y = 10*PIXEL + posY;
	line13.w = PIXEL;
	line13.h = 0*PIXEL;

	Render();
	SDL_Delay(deathDELAY);

	//death7
	int centerx = 8*PIXEL + posX;
	int centery = 10*PIXEL + posY;

	line1.x = centerx + 2*PIXEL;
	line1.y = centery - 2*PIXEL;
	line1.w = PIXEL;
	line1.h = PIXEL;

	line2.x = centerx + 3*PIXEL;
	line2.y = centery + 3*PIXEL;
	line2.w = PIXEL;
	line2.h = PIXEL;

	line3.x = centerx + 2*PIXEL;
	line3.y = centery + 2*PIXEL;
	line3.w = PIXEL;
	line3.h = PIXEL;

	line4.x = centerx + PIXEL;
	line4.y = centery - PIXEL;
	line4.w = PIXEL;
	line4.h = PIXEL;

	line5.x = centerx - PIXEL;
	line5.y = centery + PIXEL;
	line5.w = PIXEL;
	line5.h = 1*PIXEL;

	line6.x = centerx + PIXEL;
	line6.y = centery + PIXEL;
	line6.w = PIXEL;
	line6.h = PIXEL;

	line7.x = centerx - PIXEL; 
	line7.y = centery - PIXEL;
	line7.w = PIXEL;
	line7.h = PIXEL;

	line8.x = centerx + 3*PIXEL;
	line8.y = centery - 3*PIXEL; 
	line8.w = PIXEL;
	line8.h = 1*PIXEL;

	line9.x = centerx - 2*PIXEL;
	line9.y = centery + 2*PIXEL; 
	line9.w = PIXEL;
	line9.h = 1*PIXEL;

	line10.x = centerx - 3*PIXEL;
	line10.y = centery + 3*PIXEL; 
	line10.w = PIXEL;
	line10.h = 1*PIXEL;

	line11.x = centerx - 2*PIXEL;
	line11.y = centery - 2*PIXEL; 
	line11.w = PIXEL;
	line11.h = 1*PIXEL;

	line12.x = centerx - 3*PIXEL;
	line12.y = centery - 3*PIXEL; 
	line12.w = PIXEL;
	line12.h = 1*PIXEL;

	line13.x = 14*PIXEL + posX;
	line13.y = 5*PIXEL + posY;
	line13.w = PIXEL;
	line13.h = 0*PIXEL; 


	Render();
	SDL_Delay(deathDELAY);

}

/*
   void pac::endMouth() {
   posX += xspeed;
   posY += yspeed;

   switch (d) {
   case RIGHT:
   line1.x = 6*PIXEL + posX;
   line1.y = PIXEL + posY;
   line1.w = 5*PIXEL;
   line1.h = PIXEL;

   line2.x = 4*PIXEL + posX;
   line2.y = 2*PIXEL + posY;
   line2.w = 9*PIXEL;
   line2.h = PIXEL;

   line3.x = 3*PIXEL + posX;
   line3.y = 3*PIXEL + posY;
   line3.w = 11*PIXEL;
   line3.h = PIXEL;

   line4.x = 3*PIXEL + posX;
   line4.y = 4*PIXEL + posY;
   line4.w = 11*PIXEL;
   line4.h = PIXEL;

   line5.x = 2*PIXEL + posX;
   line5.y = 5*PIXEL + posY;
   line5.w = 13*PIXEL;
   line5.h = PIXEL;

   line6.x = 2*PIXEL + posX;
   line6.y = 6*PIXEL + posY;
   line6.w = 13*PIXEL;
   line6.h = PIXEL;

   line7.x = 2*PIXEL + posX;
   line7.y = 7*PIXEL + posY;
   line7.w = 13*PIXEL;
   line7.h = PIXEL;

   line8.x = 2*PIXEL + posX;
   line8.y = 8*PIXEL + posY;
   line8.w = 13*PIXEL;
   line8.h = PIXEL;

   line9.x = 2*PIXEL + posX;
   line9.y = 9*PIXEL + posY;
   line9.w = 13*PIXEL;
   line9.h = PIXEL;

   line10.x = 3*PIXEL + posX;
   line10.y = 10*PIXEL + posY;
   line10.w = 11*PIXEL;
   line10.h = PIXEL;

   line11.x = 3*PIXEL + posX;
   line11.y = 11*PIXEL + posY;
   line11.w = 11*PIXEL;
   line11.h = PIXEL;

   line12.x = 4*PIXEL + posX;
   line12.y = 12*PIXEL + posY;
   line12.w = 9*PIXEL;
   line12.h = PIXEL;

   line13.x = 6*PIXEL + posX;
   line13.y = 13*PIXEL + posY;
   line13.w = 5*PIXEL;
   line13.h = PIXEL;
break;

case LEFT:
line1.x = 8*PIXEL + posX;
line1.y = PIXEL + posY;
line1.w = -5*PIXEL;
line1.h = PIXEL;

line2.x = 10*PIXEL + posX;
line2.y = 2*PIXEL + posY;
line2.w = -9*PIXEL;
line2.h = PIXEL;

line3.x = 11*PIXEL + posX;
line3.y = 3*PIXEL + posY;
line3.w = -11*PIXEL;
line3.h = PIXEL;

line4.x = 11*PIXEL + posX;
line4.y = 4*PIXEL + posY;
line4.w = -11*PIXEL;
line4.h = PIXEL;

line5.x = 12*PIXEL + posX;
line5.y = 5*PIXEL + posY;
line5.w = -13*PIXEL;
line5.h = PIXEL;

line6.x = 12*PIXEL + posX;
line6.y = 6*PIXEL + posY;
line6.w = -13*PIXEL;
line6.h = PIXEL;

line7.x = 12*PIXEL + posX;
line7.y = 7*PIXEL + posY;
line7.w = -13*PIXEL;
line7.h = PIXEL;

line8.x = 12*PIXEL + posX;
line8.y = 8*PIXEL + posY;
line8.w = -13*PIXEL;
line8.h = PIXEL;

line9.x = 12*PIXEL + posX;
line9.y = 9*PIXEL + posY;
line9.w = -13*PIXEL;
line9.h = PIXEL;

line10.x = 11*PIXEL + posX;
line10.y = 10*PIXEL + posY;
line10.w = -11*PIXEL;
line10.h = PIXEL;

line11.x = 11*PIXEL + posX;
line11.y = 11*PIXEL + posY;
line11.w = -11*PIXEL;
line11.h = PIXEL;

line12.x = 10*PIXEL + posX;
line12.y = 12*PIXEL + posY;
line12.w = -9*PIXEL;
line12.h = PIXEL;

line13.x = 8*PIXEL + posX;
line13.y = 13*PIXEL + posY;
line13.w = -5*PIXEL;
line13.h = PIXEL;

break;

case UP:
line1.x = 2*PIXEL + posX;
line1.y = 5*PIXEL + posY;
line1.w = PIXEL;
line1.h = 5*PIXEL;

line2.x = 3*PIXEL + posX;
line2.y = 3*PIXEL + posY;
line2.w = PIXEL;
line2.h = 9*PIXEL;

line3.x = 4*PIXEL + posX;
line3.y = 2*PIXEL + posY;
line3.w = PIXEL;
line3.h = 11*PIXEL;

line4.x = 5*PIXEL + posX;
line4.y = 2*PIXEL + posY;
line4.w = PIXEL;
line4.h = 11*PIXEL;

line5.x = 6*PIXEL + posX;
line5.y = PIXEL + posY;
line5.w = PIXEL;
line5.h = 13*PIXEL;

line6.x = 7*PIXEL + posX;
line6.y = PIXEL + posY;
line6.w = PIXEL;
line6.h = 13*PIXEL;

line7.x = 8*PIXEL + posX;
line7.y = PIXEL + posY;
line7.w = PIXEL;
line7.h = 13*PIXEL;

line8.x = 9*PIXEL + posX;
line8.y = PIXEL + posY;
line8.w = PIXEL;
line8.h = 13*PIXEL;

line9.x = 10*PIXEL + posX;
line9.y = PIXEL + posY;
line9.w = PIXEL;
line9.h = 13*PIXEL;

line10.x = 11*PIXEL + posX;
line10.y = 2*PIXEL + posY;
line10.w = PIXEL;
line10.h = 11*PIXEL;

line11.x = 12*PIXEL + posX;
line11.y = 2*PIXEL + posY;
line11.w = PIXEL;
line11.h = 11*PIXEL;

line12.x = 13*PIXEL + posX;
line12.y = 3*PIXEL + posY;
line12.w = PIXEL;
line12.h = 9*PIXEL;

line13.x = 14*PIXEL + posX;
line13.y = 5*PIXEL + posY;
line13.w = PIXEL;
line13.h = 5*PIXEL;
break;

case DOWN:
line1.x = 2*PIXEL + posX;
line1.y = 5*PIXEL + posY;
line1.w = PIXEL;
line1.h = 5*PIXEL;

line2.x = 3*PIXEL + posX;
line2.y = 3*PIXEL + posY;
line2.w = PIXEL;
line2.h = 9*PIXEL;

line3.x = 4*PIXEL + posX;
line3.y = 2*PIXEL + posY;
line3.w = PIXEL;
line3.h = 11*PIXEL;

line4.x = 5*PIXEL + posX;
line4.y = 2*PIXEL + posY;
line4.w = PIXEL;
line4.h = 11*PIXEL;

line5.x = 6*PIXEL + posX;
line5.y = PIXEL + posY;
line5.w = PIXEL;
line5.h = 13*PIXEL;

line6.x = 7*PIXEL + posX;
line6.y = PIXEL + posY;
line6.w = PIXEL;
line6.h = 13*PIXEL;

line7.x = 8*PIXEL + posX;
line7.y = PIXEL + posY;
line7.w = PIXEL;
line7.h = 13*PIXEL;

line8.x = 9*PIXEL + posX;
line8.y = PIXEL + posY;
line8.w = PIXEL;
line8.h = 13*PIXEL;

line9.x = 10*PIXEL + posX;
line9.y = PIXEL + posY;
line9.w = PIXEL;
line9.h = 13*PIXEL;

line10.x = 11*PIXEL + posX;
line10.y = 2*PIXEL + posY;
line10.w = PIXEL;
line10.h = 11*PIXEL;

line11.x = 12*PIXEL + posX;
line11.y = 2*PIXEL + posY;
line11.w = PIXEL;
line11.h = 11*PIXEL;

line12.x = 13*PIXEL + posX;
line12.y = 3*PIXEL + posY;
line12.w = PIXEL;
line12.h = 9*PIXEL;

line13.x = 14*PIXEL + posX;
line13.y = 5*PIXEL + posY;
line13.w = PIXEL;
line13.h = 5*PIXEL;
break;

}


} */
