
#include "pwrup.h"

Pwrup::Pwrup() {
    setXY(0, 0);
    setPix(2);
}

Pwrup::~Pwrup() {}

void Pwrup::setXY(int ix, int iy) {
    x = ix;
    y = iy;
}

void Pwrup::setPix(int p) {
    pix = p;
}

void Pwrup::setRenderer(SDL_Renderer * r) {
    renderer = r;
}

void Pwrup::render() {
    SDL_SetRenderDrawColor(renderer, 255, 153, 51, 255);
    SDL_RenderFillRect(renderer, &bigDot);
    SDL_RenderFillRect(renderer, &hRect);
    SDL_RenderFillRect(renderer, &vRect);
}

void Pwrup::updatePwr() {
    bigDot.x = x + 6*pix;
    bigDot.y = y + 6*pix;
    bigDot.w = 4*pix;
    bigDot.h = 4*pix;

    hRect.x = x + 5*pix;
    hRect.y = y + 7*pix;
    hRect.w = 6*pix;
    hRect.h = 2*pix;

    vRect.x = x + 7*pix;
    vRect.y = y + 5*pix;
    vRect.w = 2*pix;
    vRect.h = 6*pix;
}
