#ifndef PWRUP_H
#define PWRUP_H

#include <SDL2/SDL.h>

class Pwrup {
    public:
	Pwrup();
	~Pwrup();

	void setXY(int, int);
	void setPix(int);
	void setRenderer(SDL_Renderer *);
	void render();
	void updatePwr();
    
    private:
	int x, y, pix;
	SDL_Rect bigDot, hRect, vRect;
	SDL_Renderer * renderer;
};

#endif
