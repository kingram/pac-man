#include <SDL2/SDL.h>
#include <iostream>

#define NPIXEL 15*2

int posX = 0;
int posY= 0;
int sizeX = 600;
int sizeY = 800;

SDL_Window* window;
SDL_Renderer* renderer;

bool InitEverything();
bool InitSDL();
bool CreateWindow();
bool CreateRenderer();
void SetupRenderer();

void Render();

SDL_Rect line1,line2,line3,line4,line5,line6,line7,line8,line9,line10;
SDL_Rect line11,line12,line13,line14,line15,line16,line17,line18,line19,line20;
SDL_Rect line21,line22,line23,line24,line25,line26,line27,line28,line29,line30;
SDL_Rect line31,line32,line33,line34,line35,line36,line37,line38,line39,line40;
SDL_Rect line41;

int main(int argc, char* args[] ) {
	if (!InitEverything() ) {
		return -1;
	}

	line1.x = 2*NPIXEL + posX;
	line1.y = 2*NPIXEL + posY;
	line1.w = 2*NPIXEL;
	line1.h = 2*NPIXEL;

	line2.x = 2*NPIXEL + posX;
	line2.y = 5*NPIXEL + posY;
	line2.w = 2*NPIXEL;
	line2.h = NPIXEL;

	line3.x = 5*NPIXEL + posX;
	line3.y = 2*NPIXEL + posY;
	line3.w = 3*NPIXEL;
	line3.h = 2*NPIXEL;

	line4.x = 9*NPIXEL + posX;
	line4.y = 1*NPIXEL + posY;
	line4.w = 1*NPIXEL;
	line4.h = 3*NPIXEL;

	line5.x = 11*NPIXEL + posX;
	line5.y = 2*NPIXEL + posY;
	line5.w = 3*NPIXEL;
	line5.h = 2*NPIXEL;

	line6.x = 15*NPIXEL + posX;
	line6.y = 2*NPIXEL + posY;
	line6.w = 2*NPIXEL;
	line6.h = 2*NPIXEL;

	line7.x = 15*NPIXEL + posX;
	line7.y = 5*NPIXEL + posY;
	line7.w = 2*NPIXEL;
	line7.h = 1*NPIXEL;

	line8.x = 5*NPIXEL + posX;
	line8.y = 5*NPIXEL + posY;
	line8.w = 1*NPIXEL;
	line8.h = 5*NPIXEL;

	line9.x = 6*NPIXEL + posX;
	line9.y = 7*NPIXEL + posY;
	line9.w = 2*NPIXEL;
	line9.h = 1*NPIXEL;

	line10.x = 7*NPIXEL + posX;
	line10.y = 5*NPIXEL + posY;
	line10.w = 5*NPIXEL;
	line10.h = 1*NPIXEL;
	
	line11.x = 9*NPIXEL + posX;
	line11.y = 6*NPIXEL + posY;
	line11.w = 1*NPIXEL;
	line11.h = 2*NPIXEL;

	line12.x = 13*NPIXEL + posX;
	line12.y = 5*NPIXEL + posY;
	line12.w = 1*NPIXEL;
	line12.h = 5*NPIXEL;

	line13.x = 11*NPIXEL + posX;
	line13.y = 7*NPIXEL + posY;
	line13.w = 2*NPIXEL;
	line13.h = 1*NPIXEL;

	line14.x = 8*NPIXEL + posX;
	line14.y = 9*NPIXEL + posY;
	line14.w = 1*NPIXEL;
	line14.h = 1*NPIXEL;

	line15.x = 7*NPIXEL + posX;
	line15.y = 9*NPIXEL + posY;
	line15.w = 1*NPIXEL;
	line15.h = 3*NPIXEL;

	line15.x = 7*NPIXEL + posX;
	line15.y = 9*NPIXEL + posY;
	line15.w = 1*NPIXEL;
	line15.h = 2*NPIXEL;

	line16.x = 7*NPIXEL + posX;
	line16.y = 11*NPIXEL + posY;
	line16.w = 5*NPIXEL;
	line16.h = 1*NPIXEL;

	line17.x = 10*NPIXEL + posX;
	line17.y = 9*NPIXEL + posY;
	line17.w = 1*NPIXEL;
	line17.h = 1*NPIXEL;

	line18.x = 11*NPIXEL + posX;
	line18.y = 9*NPIXEL + posY;
	line18.w = 1*NPIXEL;
	line18.h = 3*NPIXEL;

	line19.x = 5*NPIXEL + posX;
	line19.y = 11*NPIXEL + posY;
	line19.w = 1*NPIXEL;
	line19.h = 3*NPIXEL;

	line20.x = 13*NPIXEL + posX;
	line20.y = 11*NPIXEL + posY;
	line20.w = 1*NPIXEL;
	line20.h = 3*NPIXEL;

	line21.x = 7*NPIXEL + posX;
	line21.y = 13*NPIXEL + posY;
	line21.w = 5*NPIXEL;
	line21.h = 1*NPIXEL;

	line22.x = 9*NPIXEL + posX;
	line22.y = 14*NPIXEL + posY;
	line22.w = 1*NPIXEL;
	line22.h = 2*NPIXEL;

	line23.x = 2*NPIXEL + posX;
	line23.y = 15*NPIXEL + posY;
	line23.w = 1*NPIXEL;
	line23.h = 1*NPIXEL;

	line24.x = 3*NPIXEL + posX;
	line24.y = 15*NPIXEL + posY;
	line24.w = 1*NPIXEL;
	line24.h = 3*NPIXEL;

	line25.x = 5*NPIXEL + posX;
	line25.y = 15*NPIXEL + posY;
	line25.w = 3*NPIXEL;
	line25.h = 1*NPIXEL;

	line26.x = 11*NPIXEL + posX;
	line26.y = 15*NPIXEL + posY;
	line26.w = 3*NPIXEL;
	line26.h = 1*NPIXEL;

	line27.x = 16*NPIXEL + posX;
	line27.y = 15*NPIXEL + posY;
	line27.w = 1*NPIXEL;
	line27.h = 1*NPIXEL;

	line28.x = 15*NPIXEL + posX;
	line28.y = 15*NPIXEL + posY;
	line28.w = 1*NPIXEL;
	line28.h = 3*NPIXEL;

	line29.x = 1*NPIXEL + posX;
	line29.y = 17*NPIXEL + posY;
	line29.w = 1*NPIXEL;
	line29.h = 1*NPIXEL;

	line30.x = 7*NPIXEL + posX;
	line30.y = 17*NPIXEL + posY;
	line30.w = 5*NPIXEL;
	line30.h = 1*NPIXEL;

	line31.x = 9*NPIXEL + posX;
	line31.y = 18*NPIXEL + posY;
	line31.w = 1*NPIXEL;
	line31.h = 2*NPIXEL;

	line32.x = 17*NPIXEL + posX;
	line32.y = 17*NPIXEL + posY;
	line32.w = 1*NPIXEL;
	line32.h = 1*NPIXEL;

	line33.x = 5*NPIXEL + posX;
	line33.y = 17*NPIXEL + posY;
	line33.w = 1*NPIXEL;
	line33.h = 2*NPIXEL;

	line34.x = 2*NPIXEL + posX;
	line34.y = 19*NPIXEL + posY;
	line34.w = 6*NPIXEL;
	line34.h = 1*NPIXEL;

	line35.x = 13*NPIXEL + posX;
	line35.y = 17*NPIXEL + posY;
	line35.w = 1*NPIXEL;
	line35.h = 2*NPIXEL;

	line36.x = 11*NPIXEL + posX;
	line36.y = 19*NPIXEL + posY;
	line36.w = 6*NPIXEL;
	line36.h = 1*NPIXEL;

	line37.x = 1*NPIXEL + posX;
	line37.y = 7*NPIXEL + posY;
	line37.w = 3*NPIXEL;
	line37.h = 3*NPIXEL;

	line38.x = 15*NPIXEL + posX;
	line38.y = 7*NPIXEL + posY;
	line38.w = 3*NPIXEL;
	line38.h = 3*NPIXEL;

	line39.x = 1*NPIXEL + posX;
	line39.y = 11*NPIXEL + posY;
	line39.w = 3*NPIXEL;
	line39.h = 3*NPIXEL;

	line40.x = 15*NPIXEL + posX;
	line40.y = 11*NPIXEL + posY;
	line40.w = 3*NPIXEL;
	line40.h = 3*NPIXEL;

	line41.x = NPIXEL + posX;
	line41.y = NPIXEL + posY;
	line41.w = 17*NPIXEL;
	line41.h = 20*NPIXEL;

	Render();
	SDL_Delay(4000);
}

void Render() {
	SDL_SetRenderDrawColor(renderer, 0,0,0,255);
	SDL_RenderClear(renderer);
	SDL_SetRenderDrawColor(renderer,0,0,255,255);
	
	SDL_RenderFillRect(renderer, &line1);

	SDL_RenderFillRect(renderer, &line2);
	SDL_RenderFillRect(renderer, &line3);
	SDL_RenderFillRect(renderer, &line4);
	SDL_RenderFillRect(renderer, &line5);
	SDL_RenderFillRect(renderer, &line6);
	SDL_RenderFillRect(renderer, &line7);
	SDL_RenderFillRect(renderer, &line8);
	SDL_RenderFillRect(renderer, &line9);
	SDL_RenderFillRect(renderer, &line10);
	SDL_RenderFillRect(renderer, &line11);
	SDL_RenderFillRect(renderer, &line12);
	SDL_RenderFillRect(renderer, &line13); 
	SDL_RenderFillRect(renderer, &line14); 
	SDL_RenderFillRect(renderer, &line15); 
	SDL_RenderFillRect(renderer, &line16); 
	SDL_RenderFillRect(renderer, &line17); 
	SDL_RenderFillRect(renderer, &line18); 
	SDL_RenderFillRect(renderer, &line19);
 	SDL_RenderFillRect(renderer, &line20); 
 	SDL_RenderFillRect(renderer, &line21); 
 	SDL_RenderFillRect(renderer, &line22); 
 	SDL_RenderFillRect(renderer, &line23);
	SDL_RenderFillRect(renderer, &line24);  
	SDL_RenderFillRect(renderer, &line25); 
	SDL_RenderFillRect(renderer, &line26); 
	SDL_RenderFillRect(renderer, &line27); 
	SDL_RenderFillRect(renderer, &line28); 
	SDL_RenderFillRect(renderer, &line29);
	SDL_RenderFillRect(renderer, &line30); 
 	SDL_RenderFillRect(renderer, &line31);
	SDL_RenderFillRect(renderer, &line32); 
	SDL_RenderFillRect(renderer, &line33); 
	SDL_RenderFillRect(renderer, &line34);
	SDL_RenderFillRect(renderer, &line35);
	SDL_RenderFillRect(renderer, &line36);  
	SDL_RenderFillRect(renderer, &line37); 
	SDL_RenderFillRect(renderer, &line38); 
	SDL_RenderFillRect(renderer, &line39); 
	SDL_RenderFillRect(renderer, &line40);
	SDL_RenderDrawRect(renderer, &line41);  
 
	SDL_RenderPresent(renderer);

}

bool InitEverything()
{
	if ( !InitSDL() )
		return false;

	if ( !CreateWindow() )
		return false;

	if ( !CreateRenderer() )
		return false;

	SetupRenderer();

	return true;
}
bool InitSDL()
{
	if ( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
	{
		std::cout << " Failed to initialize SDL : " << SDL_GetError() << std::endl;
		return false;
	}

	return true;
}
bool CreateWindow()
{
	window = SDL_CreateWindow( "Server", posX, posY, sizeX, sizeY, 0 );

	if ( window == NULL )
	{
		std::cout << "Failed to create window : " << SDL_GetError();
		return false;
	}

	return true;
}
bool CreateRenderer()
{
	renderer = SDL_CreateRenderer( window, -1, 0 );

	if ( renderer == NULL )
	{
		std::cout << "Failed to create renderer : " << SDL_GetError();
		return false;
	}

	return true;
}
void SetupRenderer()
{
	// Set size of renderer to the same as window
		SDL_RenderSetLogicalSize( renderer, sizeX, sizeY );
	// Set color of renderer to black
		SDL_SetRenderDrawColor( renderer, 0, 0, 0, 255 );
}
