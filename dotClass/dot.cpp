#include "dot.h"
#include <SDL2/SDL.h>

dot::dot()
{
	setPosX(0);
	setPosY(0);
	setPIXEL(2);
}

dot::dot(int x, int y)
{
	setPosX(x);
	setPosY(y);
	setPIXEL(2);
}

dot::~dot() {}

void dot::setPosX(int x)
{
	posX = x;
	return;
}

void dot::setPosY(int y)
{
	posY = y;
	return;
}

void dot::setPIXEL(int p)
{
	PIXEL = p;
	return;
}

void dot::setRenderer(SDL_Renderer * r)
{
	renderer = r;
	return;
}

void dot::Render()
{
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_RenderClear(renderer);
	SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
	SDL_RenderFillRect(renderer, &Dot);
	//SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_RenderPresent(renderer);
	return;
}

void dot::UpdateDot()
{
	Dot.x = posX + 7*PIXEL;
	Dot.y = posY + 7*PIXEL;
	Dot.w = 2*PIXEL;
	Dot.h = 2*PIXEL;
	return;
}
