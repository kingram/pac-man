/* 
 * This code was taken from http://headerphile.blogspot.com/2014/04/part-3-game-programming-in-sdl2-drawing.html
 * and then manipulated to create a ghost.
 */

#include <SDL2/SDL.h>
#include <iostream>

#define PIXEL 5

int posX = 400;
int posY = 400;
int sizeX = 600;
int sizeY = 600;

SDL_Window* window;
SDL_Renderer* renderer;

bool InitEverything();
bool InitSDL();
bool CreateWindow();
bool CreateRenderer();
void SetupRenderer();

void Render();
void RunGame();
void UpdateGhost(int, int);
void UpdateLegs();

SDL_Rect playerPos;
SDL_Rect line1, line2, line3, line4, line5, line6, line7;
SDL_Rect line8, line9, line10, line11, line12, line13, line14;
SDL_Rect line4a, line5a, line6a, line7a, line10a, line11a, line12a, line13a;
SDL_Rect pupil1, pupil2;

int main( int argc, char* args[] )
{
	if ( !InitEverything() ) 
		return -1;

	// Initialze our player
	playerPos.x = 0;
	playerPos.y = 0;
	playerPos.w = 300;
	playerPos.h = 300;

	line1.x = PIXEL + posX;
	line1.y = 7*PIXEL + posY;
	line1.w = PIXEL;
	line1.h = 8*PIXEL;

	line2.x = 2*PIXEL + posX;
	line2.y = 4*PIXEL + posY;
	line2.w = PIXEL;
	line2.h = 10*PIXEL;

	line3.x = 3*PIXEL + posX;
	line3.y = 3*PIXEL + posY;
	line3.w = PIXEL;
	line3.h = 10*PIXEL;

	line4.x = 4*PIXEL + posX;
	line4.y = 2*PIXEL + posY;
	line4.w = PIXEL;
	line4.h = 12*PIXEL;

	line4a.x = 4*PIXEL + posX;
	line4a.y = 5*PIXEL + posY;
	line4a.w = PIXEL;
	line4a.h = 3*PIXEL;

	line5.x = 5*PIXEL + posX;
	line5.y = 2*PIXEL + posY;
	line5.w = PIXEL;
	line5.h = 13*PIXEL;

	line5a.x = 5*PIXEL + posX;
	line5a.y = 4*PIXEL + posY;
	line5a.w = PIXEL;
	line5a.h = 5*PIXEL;

	line6.x = 6*PIXEL + posX;
	line6.y = PIXEL + posY;
	line6.w = PIXEL;
	line6.h = 14*PIXEL;

	line6a.x = 6*PIXEL + posX;
	line6a.y = 4*PIXEL + posY;
	line6a.w = PIXEL;
	line6a.h = 5*PIXEL;

	pupil1.x = 6*PIXEL + posX;
	pupil1.y = 6*PIXEL + posY;
	pupil1.w = 2*PIXEL;
	pupil1.h = 2*PIXEL;

	line7.x = 7*PIXEL + posX;
	line7.y = PIXEL + posY;
	line7.w = PIXEL;
	line7.h = 12*PIXEL;

	line7a.x = 7*PIXEL + posX;
	line7a.y = 5*PIXEL + posY;
	line7a.w = PIXEL;
	line7a.h = 3*PIXEL;

	line8.x = 8*PIXEL + posX;
	line8.y = PIXEL + posY;
	line8.w = PIXEL;
	line8.h = 12*PIXEL;

	line9.x = 9*PIXEL + posX;
	line9.y = PIXEL + posY;
	line9.w = PIXEL;
	line9.h = 14*PIXEL;

	line10.x = 10*PIXEL + posX;
	line10.y = 2*PIXEL + posY;
	line10.w = PIXEL;
	line10.h = 13*PIXEL;

	line10a.x = 10*PIXEL + posX;
	line10a.y = 5*PIXEL + posY;
	line10a.w = PIXEL;
	line10a.h = 3*PIXEL;

	line11.x = 11*PIXEL + posX;
	line11.y = 2*PIXEL + posY;
	line11.w = PIXEL;
	line11.h = 12*PIXEL;

	line11a.x = 11*PIXEL+ posX;
	line11a.y = 4*PIXEL + posY;
	line11a.w = PIXEL;
	line11a.h = 5*PIXEL;

	line12.x = 12*PIXEL + posX;
	line12.y = 3*PIXEL + posY;
	line12.w = PIXEL;
	line12.h = 10*PIXEL;

	line12a.x = 12*PIXEL + posX;
	line12a.y = 4*PIXEL + posY;
	line12a.w = PIXEL;
	line12a.h = 5*PIXEL;

	pupil2.x = 12*PIXEL + posX;
	pupil2.y = 6*PIXEL + posY;
	pupil2.w = 2*PIXEL;
	pupil2.h = 2*PIXEL;

	line13.x = 13*PIXEL + posX;
	line13.y = 4*PIXEL + posY;
	line13.w = PIXEL;
	line13.h = 10*PIXEL;

	line13a.x = 13*PIXEL + posX;
	line13a.y = 5*PIXEL + posY;
	line13a.w = PIXEL;
	line13a.h = 3*PIXEL;

	line14.x = 14*PIXEL + posX;
	line14.y = 7*PIXEL + posY;
	line14.w = PIXEL;
	line14.h = 8*PIXEL;

	RunGame();
}
void Render()
{
	// Clear the window and make it all black
	SDL_RenderClear( renderer );

	// Render out "player"
	SDL_RenderFillRect( renderer, &playerPos );

	// Change color to red
	SDL_SetRenderDrawColor( renderer, 255, 0, 0, 255 );

	// Render out the ghost body
	SDL_RenderFillRect( renderer, &line1 );
	SDL_RenderFillRect( renderer, &line2 );
	SDL_RenderFillRect( renderer, &line3 );
	SDL_RenderFillRect( renderer, &line4 );
	SDL_RenderFillRect( renderer, &line5 );
	SDL_RenderFillRect( renderer, &line6 );
	SDL_RenderFillRect( renderer, &line7 );
	SDL_RenderFillRect( renderer, &line8 );
	SDL_RenderFillRect( renderer, &line9 );
	SDL_RenderFillRect( renderer, &line10 );
	SDL_RenderFillRect( renderer, &line11 );
	SDL_RenderFillRect( renderer, &line12 );
	SDL_RenderFillRect( renderer, &line13 );
	SDL_RenderFillRect( renderer, &line14 );

	// Change color to white
	SDL_SetRenderDrawColor( renderer, 255, 255, 255, 255 );

	// Render out the ghost's scleras
	SDL_RenderFillRect( renderer, &line4a );
	SDL_RenderFillRect( renderer, &line5a );
	SDL_RenderFillRect( renderer, &line6a );
	SDL_RenderFillRect( renderer, &line7a );
	SDL_RenderFillRect( renderer, &line10a );
	SDL_RenderFillRect( renderer, &line11a );
	SDL_RenderFillRect( renderer, &line12a );
	SDL_RenderFillRect( renderer, &line13a );

	// Change color to blue
	SDL_SetRenderDrawColor( renderer, 0, 0, 255, 255 );

	// Render out the ghost's pupils
	SDL_RenderFillRect( renderer, &pupil1 );
	SDL_RenderFillRect( renderer, &pupil2 );

	// Change color to black
	SDL_SetRenderDrawColor( renderer, 0, 0, 0, 255 );

	// Render the changes above
	SDL_RenderPresent( renderer );
}
bool InitEverything()
{
	if ( !InitSDL() )
		return false;

	if ( !CreateWindow() )
		return false;

	if ( !CreateRenderer() )
		return false;

	SetupRenderer();

	return true;
}
bool InitSDL()
{
	if ( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
	{
		std::cout << " Failed to initialize SDL : " << SDL_GetError() << std::endl;
		return false;
	}

	return true;
}
bool CreateWindow()
{
	window = SDL_CreateWindow( "Server", posX, posY, sizeX, sizeY, 0 );

	if ( window == NULL )
	{
		std::cout << "Failed to create window : " << SDL_GetError();
		return false;
	}

	return true;
}
bool CreateRenderer()
{
	renderer = SDL_CreateRenderer( window, -1, 0 );

	if ( renderer == NULL )
	{
		std::cout << "Failed to create renderer : " << SDL_GetError();
		return false;
	}

	return true;
}
void SetupRenderer()
{
	// Set size of renderer to the same as window
	SDL_RenderSetLogicalSize( renderer, sizeX, sizeY );

	// Set color of renderer to black
	SDL_SetRenderDrawColor( renderer, 0, 0, 0, 255 );
}
void RunGame()
{
	for (int i = 0; i < 25; i++)
	{
		UpdateGhost(5, 0);
		Render();
		SDL_Delay( 100 );
	}
	for (int i = 0; i < 25; i++)
	{
		UpdateGhost(5, 1);
		Render();
		SDL_Delay( 100 );
	}
	for (int i = 0; i < 25; i++)
	{
		UpdateGhost(5, 2);
		Render();
		SDL_Delay( 100 );
	}
	for (int i = 0; i < 25; i++)
	{
		UpdateGhost(5, 3);
		Render();
		SDL_Delay( 100 );
	}
}
void UpdateGhost(int speed, int direction)
{
	switch ( direction )
	// 0 is up, 1 is left, 2 is down, 3 is right
	{
		case 0:
			// Boundary conditions can go here (walls, sides of the screen, etc.)
			line1.y -= speed;
			line2.y -= speed;
			line3.y -= speed;
			line4.y -= speed;
			line5.y -= speed;
			line6.y -= speed;
			line7.y -= speed;
			line8.y -= speed;
			line9.y -= speed;
			line10.y -= speed;
			line11.y -= speed;
			line12.y -= speed;
			line13.y -= speed;
			line14.y -= speed;
			line4a.y -= speed;
			line5a.y -= speed;
			line6a.y -= speed;
			line7a.y -= speed;
			line10a.y -= speed;
			line11a.y -= speed;
			line12a.y -= speed;
			line13a.y -= speed;
			pupil1.x = line5a.x;
			pupil1.y = line5a.y;
			pupil2.x = line11a.x;
			pupil2.y = line11a.y;
			break;
		case 1:
                        line1.x -= speed;
                        line2.x -= speed;
                        line3.x -= speed;
                        line4.x -= speed;
                        line5.x -= speed;
                        line6.x -= speed;
                        line7.x -= speed;
                        line8.x -= speed;
                        line9.x -= speed;
                        line10.x -= speed;
                        line11.x -= speed;
                        line12.x -= speed;
                        line13.x -= speed;
                        line14.x -= speed;
                        line4a.x -= speed;
                        line5a.x -= speed;
                        line6a.x -= speed;
                        line7a.x -= speed;
                        line10a.x -= speed;
                        line11a.x -= speed;
                        line12a.x -= speed;
                        line13a.x -= speed;
			pupil1.x = line4a.x;
                        pupil1.y = line4a.y + PIXEL;
			pupil2.x = line10a.x;
                        pupil2.y = line10a.y + PIXEL;
			break;
		case 2:
                        line1.y += speed;
                        line2.y += speed;
                        line3.y += speed;
                        line4.y += speed;
                        line5.y += speed;
                        line6.y += speed;
                        line7.y += speed;
                        line8.y += speed;
                        line9.y += speed;
                        line10.y += speed;
                        line11.y += speed;
                        line12.y += speed;
                        line13.y += speed;
                        line14.y += speed;
                        line4a.y += speed;
                        line5a.y += speed;
                        line6a.y += speed;
                        line7a.y += speed;
                        line10a.y += speed;
                        line11a.y += speed;
                        line12a.y += speed;
                        line13a.y += speed;
			pupil1.x = line5a.x;
                        pupil1.y = line5a.y + 3*PIXEL;
			pupil2.x = line11a.x;
                        pupil2.y = line11a.y + 3*PIXEL;
			break;
		case 3:
                        line1.x += speed;
                        line2.x += speed;
                        line3.x += speed;
                        line4.x += speed;
                        line5.x += speed;
                        line6.x += speed;
                        line7.x += speed;
                        line8.x += speed;
                        line9.x += speed;
                        line10.x += speed;
                        line11.x += speed;
                        line12.x += speed;
                        line13.x += speed;
                        line14.x += speed;
                        line4a.x += speed;
                        line5a.x += speed;
                        line6a.x += speed;
                        line7a.x += speed;
                        line10a.x += speed;
                        line11a.x += speed;
                        line12a.x += speed;
                        line13a.x += speed;
			pupil1.x = line6a.x;
			pupil1.y = line6a.y + 2*PIXEL;
			pupil2.x = line12a.x;
			pupil2.y = line12a.y + 2*PIXEL;
			break;
		default:
			break;
	}
	UpdateLegs();
	// No render here because it renders at the end of RunGame
}
void UpdateLegs()
{
	if (line1.h == 7*PIXEL)
	{
		line1.h = 8*PIXEL;
		line2.h = 10*PIXEL;
		line3.h = 10*PIXEL;
		line4.h = 12*PIXEL;
		line5.h = 13*PIXEL;
		line6.h = 14*PIXEL;
		line7.h = 12*PIXEL;
		line8.h = 12*PIXEL;
		line9.h = 14*PIXEL;
		line10.h = 13*PIXEL;
		line11.h = 12*PIXEL;
		line12.h = 10*PIXEL;
		line13.h = 10*PIXEL;
		line14.h = 8*PIXEL;
	}
	else
	{
                line1.h = 7*PIXEL;
                line2.h = 11*PIXEL;
                line3.h = 12*PIXEL;
                line4.h = 12*PIXEL;
                line5.h = 11*PIXEL;
                line6.h = 13*PIXEL;
                line7.h = 14*PIXEL;
                line8.h = 14*PIXEL;
                line9.h = 13*PIXEL;
                line10.h = 11*PIXEL;
                line11.h = 12*PIXEL;
                line12.h = 12*PIXEL;
                line13.h = 11*PIXEL;
                line14.h = 7*PIXEL;
	}
}
