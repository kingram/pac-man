/* 
 * This code was taken from http://headerphile.blogspot.com/2014/04/part-3-game-programming-in-sdl2-drawing.html
 * and then manipulated to create a ghost.
 */

#include <SDL2/SDL.h>
#include <iostream>

#define PIXEL 20

int posX = 100;
int posY = 200;
int sizeX = 320;
int sizeY = 340;

SDL_Window* window;
SDL_Renderer* renderer;

bool InitEverything();
bool InitSDL();
bool CreateWindow();
bool CreateRenderer();
void SetupRenderer();

void Render();
void RunGame();

SDL_Rect playerPos;
SDL_Rect line1;
SDL_Rect line2;
SDL_Rect line3;
SDL_Rect line4;
SDL_Rect line4a;
SDL_Rect line5;
SDL_Rect line5a;
SDL_Rect line6;
SDL_Rect line6a;
SDL_Rect pupil1;
SDL_Rect line7;
SDL_Rect line7a;
SDL_Rect line8;
SDL_Rect line9;
SDL_Rect line10;
SDL_Rect line10a;
SDL_Rect line11;
SDL_Rect line11a;
SDL_Rect line12;
SDL_Rect line12a;
SDL_Rect pupil2;
SDL_Rect line13;
SDL_Rect line13a;
SDL_Rect line14;

int main( int argc, char* args[] )
{
	if ( !InitEverything() ) 
		return -1;


	// Initlaize our player
	playerPos.x = 0;
	playerPos.y = 0;
	playerPos.w = 300;
	playerPos.h = 300;

	line1.x = PIXEL;
	line1.y = 7*PIXEL;
	line1.w = PIXEL;
	line1.h = 9*PIXEL;

	line2.x = 2*PIXEL;
	line2.y = 4*PIXEL;
	line2.w = PIXEL;
	line2.h = 11*PIXEL;

	line3.x = 3*PIXEL;
	line3.y = 3*PIXEL;
	line3.w = PIXEL;
	line3.h = 11*PIXEL;

	line4.x = 4*PIXEL;
	line4.y = 2*PIXEL;
	line4.w = PIXEL;
	line4.h = 13*PIXEL;

	line4a.x = 4*PIXEL;
	line4a.y = 5*PIXEL;
	line4a.w = PIXEL;
	line4a.h = 3*PIXEL;

	line5.x = 5*PIXEL;
	line5.y = 2*PIXEL;
	line5.w = PIXEL;
	line5.h = 14*PIXEL;

	line5a.x = 5*PIXEL;
	line5a.y = 4*PIXEL;
	line5a.w = PIXEL;
	line5a.h = 5*PIXEL;

	line6.x = 6*PIXEL;
	line6.y = PIXEL;
	line6.w = PIXEL;
	line6.h = 15*PIXEL;

	line6a.x = 6*PIXEL;
	line6a.y = 4*PIXEL;
	line6a.w = PIXEL;
	line6a.h = 5*PIXEL;

	pupil1.x = 6*PIXEL;
	pupil1.y = 6*PIXEL;
	pupil1.w = 2*PIXEL;
	pupil1.h = 2*PIXEL;

	line7.x = 7*PIXEL;
	line7.y = PIXEL;
	line7.w = PIXEL;
	line7.h = 13*PIXEL;

	line7a.x = 7*PIXEL;
	line7a.y = 5*PIXEL;
	line7a.w = PIXEL;
	line7a.h = 3*PIXEL;

	line8.x = 8*PIXEL;
	line8.y = PIXEL;
	line8.w = PIXEL;
	line8.h = 13*PIXEL;

	line9.x = 9*PIXEL;
	line9.y = PIXEL;
	line9.w = PIXEL;
	line9.h = 15*PIXEL;

	line10.x = 10*PIXEL;
	line10.y = 2*PIXEL;
	line10.w = PIXEL;
	line10.h = 14*PIXEL;

	line10a.x = 10*PIXEL;
	line10a.y = 5*PIXEL;
	line10a.w = PIXEL;
	line10a.h = 3*PIXEL;

	line11.x = 11*PIXEL;
	line11.y = 2*PIXEL;
	line11.w = PIXEL;
	line11.h = 13*PIXEL;

	line11a.x = 11*PIXEL;
	line11a.y = 4*PIXEL;
	line11a.w = PIXEL;
	line11a.h = 5*PIXEL;

	line12.x = 12*PIXEL;
	line12.y = 3*PIXEL;
	line12.w = PIXEL;
	line12.h = 11*PIXEL;

	line12a.x = 12*PIXEL;
	line12a.y = 4*PIXEL;
	line12a.w = PIXEL;
	line12a.h = 5*PIXEL;

	pupil2.x = 12*PIXEL;
	pupil2.y = 6*PIXEL;
	pupil2.w = 2*PIXEL;
	pupil2.h = 2*PIXEL;

	line13.x = 13*PIXEL;
	line13.y = 4*PIXEL;
	line13.w = PIXEL;
	line13.h = 11*PIXEL;

	line13a.x = 13*PIXEL;
	line13a.y = 5*PIXEL;
	line13a.w = PIXEL;
	line13a.h = 3*PIXEL;

	line14.x = 14*PIXEL;
	line14.y = 7*PIXEL;
	line14.w = PIXEL;
	line14.h = 9*PIXEL;

	RunGame();
}
void Render()
{
	// Clear the window and make it all black
	SDL_RenderClear( renderer );

	// Render out "player"
	SDL_RenderFillRect( renderer, &playerPos );

	// Change color to red
	SDL_SetRenderDrawColor( renderer, 255, 0, 0, 255 );

	// Render out the ghost body
	SDL_RenderFillRect( renderer, &line1 );
	SDL_RenderFillRect( renderer, &line2 );
	SDL_RenderFillRect( renderer, &line3 );
	SDL_RenderFillRect( renderer, &line4 );
	SDL_RenderFillRect( renderer, &line5 );
	SDL_RenderFillRect( renderer, &line6 );
	SDL_RenderFillRect( renderer, &line7 );
	SDL_RenderFillRect( renderer, &line8 );
	SDL_RenderFillRect( renderer, &line9 );
	SDL_RenderFillRect( renderer, &line10 );
	SDL_RenderFillRect( renderer, &line11 );
	SDL_RenderFillRect( renderer, &line12 );
	SDL_RenderFillRect( renderer, &line13 );
	SDL_RenderFillRect( renderer, &line14 );

	// Change color to white
	SDL_SetRenderDrawColor( renderer, 255, 255, 255, 255 );

	// Render out the ghost's scleras
	SDL_RenderFillRect( renderer, &line4a );
	SDL_RenderFillRect( renderer, &line5a );
	SDL_RenderFillRect( renderer, &line6a );
	SDL_RenderFillRect( renderer, &line7a );
	SDL_RenderFillRect( renderer, &line10a );
	SDL_RenderFillRect( renderer, &line11a );
	SDL_RenderFillRect( renderer, &line12a );
	SDL_RenderFillRect( renderer, &line13a );

	// Change color to blue
	SDL_SetRenderDrawColor( renderer, 0, 0, 255, 255 );

	// Render out the ghost's pupils
	SDL_RenderFillRect( renderer, &pupil1 );
	SDL_RenderFillRect( renderer, &pupil2 );

	// Change color to black
	SDL_SetRenderDrawColor( renderer, 0, 0, 0, 255 );

	// Render the changes above
	SDL_RenderPresent( renderer );
}
bool InitEverything()
{
	if ( !InitSDL() )
		return false;

	if ( !CreateWindow() )
		return false;

	if ( !CreateRenderer() )
		return false;

	SetupRenderer();

	return true;
}
bool InitSDL()
{
	if ( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
	{
		std::cout << " Failed to initialize SDL : " << SDL_GetError() << std::endl;
		return false;
	}

	return true;
}
bool CreateWindow()
{
	window = SDL_CreateWindow( "Server", posX, posY, sizeX, sizeY, 0 );

	if ( window == NULL )
	{
		std::cout << "Failed to create window : " << SDL_GetError();
		return false;
	}

	return true;
}
bool CreateRenderer()
{
	renderer = SDL_CreateRenderer( window, -1, 0 );

	if ( renderer == NULL )
	{
		std::cout << "Failed to create renderer : " << SDL_GetError();
		return false;
	}

	return true;
}
void SetupRenderer()
{
	// Set size of renderer to the same as window
	SDL_RenderSetLogicalSize( renderer, sizeX, sizeY );

	// Set color of renderer to black
	SDL_SetRenderDrawColor( renderer, 0, 0, 0, 255 );
}
void RunGame()
{
	bool loop = true;

	while ( loop )
	{
		SDL_Event event;
		while ( SDL_PollEvent( &event ) )
		{
			if ( event.type == SDL_QUIT )
				loop = false;
			else if ( event.type == SDL_KEYDOWN )
			{
				switch ( event.key.keysym.sym )
				{
					case SDLK_RIGHT:
						++playerPos.x;
						break;
					case SDLK_LEFT:
						--playerPos.x;
						break;

					case SDLK_DOWN:
						++playerPos.y;
						break;
					case SDLK_UP:
						--playerPos.y;
						break;
					default :
						break;
				}
			}
		}

		Render();
		SDL_Delay( 16 );
	}
}
