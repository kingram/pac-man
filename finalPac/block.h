/* block.h */

#ifndef BLOCK_H
#define BLOCK_H

#include <SDL2/SDL.h>
#include "dot.h"
#include "pwrup.h"

/* Contains enums and struct definition of
 * block which is used throughout all logic.
 * */

typedef enum { // Type of block (used on board creation)
    GHOST_HOME,
    POWER_UP,
    REG_DOT,
    REG_NODOT,
} BlockType;

typedef enum { // Direction of movement (all logic)
    UP, 
    DOWN,
    LEFT,
    RIGHT,
    NOMOVE
} direction;

typedef enum { // Ghost name (all logic)
    CLYDE,
    BLINKY,
    PINKY,
    INKY,
    NONE,
} ghostName;

struct block {
    bool pacValid; // Can pacman enter this block
    bool isPwrup;  // Does this block have a power up
    bool isDot;	   // Does this block have a dot

    bool visited;  // Used in delBoard() and renderDots
    bool isVisit;  // to determine if a block has been visited

    block * up;	   // Contains pointers to all directions
    block * down;
    block * right;
    block * left;

    int x;	   // Contains x and y coordinates of blocks location
    int y;	   // on board

    dot * d;	   // Pointers to dot/powerup associated with block
    Pwrup * p;
};

#endif
