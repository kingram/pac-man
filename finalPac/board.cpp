/* board.cpp */

#include <cstddef>
#include <cstdio>
#include "board.h"
#include "ghost.h"
#include "dot.h"
#include "pwrup.h"
#include "back.h"

#define BOARDPIXEL 30

/* Implementation for the board class. Uses new/delete to generate
 * blocks and access functions to set the corresponding values of
 * the blocks (to initilize). Then uses initBoard() to incrementally
 * generate each other block in the board. NULL pointers are used
 * to indicate that there is no path in that direction. delBoard is
 * called by the deconstructor to use a depth-first search to remove
 * each block using delete and clean up the program. Additional logic
 * is included to indicate if the game has been won (or lost) and 
 * print the score to the screen.
 * */

Board::Board() {
    /* Initilizes board using new and access functions to set values.
     * Initilizes values used in wininng/losing logic 
     * */

    pacStart	= new block;
    blinkyStart = new block;
    pinkyStart	= new block;
    inkyStart	= new block;
    clydeStart	= new block;
    telR	= new block;
    telL	= new block;
    
    setVal(pacStart,	true,	false,	false);
    setVal(blinkyStart, false,	false,	false);
    setVal(pinkyStart,	false,	false,	false);
    setVal(inkyStart,	false,	false,	false);
    setVal(clydeStart,	false,	false,	false);
    setVal(telR,	true,	false,	false);
    setVal(telL,	true,	false,	false);

    setDir(pacStart,	NULL,	    NULL,	NULL,	    NULL);
    setDir(blinkyStart,	NULL,	    NULL,	NULL,	    pinkyStart);
    setDir(pinkyStart,	clydeStart, inkyStart,	blinkyStart, NULL);
    setDir(inkyStart,	pinkyStart, NULL,	NULL,	    NULL);
    setDir(clydeStart,	NULL,	    pinkyStart,	NULL,	    NULL);
    setDir(telL, NULL, telR, NULL, NULL);
    setDir(telR, telL, NULL, NULL, NULL);

    setXY(pacStart, 9*BOARDPIXEL, 16*BOARDPIXEL);
    setXY(blinkyStart, 9*BOARDPIXEL, 9*BOARDPIXEL);
    setXY(pinkyStart, 9*BOARDPIXEL, 10*BOARDPIXEL);
    setXY(inkyStart, 8*BOARDPIXEL, 10*BOARDPIXEL);
    setXY(clydeStart, 10*BOARDPIXEL, 10*BOARDPIXEL);
    setXY(telL, 2*BOARDPIXEL, 10*BOARDPIXEL);
    setXY(telR, 16*BOARDPIXEL, 10*BOARDPIXEL);

    initBoard();

    pacman  = pacStart;
    blinky  = blinkyStart;
    inky    = inkyStart;
    pinky   = pinkyStart;
    clyde   = clydeStart;

    nDot    = 153;
    nPwr    = 4;
    score   = 0;
    nlives  = 3;
}

Board::Board(const Board &cpBoard) { // Copy constructor does the same as constructor
    pacStart	= new block;
    blinkyStart = new block;
    pinkyStart	= new block;
    inkyStart	= new block;
    clydeStart	= new block;
    telR	= new block;
    telL	= new block;

    setVal(pacStart,	true,	false,	false);
    setVal(blinkyStart, false,	false,	false);
    setVal(pinkyStart,	false,	false,	false);
    setVal(inkyStart,	false,	false,	false);
    setVal(clydeStart,	false,	false,	false);
    setVal(telR,	true,	false,	false);
    setVal(telL,	true,	false,	false);
    
    setDir(pacStart,	NULL,	    NULL,	NULL,	    NULL);
    setDir(blinkyStart,	NULL,	    NULL,	NULL,	    pinkyStart);
    setDir(pinkyStart,	clydeStart, inkyStart,	blinkyStart, NULL);
    setDir(inkyStart,	pinkyStart, NULL,	NULL,	    NULL);
    setDir(clydeStart,	NULL,	    pinkyStart,	NULL,	    NULL);
    setDir(telL, NULL, telR, NULL, NULL);
    setDir(telR, telL, NULL, NULL, NULL);

    setXY(pacStart, 9*BOARDPIXEL, 16*BOARDPIXEL);
    setXY(blinkyStart, 9*BOARDPIXEL, 9*BOARDPIXEL);
    setXY(pinkyStart, 9*BOARDPIXEL, 10*BOARDPIXEL);
    setXY(inkyStart, 8*BOARDPIXEL, 10*BOARDPIXEL);
    setXY(clydeStart, 10*BOARDPIXEL, 10*BOARDPIXEL);
    setXY(telL, 2*BOARDPIXEL, 10*BOARDPIXEL);
    setXY(telR, 16*BOARDPIXEL, 10*BOARDPIXEL);
}

Board::~Board() {
    // calls recursive delete function to clear the board
    delBoard(pinkyStart);
}

void Board::delBoard(block * b) {
    /* Creates a pointer to the blocks which are pointed to
     * by the block b, then if those aren't NULL, recursively
     * deletes those blocks using this function. Removes the 
     * pointer from the block b is pointing to in order to
     * allow the recursive function to implement properly.
     * */
    
    block * ptrR = b->right;
    
    b->visited = true;

    if(ptrR) {
	ptrR->left = NULL;
	if(ptrR->visited == false)
	    delBoard(ptrR);
    }

    block * ptrL = b->left;

    if(ptrL) {
	ptrL->right = NULL;
	if(ptrL->visited == false)
	    delBoard(ptrL);
    }

    block * ptrU = b->up;

    if(ptrU) {
	ptrU->down = NULL;
	if(ptrU->visited == false)
	    delBoard(ptrU);
    }

    block * ptrD = b->down;

    if(ptrD) {
	ptrD->up = NULL;
	if(ptrD->visited == false)
	    delBoard(ptrD);
    }

    // deletes pointer to dot and powerup

    if (b->isDot == true)
	delete(b->d);
    if (b->isPwrup == true)
	delete(b->p);
    delete b;
     
    return;
}

void Board::renderDots(block * b, bool visit) { 
    /* Uses same depth-first search (from delBoard) to access
     * each block in the board and generate an array which
     * contains the locations of the dots (and powerups) on
     * the board.
     * */
    block * ptrR = b->right;

    if(b->isVisit == false) // Used so this may be run more than once (if necessary)
	b->isVisit = true;
    else
	b->isVisit = false;

    if(ptrR) {
	if(ptrR->isVisit == visit)
	    renderDots(ptrR, visit);
    }

    block * ptrL = b->left;

    if(ptrL) {
	if(ptrL->isVisit == visit)
	    renderDots(ptrL, visit);
    }

    block * ptrU = b->up;

    if(ptrU) {
	if(ptrU->isVisit == visit)
	    renderDots(ptrU, visit);
    }

    block * ptrD = b->down;

    if(ptrD) {
	if(ptrD->isVisit == visit)
	    renderDots(ptrD, visit);
    }

    // Adds values to matrix

    if (b->isDot == true)
	ifDot[(b->x) / 30][(b->y) / 30] = 1;
    else if (b->isPwrup == true)
	ifDot[(b->x) / 30][(b->y) / 30] = 2;
    return;

}

void Board::setMatrix() {
    // initilizes original matrix (all 0s)

    for (int i = 0; i < 18; i++)
    {
	for (int j = 0; j < 21; j++)
	{
	    ifDot[i][j] = 0;
	}
    }

    // calls initial renderDots
    
    renderDots(pacman, false);

    return;
}

void Board::setRenderer(SDL_Renderer * r) {
    renderer = r;
    return;
}

void Board::setVal(block * b, bool pV, bool iD, bool iP) {
    // initilizes boolean values of the block
    
    b->pacValid = pV;
    b->isDot	= iD;
    b->isPwrup	= iP;
    b->visited  = false; // Used in delete
    b->isVisit	= false; // Used in going through dots
}

void Board::setDir(block * b, block * r, block * l, block * u, block * d) {
    // initilizes directions corresponding the block
    
    b->right	= r;
    b->left	= l;
    b->up	= u;
    b->down	= d;
}

void Board::setXY(block * b, int x, int y) {
    // initilizes x and y values for block
    
    b->x    = x;
    b->y    = y;
}

block * Board::block_r(block * b, BlockType bt, block * r, block * u, block * d) {
    /* Uses genBlock() to create a new block to the right of the block which is
     * passed into the function. Allocates the values and directions based on 
     * which values are coming into the function. Sets up pointers properly so 
     * pointers come from both blocks (pointing to other block). 
     * */
    
    switch(bt) { // Switch between different types of block
	case GHOST_HOME:
	    b->right = genBlock(r, b, u, d, false, false, false);
	    break;
	case POWER_UP:
	    b->right = genBlock(r, b, u, d, true, false, true);
	    break;
	case REG_DOT:
	    b->right = genBlock(r, b, u, d, true, true, false);
	    break;
	case REG_NODOT:
	    b->right = genBlock(r, b, u, d, true, false, false);
	    break;
    }
    
    // Determines x and y coordinates based on prev. coordinates from b

    b->right->x = b->x + BOARDPIXEL;
    b->right->y = b->y;

    // Adds dot and powerup information if that is required
    
    if (b->right->isDot == true) {
        b->right->d = new dot;

        b->right->d->setRenderer(renderer);
        b->right->d->setPosX(b->x);
        b->right->d->setPosY(b->y);
        if(b->right->isDot)
            b->right->d->setPIXEL(2);
        else
            b->right->d->setPIXEL(0);
        b->right->d->UpdateDot();
        b->right->d->Render();
    }

    if (b->right->isPwrup == true) {
        b->right->p = new Pwrup;
        b->right->p->setRenderer(renderer);
        b->right->p->setXY(b->x, b->y);
        if(b->right->isPwrup)
            b->right->p->setPix(2);
        else
            b->right->p->setPix(0);
        b->right->p->updatePwr();
        b->right->p->render();
    }

    return b->right;
}

block * Board::block_l(block * b, BlockType bt, block * l, block * u, block * d) {
    /* Does same thing as block_r() but instead creates function to the left. */
    
    switch(bt) {
	case GHOST_HOME:
	    b->left = genBlock(b, l, u, d, false, false, false);
	    break;
	case POWER_UP:
	    b->left = genBlock(b, l, u, d, true, false, true);
	    break;
	case REG_DOT:
	    b->left = genBlock(b, l, u, d, true, true, false);
	    break;
	case REG_NODOT:
	    b->left = genBlock(b, l, u, d, true, false, false);
	    break;
    }
    b->left->x = b->x - BOARDPIXEL;
    b->left->y = b->y;

    if (b->left->isDot == true) {
        b->left->d = new dot;

        b->left->d->setRenderer(renderer);
        b->left->d->setPosX(b->x);
        b->left->d->setPosY(b->y);
        if(b->left->isDot)
            b->left->d->setPIXEL(2);
        else
            b->left->d->setPIXEL(0);
        b->left->d->UpdateDot();
        b->left->d->Render();
    }

    if (b->left->isPwrup == true) {
        b->left->p = new Pwrup;
        b->left->p->setRenderer(renderer);
        b->left->p->setXY(b->x, b->y);
        if(b->left->isPwrup)
            b->left->p->setPix(2);
        else
            b->left->p->setPix(0);
        b->left->p->updatePwr();
        b->left->p->render();
    }

    return b->left;
}

block * Board::block_u(block * b, BlockType bt, block * r, block * l, block * u) {
    /* Does the same thing as block_r() but instead creates a block up. */
    
    switch(bt) {
	case GHOST_HOME:
	    b->up = genBlock(r, l, u, b, false, false, false);
	    break;
	case POWER_UP:
	    b->up = genBlock(r, l, u, b, true, false, true);
	    break;
	case REG_DOT:
	    b->up = genBlock(r, l, u, b, true, true, false);
	    break;
	case REG_NODOT:
	    b->up = genBlock(r, l, u, b, true, false, false);
	    break;
    }
    b->up->x = b->x;
    b->up->y = b->y - BOARDPIXEL;

    if (b->up->isDot == true) {
        b->up->d = new dot;

        b->up->d->setRenderer(renderer);
        b->up->d->setPosX(b->x);
        b->up->d->setPosY(b->y);
        if(b->up->isDot)
            b->up->d->setPIXEL(2);
        else
            b->up->d->setPIXEL(0);
        b->up->d->UpdateDot();
        b->up->d->Render();
    }

    if (b->up->isPwrup == true) {
        b->up->p = new Pwrup;
        b->up->p->setRenderer(renderer);
        b->up->p->setXY(b->x, b->y);
        if(b->up->isPwrup)
            b->up->p->setPix(2);
        else
            b->up->p->setPix(0);
        b->up->p->updatePwr();
        b->up->p->render();
    }

    return b->up;
}

block * Board::block_d(block * b, BlockType bt, block * r, block * l, block * d) {
    /* Does the same thing as block_r() but instead creates a block down. */
    
    switch(bt) {
	case GHOST_HOME:
	    b->down = genBlock(r, l, b, d, false, false, false);
	    break;
	case POWER_UP:
	    b->down = genBlock(r, l, b, d, true, false, true);
	    break;
	case REG_DOT:
	    b->down = genBlock(r, l, b, d, true, true, false);
	    break;
	case REG_NODOT:
	    b->down = genBlock(r, l, b, d, true, false, false);
	    break;
    }
    b->down->x = b->x;
    b->down->y = b->y + BOARDPIXEL;

    if (b->down->isDot == true) {
        b->down->d = new dot;

        b->down->d->setRenderer(renderer);
        b->down->d->setPosX(b->x);
        b->down->d->setPosY(b->y);
        if(b->down->isDot)
            b->down->d->setPIXEL(2);
        else
            b->down->d->setPIXEL(0);
        b->down->d->UpdateDot();
        b->down->d->Render();
    }

    if (b->down->isPwrup == true) {
        b->down->p = new Pwrup;
        b->down->p->setRenderer(renderer);
        b->down->p->setXY(b->x, b->y);
        if(b->down->isPwrup)
            b->down->p->setPix(2);
        else
            b->down->p->setPix(0);
        b->down->p->updatePwr();
        b->down->p->render();
    }

    return b->down;
}

block * Board::genBlock(block * r, block * l, block * u, block * d, bool pV, bool iD, bool iP) {
    /* Generates a block using new based on inputs about the type of block
     * passed into the function. Returns the pointer to the new block.
     * */
    
    block * newBlock = new block;
    setVal(newBlock, pV, iD, iP);
    setDir(newBlock, r, l, u, d);

    return newBlock;
}

void Board::initBoard() {
    /* Systematically creates all the blocks in the board
     * using a series of for loops and the block_r(), block_u(),
     * block_l(), and block_d() to add all the blocks. Contains
     * print statements which indicate errors throughout to
     * ensure no errors were made in the board initilization.
     * */
    
    block * ptr1, * ptr2, * tempPtr;

    ptr1 = block_r(pacStart, REG_DOT, NULL, NULL, NULL);
    ptr2 = block_l(pacStart, REG_DOT, NULL, NULL, NULL);

    if(pacStart->right != ptr1)
	fprintf(stderr, "ERROR\n");
    if(pacStart->left != ptr2)
	fprintf(stderr, "ERROR\n");

    block * ptr3, * ptr4, * ptr5, * ptr6;

    ptr3 = ptr2;
    ptr4 = ptr1;

    for(int i = 0; i != 2; i++) {
	ptr3 = block_u(ptr3, REG_DOT, NULL, NULL, NULL);
	ptr4 = block_u(ptr4, REG_DOT, NULL, NULL, NULL);
    }
    for(int i = 0; i != 2; i++) {
	ptr3 = block_l(ptr3, REG_DOT, NULL, NULL, NULL);
	ptr4 = block_r(ptr4, REG_DOT, NULL, NULL, NULL);
    }

    ptr5 = ptr4;
    ptr6 = ptr3;
    
    for(int i = 0; i != 2; i++) {
	ptr3 = block_l(ptr3, REG_DOT, NULL, NULL, NULL);
	ptr4 = block_r(ptr4, REG_DOT, NULL, NULL, NULL);
    }

    if(ptr4->x != 14*BOARDPIXEL)
	fprintf(stderr, "ERROR\n");
    if(ptr3->x != 4*BOARDPIXEL)
	fprintf(stderr, "ERROR\n");

    for(int i = 0; i != 2; i++) {
	ptr2 = block_l(ptr2, REG_DOT, NULL, NULL, NULL);
	ptr1 = block_r(ptr1, REG_DOT, NULL, NULL, NULL);
    }

    block * ptr7, * ptr8;

    ptr7 = ptr2;
    ptr8 = ptr1;

    for(int i = 0; i != 2; i++) {
	ptr7 = block_d(ptr7, REG_DOT, NULL, NULL, NULL);
	ptr8 = block_d(ptr8, REG_DOT, NULL, NULL, NULL);
    }
    for(int i = 0; i != 2; i++) {
	ptr7 = block_r(ptr7, REG_DOT, NULL, NULL, NULL);
	ptr8 = block_l(ptr8, REG_DOT, NULL, NULL, NULL);
    }
    for(int i = 0; i != 2; i++) {
	ptr7 = block_d(ptr7, REG_DOT, NULL, NULL, NULL);
	ptr8 = block_d(ptr8, REG_DOT, NULL, NULL, NULL);
    }

    if(ptr7->y != BOARDPIXEL*20)
	fprintf(stderr, "ERROR\n");
    if(ptr8->y != BOARDPIXEL*20)
	fprintf(stderr, "ERROR\n");

    tempPtr = ptr7;
    tempPtr = block_r(tempPtr, REG_DOT, ptr8, NULL, NULL);
    ptr8->left = tempPtr;

    if(tempPtr->right != ptr8)
	fprintf(stderr, "ERROR\n");
    if(ptr8->left !=  tempPtr)
	fprintf(stderr, "ERROR\n");
    if(tempPtr->left != ptr7)
	fprintf(stderr, "ERROR\n");
    if(ptr7->right != tempPtr)
	fprintf(stderr, "ERROR\n");

    for(int i = 0; i != 2; i++) {
	ptr2 = block_l(ptr2, REG_DOT, NULL, NULL, NULL);
	ptr1 = block_r(ptr1, REG_DOT, NULL, NULL, NULL);
    }

    if(ptr2->x != ptr3->x || ptr1->x != ptr4->x)
	fprintf(stderr, "ERROR\n");

    tempPtr = ptr3;
    tempPtr = block_d(tempPtr, REG_DOT, NULL, NULL, ptr2);
    ptr2->up = tempPtr;

    tempPtr = ptr4;
    tempPtr = block_d(tempPtr, REG_DOT, NULL, NULL, ptr1);
    ptr1->up = tempPtr;

    for(int i = 0; i != 7; i++) {
	ptr7 = block_l(ptr7, REG_DOT, NULL, NULL, NULL);
	ptr8 = block_r(ptr8, REG_DOT, NULL, NULL, NULL);
    }
    for(int i = 0; i != 2; i++) {
	ptr7 = block_u(ptr7, REG_DOT, NULL, NULL, NULL);
	ptr8 = block_u(ptr8, REG_DOT, NULL, NULL, NULL);
    }
    ptr7 = block_r(ptr7, REG_DOT, NULL, NULL, NULL);
    ptr8 = block_l(ptr8, REG_DOT, NULL, NULL, NULL);

    if(ptr7->x != 2*BOARDPIXEL || ptr7->y != 18*BOARDPIXEL)
	fprintf(stderr, "ERROR\n");
    
    tempPtr = ptr7;
    for(int i = 0; i != 2; i++)
	tempPtr = block_r(tempPtr, REG_DOT, NULL, NULL, NULL);
    tempPtr = block_u(tempPtr, REG_DOT, NULL, NULL, ptr2);
    ptr2->down = tempPtr;

    tempPtr = ptr8;
    for(int i = 0; i != 2; i++)
	tempPtr = block_l(tempPtr, REG_DOT, NULL, NULL, NULL);
    tempPtr = block_u(tempPtr, REG_DOT, NULL, NULL, ptr1);
    ptr1->down = tempPtr;

    for(int i = 0; i != 2; i++) {
	ptr7 = block_u(ptr7, REG_DOT, NULL, NULL, NULL);
	ptr8 = block_u(ptr8, REG_DOT, NULL, NULL, NULL);
    }
    ptr7 = block_l(ptr7, POWER_UP, NULL, NULL, NULL);
    ptr8 = block_r(ptr8, POWER_UP, NULL, NULL, NULL);
    for(int i = 0; i != 2; i++) {
	ptr7 = block_u(ptr7, REG_DOT, NULL, NULL, NULL);
	ptr8 = block_u(ptr8, REG_DOT, NULL, NULL, NULL);
    }

    tempPtr = ptr7;
    tempPtr = block_r(tempPtr, REG_DOT, NULL, NULL, NULL);
    tempPtr = block_r(tempPtr, REG_DOT, ptr3, NULL, NULL);
    ptr3->left = tempPtr;

    tempPtr = ptr8;
    tempPtr = block_l(tempPtr, REG_DOT, NULL, NULL, NULL);
    tempPtr = block_l(tempPtr, REG_DOT, ptr4, NULL, NULL);
    ptr4->right = tempPtr;

    for(int i = 0; i != 2; i++) {
	ptr6 = block_u(ptr6, REG_NODOT, NULL, NULL, NULL);
	ptr5 = block_u(ptr5, REG_NODOT, NULL, NULL, NULL);
    }

    for(int i = 0; i != 4; i++) {
	ptr3 = block_u(ptr3, REG_DOT, NULL, NULL, NULL);
	ptr4 = block_u(ptr4, REG_DOT, NULL, NULL, NULL);
    }

    tempPtr = ptr6;
    for(int i = 0; i != 4; i++)
	tempPtr = block_r(tempPtr, REG_NODOT, NULL, NULL, NULL);
    tempPtr = block_r(tempPtr, REG_NODOT, ptr5, NULL, NULL);
    ptr5->left = tempPtr;

    for(int i = 0; i != 2; i++) {
	ptr6 = block_u(ptr6, REG_NODOT, NULL, NULL, NULL);
	ptr5 = block_u(ptr5, REG_NODOT, NULL, NULL, NULL);
    }

    tempPtr = ptr3;
    tempPtr = block_r(tempPtr, REG_NODOT, ptr6, NULL, NULL);
    ptr6->left = tempPtr;

    tempPtr = ptr5; 
    tempPtr = block_r(tempPtr, REG_NODOT, ptr4, NULL, NULL);
    ptr4->left = tempPtr;

    tempPtr = ptr3;
    tempPtr = block_l(tempPtr, REG_NODOT, telL, NULL, NULL);
    telL->right = tempPtr;
    tempPtr = ptr4;
    tempPtr = block_r(tempPtr, REG_NODOT, telR, NULL, NULL);
    telR->left = tempPtr;

    for(int i = 0; i != 4; i++) {
	ptr3 = block_u(ptr3, REG_DOT, NULL, NULL, NULL);
	ptr4 = block_u(ptr4, REG_DOT, NULL, NULL, NULL);
    }

    ptr7 = ptr3;
    ptr8 = ptr4;

    for(int i = 0; i != 3; i++) {
	ptr7 = block_l(ptr7, REG_DOT, NULL, NULL, NULL);
	ptr8 = block_r(ptr8, REG_DOT, NULL, NULL, NULL);
    }

    for(int i = 0; i != 2; i++) {
	ptr3 = block_u(ptr3, REG_DOT, NULL, NULL, NULL);
	ptr4 = block_u(ptr4, REG_DOT, NULL, NULL, NULL);
	ptr7 = block_u(ptr7, REG_DOT, NULL, NULL, NULL);
	ptr8 = block_u(ptr8, REG_DOT, NULL, NULL, NULL);
    }

    tempPtr = ptr7;
    tempPtr = block_r(tempPtr, REG_DOT, NULL, NULL, NULL);
    tempPtr = block_r(tempPtr, REG_DOT, ptr3, NULL, NULL);
    ptr3->left = tempPtr;

    tempPtr = ptr4;
    tempPtr = block_r(tempPtr, REG_DOT, NULL, NULL, NULL);
    tempPtr = block_r(tempPtr, REG_DOT, ptr8, NULL, NULL);
    ptr8->left = tempPtr;

    ptr7 = block_u(ptr7, POWER_UP, NULL, NULL, NULL);
    ptr8 = block_u(ptr8, POWER_UP, NULL, NULL, NULL);

    for(int i = 0; i != 2; i++) {
	ptr7 = block_u(ptr7, REG_DOT, NULL, NULL, NULL);
	ptr8 = block_u(ptr8, REG_DOT, NULL, NULL, NULL);
    }

    for(int i = 0; i != 3; i++) {
	ptr7 = block_r(ptr7, REG_DOT, NULL, NULL, NULL);
	ptr8 = block_l(ptr8, REG_DOT, NULL, NULL, NULL);
    }
    
    tempPtr = ptr7;
    tempPtr = block_d(tempPtr, REG_DOT, NULL, NULL, NULL);
    tempPtr = block_d(tempPtr, REG_DOT, NULL, NULL, ptr3);
    ptr3->up = tempPtr;

    tempPtr = ptr8;
    tempPtr = block_d(tempPtr, REG_DOT, NULL, NULL, NULL);
    tempPtr = block_d(tempPtr, REG_DOT, NULL, NULL, ptr4);
    ptr4->up = tempPtr;

    //fprintf(stdout, "PTR4 DOWN = (%d, %d)\n", ptr4->up->x/BOARDPIXEL, ptr4->up->y/BOARDPIXEL);

    for(int i = 0; i != 4; i++) {
	ptr7 = block_r(ptr7, REG_DOT, NULL, NULL, NULL);
	ptr8 = block_l(ptr8, REG_DOT, NULL, NULL, NULL);
    }

    for(int i = 0; i != 3; i++) {
	ptr7 = block_d(ptr7, REG_DOT, NULL, NULL, NULL);
	ptr8 = block_d(ptr8, REG_DOT, NULL, NULL, NULL);
    }

    tempPtr = ptr7;
    tempPtr = block_r(tempPtr, REG_DOT, ptr8, NULL, NULL);
    ptr8->left = tempPtr;

    for(int i = 0; i != 2; i++) {
	ptr3 = block_r(ptr3, REG_DOT, NULL, NULL, NULL);
	ptr4 = block_l(ptr4, REG_DOT, NULL, NULL, NULL);
    }

    tempPtr = ptr3;
    tempPtr = block_r(tempPtr, REG_DOT, ptr7, NULL, NULL);
    ptr7->left = tempPtr;

    tempPtr = ptr8;
    tempPtr = block_r(tempPtr, REG_DOT, ptr4, NULL, NULL);
    ptr4->left = tempPtr;

    for(int i = 0; i != 2; i++) {
	ptr5 = block_u(ptr5, REG_NODOT, NULL, NULL, NULL);
	ptr6 = block_u(ptr6, REG_NODOT, NULL, NULL, NULL);
    }
    for(int i = 0; i != 2; i++) {
	ptr5 = block_l(ptr5, REG_NODOT, NULL, NULL, NULL);
	ptr6 = block_r(ptr6, REG_NODOT, NULL, NULL, NULL);
    }

    tempPtr = ptr6;
    tempPtr = block_u(tempPtr, REG_NODOT, NULL, NULL, NULL);
    tempPtr = block_u(tempPtr, REG_DOT, NULL, NULL, NULL);
    for(int i = 0; i != 2; i++) {
	tempPtr = block_l(tempPtr, REG_DOT, NULL, NULL, NULL);
    }
    tempPtr = block_u(tempPtr, REG_DOT, NULL, NULL, ptr3);
    ptr3->down = tempPtr;

    tempPtr = ptr5;
    tempPtr = block_u(tempPtr, REG_NODOT, NULL, NULL, NULL);
    tempPtr = block_u(tempPtr, REG_DOT, NULL, NULL, NULL);
    for(int i = 0; i != 2; i++) {
	tempPtr = block_r(tempPtr, REG_DOT, NULL, NULL, NULL);
    }
    tempPtr = block_u(tempPtr, REG_DOT, NULL, NULL, ptr4);
    ptr4->down = tempPtr;

    ptr6 = block_r(ptr6, REG_NODOT, ptr5, NULL, blinkyStart);
    ptr5->left = ptr6;
    blinkyStart->up = ptr6;
}

bool Board::canMove(block * b, direction d, ghostName g) {
    /* Returns a boolean value to indicate if a ghost g
     * (which is NONE when pacman) can move to a particular
     * block. Neither can move to NULL pointers (because that
     * indicates they ran into a wall) and pacman cannot
     * enter the ghost hut, as well as the ghost are unable
     * to enter the teleportation blocks (they cannot teleport).
     * */
    
    switch(d) {
	case RIGHT:
	    if(b->right != NULL) {
		if(g == NONE && b->right->pacValid)
		    return true;
		else if(g != NONE && (b->right != telR && b->right != telL))
		    return true;
		else
		    return false;
	    } else
		return false;
	    break;
	case LEFT:
	    if(b->left != NULL) {
		if(g == NONE && b->left->pacValid)
		    return true;
		else if(g != NONE && (b->left != telR && b->left != telL))
		    return true;
		else
		    return false;
	    } else
		return false;
	    break;
	case UP:
	    if(b->up != NULL) {
		if(g == NONE && b->up->pacValid)
		    return true;
		else if(g != NONE && (b->up != telR && b->up != telL))
		    return true;
		else
		    return false;
	    } else
		return false;
	    break;
	case DOWN:
	    if(b->down != NULL) {
		if(g == NONE && b->down->pacValid)
		    return true;
		else if(g != NONE && (b->down != telR && b->down != telL))
		    return true;
		else
		    return false;
	    } else
		return false;
	    break;
	case NOMOVE: // This indicates that pacman is not moving
	    return false;
	    break;
    }
}

block * Board::move(block * b, direction d) {
    /* Actually updates the pointer in the direction
     * specified. Returns the value of b (which is moved
     * to new location).
     * */
    
    switch(d) {
	case RIGHT:
	    b = b->right;
	    break;
	case LEFT:
	    b = b->left;
	    break;
        case UP:
	   b = b->up;
	   break;
	case DOWN:
	    b = b->down;	
	    break;
	case NOMOVE:
	    b=b;
	    break;
    }
    return b;
}

int Board::getDot() {
    return nDot;
}

ghostName Board::isDead() {
    /* Original implementation of loogic to determine if
     * pacman has died.
     * */
    
    if(pacman->x == blinky->x && pacman->y == blinky->y)
	return BLINKY;
    if(pacman->x == inky->x && pacman->y == inky->y)
	return INKY;
    if(pacman->x == pinky->x && pacman->y == pinky->y)
	return PINKY;
    if(pacman->x == clyde->x && pacman->y == clyde->y)
	return CLYDE;
    return NONE;
	
}

block * Board::getThing(ghostName b) {
    /* Returns the pointer to the location which
     * a ghost b starts. Used to reset locations
     * on the board mid-gameplay and to initilize
     * pointers for logic in the game.
     * */
    
    switch(b) {
	case BLINKY:
	    return blinky;
	    break;
	case PINKY:
	    return pinky;
	    break;
	case INKY:
	    return inky;
	    break;
	case CLYDE:
	    return clyde;
	    break;
	case NONE:
	    return pacman;
	    break;
    }
}

bool Board::setScore(int s) {
	/* Updates the score and uses knowledege about
	 * how the program increments score to determine
	 * if the game has been won or lost (based on
	 * number of dots remaining and number of lives).
	 * Outputs a corresponding message (which leading
	 * newlines to appear as if the score is updating
	 * on the screen). 
	 * */
	
	if((s-score) == 100 && nDot == 1) { // Only 100 when dot is removed
	    score = s;
	    for(int i = 0; i != 100; i++) {
		fprintf(stdout, "\n");
	    }
	    fprintf(stdout, "YOU WIN!\n");
	    fprintf(stdout, "Score: %d\n", score);
	    return false;
	} else if(score == s && nlives == 0) { // Score == s when life was destroyed
	    for(int i = 0; i != 100; i++) {
		fprintf(stdout, "\n");
	    }
	    fprintf(stdout, "YOU LOSE!\n");
	    fprintf(stdout, "Score: %d\n", s);
	    fprintf(stdout, "Lives: 0\n");
	    return false;
	} else { // Generic output
	    nDot = nDot - 1;
	    score = s;
	    for(int i = 0; i != 100; i++) {
		fprintf(stdout, "\n");
	    }
	    fprintf(stdout, "Score: %d\n", score);
	    fprintf(stdout, "Lives: %d\n", nlives);
	    return true;
	}
}

int Board::getScore() {
	// Returns score

	return score;
}

bool Board::killLife() {
    /* Decrements the life count. If the life
     * count is at 0 (after decreasing) outputs
     * a false to indicate that there are no 
     * lives left.
     * */
    
    nlives = nlives - 1;
    if(nlives == 0) 
	return setScore(getScore());
    else {
	return setScore(getScore());
    }
}

void Board::printXY(block * b) {
	// Print statment used in error checking
	// to determine location of pointer at various
	// times in main.

	printf("(%d %d)\n", b->x, b->y);
}

void Board::getXY(block * b, int &px, int &py) {
	/* Returns the x and y of a pointer to use
	 * when moving ghost or pacman or resetting
	 * location when going through teleport.
	 * */

	px = b->x;
	py = b->y;
}
