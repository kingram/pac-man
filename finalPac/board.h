/* board.h */

#ifndef BOARD_H
#define BOARD_H

#include "block.h"
#include "dot.h"
#include "pwrup.h"

class Board {
    public:
	// Constructors
	Board();
	Board(const Board &); // copy constructor
	~Board();
	void delBoard(block *); // recursive function used for delete

	block * block_r(block *, BlockType, block *, block *, block *); // tools for drawing board
	block * block_l(block *, BlockType, block *, block *, block *); // creates block right (r), left
	block * block_u(block *, BlockType, block *, block *, block *); // up and down (based on letters)
	block * block_d(block *, BlockType, block *, block *, block *);
	block * genBlock(block *, block *, block *, block *, bool, bool, bool); // generates new block

	void initBoard(); // draws entire board (with pointers)

	// Access Functions (sets values of block * easily)
	void setVal(block *, bool, bool, bool);
	void setDir(block *, block *, block *, block *, block *);
	void setXY(block *, int, int); 

	bool setScore(int); // increments and sets score
	int getScore(); // outputs score

	// Utility Functions
	bool canMove(block *, direction, ghostName); // detemines if move is valid
	block * move(block *, direction); // actually moves pointer to next location
	ghostName isDead();
	int getDot(); // returns number of dots
	block * getThing(ghostName); // returns pointer (to original location of ghost)

	void renderDots(block *, bool); // sets matrix to initilize blocks on background
	void setRenderer(SDL_Renderer *); // SDL render set function
	void setMatrix(); // initilizes matrix and calls renderDots()

	void printXY(block *); // prints XY values for testing
	void getXY(block *,int&, int&); // determines x and y values for placement of ghost/pacman
	bool killLife(); // removes 

	int	ifDot[18][21]; // array containing all the locations of dots

    private:
	block * pacStart;
	block * blinkyStart;
	block * pinkyStart;
	block * inkyStart;
	block * clydeStart;
	block * telR;
	block * telL;

	block * pacman;
	block * blinky;
	block * pinky;
	block * inky;
	block * clyde;

	int	nPwr; // number of powerups
	int	nDot; // number of dots (to determine if game ends)
	int	score; 
	int	nlives; // number of lives remaining
    
	SDL_Renderer * renderer;
};

#endif
