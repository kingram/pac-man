#include "dot.h"
#include <SDL2/SDL.h>

dot::dot() //default dot constructor
{
	setPosX(0);
	setPosY(0);
	setPIXEL(2);
}

dot::dot(int x, int y) //nondefault dot constructor 
{
	setPosX(x);
	setPosY(y);
	setPIXEL(2);
}

dot::~dot() {}

void dot::setPosX(int x)
{
	posX = x;
	return;
}

void dot::setPosY(int y)
{
	posY = y;
	return;
}

void dot::setPIXEL(int p)
{
	PIXEL = p;
	return;
}

void dot::setRenderer(SDL_Renderer * r)
{
	renderer = r;
	return;
}

void dot::Render() //set the colour and fill in the one rectangle for the dot
{
	SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
	SDL_RenderFillRect(renderer, &Dot);
	return;
}

void dot::UpdateDot() //place dot in the center of a block, instead of in the
{							//top corner, like they are automatically set
	Dot.x = posX + 7*PIXEL;
	Dot.y = posY + 7*PIXEL;
	Dot.w = 2*PIXEL;
	Dot.h = 2*PIXEL;
	return;
}
