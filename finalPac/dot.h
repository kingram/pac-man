#ifndef DOT_H
#define DOT_H

#include <SDL2/SDL.h>

class dot
{
	public:
		// Constructor and Deconstructor
		dot();
		dot(int, int);
		~dot();

		//basic setters
		void setPosX(int);
		void setPosY(int);
		void setPIXEL(int);
		void setRenderer(SDL_Renderer *);

		//functions related to updating and drawing
		void Render();
		void UpdateDot();
	private:
		int posX;
		int posY;
		int PIXEL;
		SDL_Rect Dot; //each dot is made up of only one rectangle
		SDL_Renderer * renderer;
};

#endif
