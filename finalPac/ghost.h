/* ghost.h */

#ifndef GHOST_H
#define GHOST_H

#include <SDL2/SDL.h>
#include <iostream>
#include "board.h"

enum ghostMode
{
	ALIVE,
	EDIBLE,
	DEAD,
	GONE
};
// Enum specifying modes specific to the ghost class

class ghost
{
	public:
		// ---------------------------------
		// | Constructor and Deconstructor |
		// ---------------------------------

		ghost();
		~ghost();

		// ---------------------------
		// | Functions to Set Values |
		// ---------------------------

		void setPosX(int);
		void setPosY(int);
		void setPIXEL(int);
		// Sets ghost position and pixel size

		void setBodyR(int);
		void setBodyG(int);
		void setBodyB(int);
		// Sets body color (changed during implementation)

		void setOriginalBodyR(int);
		void setOriginalBodyG(int);
		void setOriginalBodyB(int);
		// Stores original body color (for revival)

		void setScleraR(int);
		void setScleraG(int);
		void setScleraB(int);
		// Sets color for whites of ghost's eyes

		void setPupilR(int);
		void setPupilG(int);
		void setPupilB(int);
		// Sets pupil color (changed when edible)

		void setOriginalPupilR(int);
		void setOriginalPupilG(int);
		void setOriginalPupilB(int);
		// Stores original pupil color (for revival)

		void setMouthR(int);
		void setMouthG(int);
		void setMouthB(int);
		// Sets mouth color (for edible mode)

		void setSpeed(int);
		void setDirection(direction);
		void setMode(ghostMode);
		void setRenderer(SDL_Renderer *);
		void setWindow(SDL_Window *);
		// Set speed, direction, mode, render, and window

		// ---------------------------------
		// | Functions Related to Movement |
		// ---------------------------------

		void UpdateMouth();
		// Changes rectangles from eye whites to mouth and back

		void UpdateLegs();
		// Switches between leg modes to show movement

		void UpdateGhost(int);
		// Update ghost's apparent position on the board

		void Render();
		// Sends changes to the screen

		void UpdateValues();
		// Updates values based on changed parameters

		void MoveRandom();
		// Moves a ghost in a random direction

		direction RandomDirection();
		// Returns a random direction

		direction generateDirection(int);
		// Generates a direction from the enum based upon an input integer

		int DirToNum (direction);
		// Turns a direction into a number corresponding to the enum

		int OppDirToNum (direction);
		// Turns a direction into a number corresponding to the enum and the opposite direction

		// ---------------------------
		// | Initalization Functions |
		// ---------------------------

		ghost initializeClyde(int, int, SDL_Renderer *, SDL_Window *);
		ghost initializeBlinky(int, int, SDL_Renderer *, SDL_Window *);
		ghost initializePinky(int, int, SDL_Renderer *, SDL_Window *);
		ghost initializeInky(int, int, SDL_Renderer *, SDL_Window *);
		// Initializes each ghost with its unique character values

		// -----------------
		// | Get Functions |
		// -----------------

		direction getDirection();
		// Get current direction

		ghostMode getMode();
		// Gets current ghost mode

		int getPosX();
		int getPosY();
		// Gets current positions 

	private:
		int posX;
		int posY;
		// Stores ghost's position

		int PIXEL;
		// Value that corresponds to the overall size of the ghost

		int bodyR, bodyG, bodyB;
		// Changeable body color storage

		int scleraR, scleraG, scleraB;
		// Changeable eye white color storage

		int pupilR, pupilG, pupilB;
		// Pupil color storage

		int mouthR, mouthG, mouthB;
		// Mouth color storage

		int originalBodyR, originalBodyG, originalBodyB;
		// Storage for original body color

		int originalPupilR, originalPupilG, originalPupilB;
		// Storage for original pupil color

		int speed;
		// Holds the speed of the ghost

		int i;
		int k;
		// Variables used to make animations non-blocking

		direction d;
		// Current ghost direction

		ghostMode mode;
		// Current ghost mode

		SDL_Window * window;
		SDL_Renderer * renderer;
		// Window and renderer

		SDL_Rect line1, line2, line3, line4, line5, line6, line7;
		SDL_Rect line8, line9, line10, line11, line12, line13, line14;
		// Body rectangles	

		SDL_Rect line4a, line5a, line6a, line7a;
		SDL_Rect line10a, line11a, line12a, line13a;
		// Rectangles for whites of eyes and mouth

		SDL_Rect pupil1, pupil2;
		// Rectangles for pupils
};

#endif
