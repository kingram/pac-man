/* main.cpp */

#include <iostream>
#include <SDL2/SDL.h>
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <ctime>

#include "board.h"
#include "pac.h"
#include "back.h"
#include "ghost.h"

#define PIXEL 30
#define DELAY 60

// --------------------
// | GLOBAL VARIABLES |
// --------------------

int posX = 0;
int posY = 0;
// Placement of window

int sizeX = 570;
int sizeY = 660;
// Size of window

int j = 0;
// Helps update movements without switching pointers

bool al;
// Used for death logic

SDL_Window * window;
// Name of window

SDL_Renderer * renderer;
// Name of renderer

// -----------------------
// | FUNCTION PROTOTYPES |
// -----------------------

bool InitEverything();
bool InitSDL();
bool CreateWindow();
bool CreateRenderer();
void SetupRenderer();

// --------
// | MAIN |
// --------

int main(int argc, char * argv[])
{
	srand(time(NULL));
	if (!InitEverything())
		return -1;
	// Ensures the board was initialized properly

	bool begin = true;
	// Used to ensure the ghosts move with the correct logic

	Board board;
	board.setRenderer(renderer);
	block * pman = board.getThing(NONE);
	block * cly = board.getThing(CLYDE);
	block * pin = board.getThing(PINKY);
	block * ink = board.getThing(INKY);
	block * bli = board.getThing(BLINKY);

	block * prevcly = NULL;
	block * prevpin = NULL;
	block * prevink = NULL;
	block * prevbli = NULL;

	back background;
	// Initalization

	background.setWindow(window);
	background.setRenderer(renderer);
	// Sets window and renderer based on the window and renderer

	board.setMatrix();
	for (int i = 0; i < 18; i++)
	{
		for (int j = 0; j < 21; j++)
		{
			background.setMultiplier(i, j, board.ifDot[i][j]);
		}
	}
	// Creates and initializes a matrix of 0s, 1s, and 2s corresponding to dots

	ghost clyde = clyde.initializeClyde(10*PIXEL, 10*PIXEL, renderer, window);
	ghost pinky = pinky.initializePinky(9*PIXEL, 10*PIXEL, renderer, window);
	ghost inky = inky.initializeInky(8*PIXEL, 10*PIXEL, renderer, window);
	ghost blinky = blinky.initializeBlinky(9*PIXEL, 9*PIXEL, renderer, window);
	// Initializes ghosts

	pac pacman;
	pacman.setWindow(window);
	pacman.setRenderer(renderer);
	// Initializes pacman

	background.Render();
	board.renderDots(pman, false);
	clyde.Render();
	pinky.Render();
	inky.Render();
	blinky.Render();
	pacman.Render();
	// Renders everything

	SDL_Event e;
	bool stop = false;
	int pacX;
	int pacY;
	// Initializes values related to pacman's movement

	while (true)
	{
		if (pman == cly || pman == prevcly)
		{
			al = (clyde.getMode() == ALIVE);
			if (clyde.getMode() == EDIBLE)
			{
				board.setScore(board.getScore() + 800);
				clyde.setMode(DEAD);
				clyde.UpdateGhost(DELAY);
			}
		}
		// Checks if pacman has run into clyde
		// Appropriate logic is carried out based upon clyde's current state

		else if (pman == pin || pman == prevpin)
		{
			al = (pinky.getMode() == ALIVE);
			if (pinky.getMode() == EDIBLE)
			{
				board.setScore(board.getScore() + 800);
				pinky.setMode(DEAD);
				pinky.UpdateGhost(DELAY);
			}
		}
		// Checks if pacman has run into pinky
		// Appropriate logic is carried out based upon pinky's current state

		else if (pman == ink || pman == prevink)
		{
			al = (inky.getMode() == ALIVE);
			if (inky.getMode() == EDIBLE)
			{
				board.setScore(board.getScore() + 800);
				inky.setMode(DEAD);
				inky.UpdateGhost(DELAY);
			}
		}
		// Checks if pacman has run into inky
		// Appropriate logic is carried out based upon inky's current state

		else if (pman == bli || pman == prevbli)
		{
			al = (blinky.getMode() == ALIVE);
			if (blinky.getMode() == EDIBLE)
			{
				board.setScore(board.getScore() + 800);
				blinky.setMode(DEAD);
				blinky.UpdateGhost(DELAY);
			}
		}
		// Checks if pacman has run into blinky
		// Appropriate logic is carried out based upon blinky's current state

		if (al)
		{
			clyde.setMode(GONE);
			pinky.setMode(GONE);
			inky.setMode(GONE);
			blinky.setMode(GONE);

			clyde.Render();
			pinky.Render();
			inky.Render();
			blinky.Render();
			SDL_Delay(100);

			pacman.death1();
			SDL_RenderClear(renderer);
			background.Render();
			board.renderDots(pman, true);
			SDL_Delay(100);

			pacman.death2();
			SDL_RenderClear(renderer);
			background.Render();
			board.renderDots(pman, false);
			SDL_Delay(100);

			pacman.death3();
			SDL_RenderClear(renderer);
			background.Render();
			board.renderDots(pman, true);
			SDL_Delay(100);

			pacman.death4();
			SDL_RenderClear(renderer);
			background.Render();
			board.renderDots(pman, false);
			SDL_Delay(100);

			pacman.death5();
			SDL_RenderClear(renderer);
			background.Render();
			board.renderDots(pman, true);
			SDL_Delay(100);

			pacman.death6();
			SDL_RenderClear(renderer);
			background.Render();
			board.renderDots(pman, false);
			SDL_Delay(100);

			pacman.death7();
			SDL_RenderClear(renderer);
			background.Render();
			board.renderDots(pman, true);
			SDL_Delay(100);

			pacman.setPosX(9*30);
			pacman.setPosY(16*30);
			pacman.setDir(RIGHT);
			pman = board.getThing(NONE);
			pacman.setSpeedx(0);
			pacman.setSpeedy(0);
			pacman.UpdateValues();
			pacman.setDir(NOMOVE);

			clyde.setMode(ALIVE);
			clyde.setPosX(10*PIXEL);
			clyde.setPosY(10*PIXEL);
			cly = board.getThing(CLYDE);
			begin = true;

			pinky.setMode(ALIVE);
			pinky.setPosX(9*PIXEL);
			pinky.setPosY(10*PIXEL);
			pin = board.getThing(PINKY);

			inky.setMode(ALIVE);
			inky.setPosX(8*PIXEL);
			inky.setPosY(10*PIXEL);
			ink = board.getThing(INKY);

			blinky.setMode(ALIVE);
			blinky.setPosX(9*PIXEL);
			blinky.setPosY(9*PIXEL);
			bli = board.getThing(BLINKY);			
			
			if(!board.killLife())
			    break; 
			// Pacman has run out of lives... #dead

			al = false;
		}

		/* If the ghost is alive when pacman runs into it, the death sequence,
		 * which is the above code, runs*/ 

		while (SDL_PollEvent(&e) != 0)
		{
			if (e.type == SDL_QUIT)
				return 1;
			else if (e.type == SDL_KEYDOWN)
			{
				switch (e.key.keysym.sym)
				{
					case SDLK_UP:
						if (board.canMove(pman, UP, NONE))
						{
							stop = false;
							board.getXY(pman, pacX, pacY);
							pacman.setPosX(pacX);
							pacman.setPosY(pacY);	
							pacman.moveU();
						}
						else {
							stop = true;
							pacman.moveU();
						}
						break;
						// Logic for when pacman is prompted to move upward

					case SDLK_DOWN:
						if (board.canMove(pman, DOWN, NONE))
						{
							stop = false;
							board.getXY(pman, pacX, pacY);
							pacman.setPosX(pacX);
							pacman.setPosY(pacY);
							pacman.moveD();
						}
						else {
							stop = true;
							pacman.moveD();
						}
						break;
						// Logic for when pacman is prompted to move downward

					case SDLK_LEFT:
						if (board.canMove(pman, LEFT, NONE))
						{
							board.getXY(pman, pacX, pacY);
							pacman.setPosX(pacX);
							pacman.setPosY(pacY);
							stop = false;
							pacman.moveL();
						}
						else {
							stop = true;
							pacman.moveL();
						}
						break;
						// Logic for when pacman is prompted to move to the left

					case SDLK_RIGHT:
						if (board.canMove(pman, RIGHT, NONE))
						{
							board.getXY(pman, pacX, pacY);
							pacman.setPosX(pacX);
							pacman.setPosY(pacY);
							stop = false;
							pacman.moveR();
						}
						else {
							stop = true;
							pacman.moveR();
						}
						break;
						// Logic for when pacman is prompted to move to the right

					case SDLK_q:
						SDL_Quit();
						return 0;
						break;
						// Game quits when `q` is pressed

					default:
						break;
				}
			}
		}

		if (begin || j == 15)
		// Updates pointers if it is the beginning, or 15 movements have happened
		{
			begin = false;
			// Set that it is no longer the beginning
			if (board.canMove(cly, clyde.getDirection(), CLYDE))
			{
				direction prevDir = clyde.getDirection();
				for (int j = 0; j < 4; j++)
				{
					if (j != clyde.DirToNum(prevDir) &&
					    j != clyde.OppDirToNum(prevDir))
					{
						direction i = clyde.generateDirection(j);
						if (board.canMove(cly, i, CLYDE))
						{
							if (rand() % 2)
								clyde.setDirection(i);
						}
					}
				}
				prevcly = cly;
				cly = board.move(cly, clyde.getDirection());
			}
			else
			{
				direction dir = clyde.RandomDirection();
				while (!board.canMove(cly, dir, CLYDE))
					dir = clyde.RandomDirection();
				prevcly = cly;
				cly = board.move(cly, dir);
				clyde.setDirection(dir);
			}
			// Changes clyde's direction if possible

			if (board.canMove(bli, blinky.getDirection(), BLINKY))
			{
				direction prevDir = blinky.getDirection();
				for (int j = 0; j < 4; j++)
				{
					if (j != blinky.DirToNum(prevDir) &&
		 			    j != blinky.OppDirToNum(prevDir))
					{
						direction i = blinky.generateDirection(j);
						if (board.canMove(bli, i, BLINKY))
						{
							if (rand() % 2)
								blinky.setDirection(i);
						}
					}
				}
				prevbli = bli;
				bli = board.move(bli, blinky.getDirection());
			}
			else
			{
				direction dir = blinky.RandomDirection();
				while (!board.canMove(bli, dir, BLINKY))
					dir = blinky.RandomDirection();
				prevbli = bli;
				bli = board.move(bli, dir);
				blinky.setDirection(dir);
			}
			// Changes blinky's direction if possible

			if (board.canMove(pin, pinky.getDirection(), PINKY))
			{
				direction prevDir = pinky.getDirection();
				for (int j = 0; j < 4; j++)
				{
					if (j != pinky.DirToNum(prevDir) &&
					    j != pinky.OppDirToNum(prevDir))
					{
						direction i = pinky.generateDirection(j);
						if (board.canMove(pin, i, PINKY))
						{
							if (rand() % 2)
								pinky.setDirection(i);
						}
					}
				}
				prevpin = pin;
				pin = board.move(pin, pinky.getDirection());
			}
			else
			{
				direction dir = pinky.RandomDirection();
				while (!board.canMove(pin, dir, PINKY))
					dir = pinky.RandomDirection();
				prevpin = pin;
				pin = board.move(pin, dir);
				pinky.setDirection(dir);
			}
			// Changes pinky's direction if possible

			if (board.canMove(ink, inky.getDirection(), INKY))
			{
				direction prevDir = inky.getDirection();
				for (int j = 0; j < 4; j++)
				{
					if (j != inky.DirToNum(prevDir) &&
					    j != inky.OppDirToNum(prevDir))
						continue;
					{
						direction i = inky.generateDirection(j);
						if (board.canMove(ink, i, INKY))
						{
							if (rand() % 2)
								inky.setDirection(i);
						}
					}
				}
				prevink = ink;
				ink = board.move(ink, inky.getDirection());
			}
			else
			{
				direction dir = inky.RandomDirection();
				while (!board.canMove(ink, dir, INKY))
					dir = inky.RandomDirection();
				prevink = ink;
				ink = board.move(ink, dir);
				inky.setDirection(dir);
			}
			// Changes inky's directoin if possible

			if (board.canMove(pman, pacman.getDirection(), NONE))
			{
				pman = board.move(pman, pacman.getDirection());
			}
			else
			{
				pman = board.move(pman, NOMOVE);
				stop = true;	
			}
			// Ensures pacman can continue moving
			// Pacman stops moving if he cannot move to the specified location

			j = 0;
		}

		board.getXY(pman, pacX, pacY);
		// Updates the pacman pointer

	        if(pacX == 2*30 && pacY == 10*30 && pacman.getDirection() == RIGHT)
		{
			// Has now gone through magic teleporting tube
			pacman.setPosX(pacX+30);
			pacman.setPosY(pacY);
		    	stop = false;
		    	pacman.moveR();
		}
		else if(pacX == 16*30 && pacY == 10*30 && pacman.getDirection() == LEFT) 
		{
		 	// Has now gone through magic teleporing tube
		    	pacman.setPosX(pacX-30);
		    	pacman.setPosY(pacY);
		    	stop = false;
		    	pacman.moveL();
		}

		clyde.UpdateGhost(DELAY);
		blinky.UpdateGhost(DELAY);
		pinky.UpdateGhost(DELAY);
		inky.UpdateGhost(DELAY);
		j++;

		// Changes ghost locations

		SDL_RenderClear(renderer);
		background.Render();
		board.renderDots(pman, true);
		clyde.Render();
		blinky.Render();
		pinky.Render();
		inky.Render();

		// Renders all the things

		if (!stop)
		{
			pacman.executeMovement();
		}
		else
		{
			pacman.stopMovement();
			pman = board.move(pman, NOMOVE);
		}
		// Moves pacman if doing so is allowed

		SDL_Delay(DELAY/2);

		int dotx;
		int doty;
		board.getXY(pman, dotx, doty);
		// Set dotx and doty to the values of pacman's pointer location

		if (background.getMultiplier(dotx/30, doty/30) == 2
		    && clyde.getMode() != DEAD && blinky.getMode() != DEAD
		    && pinky.getMode() != DEAD && inky.getMode() != DEAD)
		{
			clyde.setMode(EDIBLE);
			blinky.setMode(EDIBLE);
			pinky.setMode(EDIBLE);
			inky.setMode(EDIBLE);
			board.setScore(board.getScore() + 350);
		}
		// Sets ghosts to edible and increases score when Pacman encounters a powerup
 
		else if(background.getMultiplier(dotx/30, doty/30) == 1) 
		{
			if(board.setScore(board.getScore() + 100) == false)
				break;
		}
		// Increments the score if a normal dot is encountered

		background.setMultiplier(dotx/30, doty/30, 0);
		// Sets the previously processed value to zero

		if (!board.canMove(pman, pacman.getDirection(), NONE))
			stop = true;
		// Stop pacman's motion if he cannot move in the specified direction

		if (!stop)
		{
			if (j == 15)
			// Same logic as above, but it can only be the beginning once
			{
				if (board.canMove(cly, clyde.getDirection(), CLYDE))
				{
					direction prevDir = clyde.getDirection();
					for (int j = 0; j < 4; j++)
					{
						if (j != clyde.DirToNum(prevDir) &&
						    j != clyde.OppDirToNum(prevDir))
						{
							direction i = clyde.generateDirection(j);
							if (board.canMove(cly, i, CLYDE))
							{
								if (rand() % 2)
									clyde.setDirection(i);
							}
						}
					}
					prevcly = cly;
					cly = board.move(cly, clyde.getDirection());
				}
				else
				{
					direction dir = clyde.RandomDirection();
					while (!board.canMove(cly, dir, CLYDE))
						dir = clyde.RandomDirection();
					prevcly = cly;
					cly = board.move(cly, dir);
					clyde.setDirection(dir);
				}
				// Changes clyde's direction if possible

				if (board.canMove(bli, blinky.getDirection(), BLINKY))
				{
					direction prevDir = blinky.getDirection();
					for (int j = 0; j < 4; j++)
					{
						if (j != blinky.DirToNum(prevDir) &&
						    j != blinky.OppDirToNum(prevDir))
						{
							direction i = blinky.generateDirection(j);
							if (board.canMove(bli, i, BLINKY))
							{
								if (rand() % 2)
									blinky.setDirection(i);
							}
						}
					}
					prevbli = bli;
					bli = board.move(bli, blinky.getDirection());
				}
				else
				{
					direction dir = blinky.RandomDirection();
					while (!board.canMove(bli, dir, BLINKY))
						dir = blinky.RandomDirection();
					prevbli = bli;
					bli = board.move(bli, dir);
					blinky.setDirection(dir);
				}
				// Changes blinky's direction if possible

				if (board.canMove(pin, pinky.getDirection(), PINKY))
				{
					direction prevDir = pinky.getDirection();
					for (int j = 0; j < 4; j++)
					{
						if (j != pinky.DirToNum(prevDir) &&
						    j != pinky.OppDirToNum(prevDir))
						{
							direction i = pinky.generateDirection(j);
							if (board.canMove(pin, i, PINKY))
							{
								if (rand() % 2)
									pinky.setDirection(i);
							}
						}
					}
					prevpin = pin;
					pin = board.move(pin, pinky.getDirection());
				}
				else
				{
					direction dir = pinky.RandomDirection();
					while (!board.canMove(pin, dir, PINKY))
						dir = pinky.RandomDirection();
					prevpin = pin;
					pin = board.move(pin, dir);
					pinky.setDirection(dir);
				}
				// Changes pinky's direction if possible

				if (board.canMove(ink, inky.getDirection(), INKY))
				{
					direction prevDir = inky.getDirection();
					for (int j = 0; j < 4; j++)
					{
						if (j != inky.DirToNum(prevDir) &&
						    j != inky.OppDirToNum(prevDir))
						{
							direction i = inky.generateDirection(j);
							if (board.canMove(ink, i, INKY))
							{
								if (rand() % 2)
									inky.setDirection(i);
							}
						}
					}
					prevink = ink;
					ink = board.move(ink, inky.getDirection());
				}
				else
				{
					direction dir = inky.RandomDirection();
					while (!board.canMove(ink, dir, INKY))
						dir = inky.RandomDirection();
					prevink = ink;
					ink = board.move(ink, dir);
					inky.setDirection(dir);
				}
				// Changes inky's direction if possible

				if (board.canMove(pman, pacman.getDirection(), INKY))
				{
					pman = board.move(pman, pacman.getDirection());
				}
				else
				{
					pman = board.move(pman, NOMOVE);
					stop = true;
				}
				// Ensures pacman can continue moving
				// Pacman stops moving if he cannot move to the specified location

				j = 0;
			}

			clyde.UpdateGhost(DELAY);
			blinky.UpdateGhost(DELAY);
			pinky.UpdateGhost(DELAY);
			inky.UpdateGhost(DELAY);
			pacman.midMouth();
			j++;

			// Changes ghost locations

			SDL_RenderClear(renderer);
			background.Render();
			board.renderDots(pman, false);
			clyde.Render();
			blinky.Render();
			pinky.Render();
			inky.Render();
			pacman.Render();

			// Renders all the things

			SDL_Delay(DELAY/2);
		}
	}

	SDL_Delay(1000);

	// Keeps the window open after you win/lose

	SDL_QuitSubSystem(SDL_INIT_EVERYTHING);
	SDL_Quit();
	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(renderer);

	// Cleans up

	return 0;
}

// ----------------------------
// | FUNCTION IMPLEMENTATIONS |
// ----------------------------

/* Note: the code below was taken from a source online cited in
 * the acknowledgement section of our report */

bool InitEverything()
{
	if (!InitSDL())
		return false;
	if (!CreateWindow())
		return false;
	if (!CreateRenderer())
		return false;

	SetupRenderer();
 	return true;
}

bool InitSDL()
{
	if (SDL_Init(SDL_INIT_EVERYTHING) == -1)
	{
		std::cout << "Failed to initialize SDL : ";
		std::cout << SDL_GetError() << std::endl;
		return false;
	}

	return true;
}

bool CreateWindow()
{
	window = SDL_CreateWindow("Pacman", posX, posY, sizeX, sizeY, 0);

	if (window == NULL)
	{
		std::cout << "Failed to create window : ";
		std::cout << SDL_GetError() << std::endl;
		return false;
	}

	return true;
}

bool CreateRenderer()
{
	renderer = SDL_CreateRenderer(window, -1, 0);

	if (renderer == NULL)
	{
		std::cout << "Failed to create renderer : ";
		std::cout << SDL_GetError() << std::endl;
		return false;
	}

	return true;
}

void SetupRenderer()
{
	SDL_RenderSetLogicalSize(renderer, sizeX, sizeY);
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
}

