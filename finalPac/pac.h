#ifndef PAC_H
#define PAC_H

#include <SDL2/SDL.h>
#include <iostream>
#include "board.h"

#define deathDELAY 100 //delay for each phase of the death sequence

class pac {
  public:
	// constructor/deconstructor
	pac();
	~pac();

	pac initPac(SDL_Window*, SDL_Renderer*); //set init. pac to corrent window/
													//renderer	
	// getters and setters
	void setPosX(int);							
	void setPosY(int);
	void setPIXEL(int);
	void setSpeedx(int);
	void setSpeedy(int);
	int getSpeedx();
	int getSpeedy();
	int getPosX();
	int getPosY();
	void setDir(direction);
	direction getDirection();
	
	//functions related to SDL/rendering
	void UpdateValues();
	void Render();
	void setRenderer(SDL_Renderer *);
	void setWindow(SDL_Window *);

	//set speed and directions based on desired direction
	void moveR();
	void moveL();
	void moveD();
	void moveU();

	//pacman drawn at the halfway chomping point
	void midMouth();

	//movement wrapper functions
	void executeMovement();
	void stopMovement();
	
	//functions for each of the phases of the death sequence
	void death1();
	void death2();
	void death3();
	void death4();
	void death5();
	void death6();
	void death7();

  private:
	int DELAY;
	int posX;
	int posY;
	int PIXEL;
	int xspeed;
	int yspeed;
	int defspeed; //default speed, used in the moveR/L/D/U functions
	direction d;

	//SDL window and renderer
	SDL_Window * window;
	SDL_Renderer * renderer;

	//lines that compose pacman
	SDL_Rect line1,line2,line3,line4,line5,line6,line7,line8,line9,line10;
	SDL_Rect line11,line12,line13;
	

};

#endif
