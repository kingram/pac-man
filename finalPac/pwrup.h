#ifndef PWRUP_H
#define PWRUP_H

#include <SDL2/SDL.h>

class Pwrup { //powerup class (the special dots that cause the ghosts to become
  public:		//edible
	Pwrup();
	~Pwrup();

	//setters
	void setXY(int, int);
	void setPix(int);
	void setRenderer(SDL_Renderer *);

	//functions to update and draw powerups
	void render();
	void updatePwr();
    
  private:
	int x, y, pix;
	SDL_Rect bigDot, hRect, vRect; //powerup made of three rectangles
	SDL_Renderer * renderer;
};

#endif
