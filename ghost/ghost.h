/* ghost.h
 *
 * This file contains function headers for the ghost class.
 *
 */

#ifndef GHOST_H
#define GHOST_H

#include <SDL2/SDL.h>
#include <iostream>

enum ghostMode
{
	ALIVE,
	EDIBLE,
	DEAD,
	GONE
};

enum direction
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

class ghost
{
	public:
		// Constructor and Deconstructor
		ghost();
		~ghost();

		// Functions to set values
		void setPosX(int);
		void setPosY(int);
		void setPIXEL(int);

		void setBodyR(int);
		void setBodyG(int);
		void setBodyB(int);

		void setOriginalBodyR(int);
		void setOriginalBodyG(int);
		void setOriginalBodyB(int);

		void setScleraR(int);
		void setScleraG(int);
		void setScleraB(int);

		void setPupilR(int);
		void setPupilG(int);
		void setPupilB(int);

		void setOriginalPupilR(int);
		void setOriginalPupilG(int);
		void setOriginalPupilB(int);

		void setMouthR(int);
		void setMouthG(int);
		void setMouthB(int);

		void setSpeed(int);
		void setDirection(direction);
		void setMode(ghostMode);
		void setRenderer(SDL_Renderer *);

		// Functions related to ghost movement
		void UpdateGhost(int);
		void UpdateLegs();
		void UpdateMouth();
		void Render();
		void UpdateValues();
		void NewDirection();
		direction RandomDirection();

		ghost initializeClyde(int, int, SDL_Renderer *);
		ghost initializeBlinky(int, int, SDL_Renderer *);
		ghost initializePinky(int, int, SDL_Renderer *);
		ghost initializeInky(int, int, SDL_Renderer *);

	private:
		int posX;
		int posY;
		int PIXEL;
		int bodyR, bodyG, bodyB;
		int scleraR, scleraG, scleraB;
		int pupilR, pupilG, pupilB;
		int mouthR, mouthG, mouthB;
		int originalBodyR, originalBodyG, originalBodyB;
		int originalPupilR, originalPupilG, originalPupilB;
		int speed;
		int i;
		direction d;
		ghostMode mode;
		SDL_Renderer * renderer;
		SDL_Rect line1, line2, line3, line4, line5, line6, line7;
		SDL_Rect line8, line9, line10, line11, line12, line13, line14;
		SDL_Rect line4a, line5a, line6a, line7a;
		SDL_Rect line10a, line11a, line12a, line13a;
		SDL_Rect pupil1, pupil2;
};

#endif
