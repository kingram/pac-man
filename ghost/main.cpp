// Lots of things are still wrong with this code.

#include "ghost.h"
#include <SDL2/SDL.h>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <ctime>

SDL_Window * window;
SDL_Renderer * renderer;
int posX = 400;
int posY = 400;
int sizeX = 600;
int sizeY = 600;

#define DELAY 60

bool InitSDL()
{
        if ( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
        {
                std::cout << " Failed to initialize SDL : " << SDL_GetError() << std::endl;
                return false;
        }

        return true;
}
bool CreateWindow()
{
        window = SDL_CreateWindow( "Server", posX, posY, sizeX, sizeY, 0 );

        if ( window == NULL )
        {
                std::cout << "Failed to create window : " << SDL_GetError();
                return false;
        }

        return true;
}
bool CreateRenderer()
{
        renderer = SDL_CreateRenderer( window, -1, 0 );

        if ( renderer == NULL )
        {
                std::cout << "Failed to create renderer : " << SDL_GetError();
                return false;
        }

        return true;
}
void SetupRenderer()
{
        // Set size of renderer to the same as window
        SDL_RenderSetLogicalSize( renderer, sizeX, sizeY );
        
        // Set color of renderer to black 
        SDL_SetRenderDrawColor( renderer, 0, 0, 0, 255 );
}

bool InitEverything()
{
        if ( !InitSDL() )
                return false;

        if ( !CreateWindow() )
                return false;

        if ( !CreateRenderer() )
                return false;

        SetupRenderer();

        return true;
}


int main( int argc, char* args[] )
{
	srand (time(NULL));
	if (!InitEverything())
		return -1;

	ghost clyde = clyde.initializeClyde(200, 200, renderer);
	ghost blinky = blinky.initializeBlinky(400, 200, renderer);
	ghost pinky = pinky.initializePinky(200, 400, renderer);
	ghost inky = inky.initializeInky(400, 400, renderer);

	for (int i = 0; i < 2; i++)
        {
	        for (int i = 0; i < 10; i++)
	        {
			clyde.NewDirection();
			blinky.NewDirection();
			pinky.NewDirection();
			inky.NewDirection();

			for (int j = 0; j < 15; j++)
			{
				clyde.UpdateGhost(DELAY);
				blinky.UpdateGhost(DELAY);
				pinky.UpdateGhost(DELAY);
				inky.UpdateGhost(DELAY);

				SDL_RenderClear(renderer);

				clyde.Render();
				blinky.Render();
				pinky.Render();
				inky.Render();
			
				SDL_Delay(DELAY);
			}
	        }

		clyde.setMode(EDIBLE);
		blinky.setMode(EDIBLE);
		pinky.setMode(EDIBLE);
		inky.setMode(EDIBLE);

		for (int i = 0; i < 10; i++)
		{
	                clyde.NewDirection();
        	        blinky.NewDirection();
        	        pinky.NewDirection();
                        inky.NewDirection();

			for (int j = 0; j < 15; j++)
			{
        	                clyde.UpdateGhost(DELAY);
                	        blinky.UpdateGhost(DELAY);
                        	pinky.UpdateGhost(DELAY);
	                        inky.UpdateGhost(DELAY);

				SDL_RenderClear(renderer);

				clyde.Render();
				blinky.Render();
				pinky.Render();
				inky.Render();

				SDL_Delay(DELAY);
			}
			if (i == 2) clyde.setMode(DEAD);
		}

                clyde.setMode(DEAD);
                blinky.setMode(DEAD);
                pinky.setMode(DEAD);
                inky.setMode(DEAD);

		for (int i = 0; i < 10; i++)
		{
                        clyde.NewDirection();
                        blinky.NewDirection();
                        pinky.NewDirection();
                        inky.NewDirection();

			for (int j = 0; j < 15; j++)
			{
        	                clyde.UpdateGhost(DELAY);
	                        blinky.UpdateGhost(DELAY);
                        	pinky.UpdateGhost(DELAY);
                	        inky.UpdateGhost(DELAY);

        	                SDL_RenderClear(renderer);

	                        clyde.Render();
                        	blinky.Render();
                	        pinky.Render();
        	                inky.Render();

	                        SDL_Delay(DELAY);
			}
                }
                clyde.setMode(ALIVE);
                blinky.setMode(ALIVE);
                pinky.setMode(ALIVE);
                inky.setMode(ALIVE);
        }
	return 0;
}

