
#include "pac.h"
#include <SDL2/SDL.h>
#include <iostream>
#include <cmath>

SDL_Window * window;
SDL_Renderer * renderer;
int posX = 400;
int posY = 400;
int sizeX = 600;
int sizeY = 600;


bool InitSDL()
{
        if ( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
        {
                std::cout << " Failed to initialize SDL : " << SDL_GetError() << std::endl;
                return false;
        }

        return true;
}

bool CreateWindow()
{
        window = SDL_CreateWindow( "Server", posX, posY, sizeX, sizeY, 0 );

        if ( window == NULL )
        {
                std::cout << "Failed to create window : " << SDL_GetError();
                return false;
        }

        return true;
}

bool CreateRenderer()
{
        renderer = SDL_CreateRenderer( window, -1, 0 );

        if ( renderer == NULL )
        {
                std::cout << "Failed to create renderer : " << SDL_GetError();
                return false;
        }

        return true;
}
void SetupRenderer()
{
        // Set size of renderer to the same as window
        SDL_RenderSetLogicalSize( renderer, sizeX, sizeY );
        
        // Set color of renderer to black 
        SDL_SetRenderDrawColor( renderer, 0, 0, 0, 255 );
}

bool InitEverything()
{
        if ( !InitSDL() )
                return false;

        if ( !CreateWindow() )
                return false;

        if ( !CreateRenderer() )
                return false;

        SetupRenderer();

        return true;
}


int main( int argc, char* args[] )
{
	if (!InitEverything()) {
		return -1;
	}

	pac Pacman;

	Pacman.setRenderer(renderer);
	Pacman.UpdateValues();
	Pacman.Render();
//	SDL_Delay(5000);

/*	for (int i=0; i<100; i++) {
		Pacman.UpdateValues();
		Pacman.Render();
		SDL_Delay(50);
	}*/

	bool stop = false;	
	SDL_Event e;
	for (int i= 0; i<1000; i++) {
		//SDL_PollEvent(&e);
		
		while (SDL_PollEvent( &e) != 0) {
			if (e.type == SDL_QUIT) {
				return 1;
			} else if (e.type == SDL_KEYDOWN ) {
				switch (e.key.keysym.sym) {

					case SDLK_UP:
						Pacman.moveU();
						stop = false;
					break;

					case SDLK_DOWN:
						Pacman.moveD();
						stop = false;
					break;

					case SDLK_LEFT:
						Pacman.moveL();
						stop = false;
					break;

					case SDLK_RIGHT:
						Pacman.moveR();
						stop = false;
					break;

					case SDLK_SPACE:
						stop = true;
					break;

					case SDLK_d:
						Pacman.death();
						Pacman.setPIXEL(0);
						stop = true;
					break;

					case SDLK_r: //revive pacman after death
						Pacman.setPIXEL(5);
						stop = true;
					break;

					default:
					break;
 
	
				}
			}
		

		}

		if (!stop) { 
			Pacman.executeMovement();
		} else {
			Pacman.stopMovement();
		}
		
		//if you want to stop, use "Pacman.stopMovement()" function
		
	} 

	return 0;
}

