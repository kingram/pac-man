#ifndef PAC_H
#define PAC_H

#include <SDL2/SDL.h>
#include <iostream>

#define deathDELAY 100

enum direction {
	RIGHT,
	LEFT,
	UP,
	DOWN

};

class pac {
  public:
	pac();
	~pac();

	void setPosX(int);
	void setPosY(int);
	void setPIXEL(int);
	void setSpeedx(int);
	void setSpeedy(int);
	int getSpeedx();
	int getSpeedy();
	void setDir(direction);
	
	void UpdateValues();
	void Render();
	void setRenderer(SDL_Renderer *);

	void moveR();
	void moveL();
	void moveD();
	void moveU();

	void midMouth();
//	void endMouth();

	void executeMovement();
	void stopMovement();
	void death();
  private:
	int DELAY;
	int posX;
	int posY;
	int PIXEL;
	int xspeed;
	int yspeed;
	int defspeed;
	direction d;
	SDL_Renderer * renderer;
	SDL_Rect line1,line2,line3,line4,line5,line6,line7,line8,line9,line10;
	SDL_Rect line11,line12,line13;
	

};

#endif
