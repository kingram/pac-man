#include <SDL2/SDL.h>
#include <iostream>

#define PIXEL 5

int posX = 300;
int posY= 300;
int sizeX = 600;
int sizeY = 600;

SDL_Window* window;
SDL_Renderer* renderer;

bool InitEverything();
bool InitSDL();
bool CreateWindow();
bool CreateRenderer();
void SetupRenderer();

void Render();

SDL_Rect line1,line2,line3,line4,line5a,line6a,line7a,line8a,line9a,line10a;
SDL_Rect line11a,line12a,line5b,line6b,line7b,line8b,line9b,line10b,line11b;
SDL_Rect line12b;

int main(int argc, char* args[] ) {
	if (!InitEverything() ) {
		return -1;
	}

	line1.x = PIXEL + posX;
	line1.y = 5*PIXEL + posY;
	line1.w = PIXEL;
	line1.h = 5*PIXEL;

	line2.x = 2*PIXEL + posX;
	line2.y = 3*PIXEL + posY;
	line2.w = PIXEL;
	line2.h = 9*PIXEL;

	line3.x = 3*PIXEL + posX;
	line3.y = 2*PIXEL + posY;
	line3.w = PIXEL;
	line3.h = 11*PIXEL;

	line4.x = 4*PIXEL + posX;
	line4.y = 2*PIXEL + posY;
	line4.w = PIXEL;
	line4.h = 11*PIXEL;

	line5a.x = 5*PIXEL + posX;
	line5a.y = PIXEL + posY;
	line5a.w = PIXEL;
	line5a.h = 6*PIXEL;

	line6a.x = 6*PIXEL + posX;
	line6a.y = PIXEL + posY;
	line6a.w = PIXEL;
	line6a.h = 6*PIXEL;

	line7a.x = 7*PIXEL + posX;
	line7a.y = PIXEL + posY;
	line7a.w = PIXEL;
	line7a.h = 6*PIXEL;

	line8a.x = 8*PIXEL + posX;
	line8a.y = PIXEL + posY;
	line8a.w = PIXEL;
	line8a.h = 5*PIXEL;

	line9a.x = 9*PIXEL + posX;
	line9a.y = PIXEL + posY;
	line9a.w = PIXEL;
	line9a.h = 5*PIXEL;

	line10a.x = 10*PIXEL + posX;
	line10a.y = 2*PIXEL + posY;
	line10a.w = PIXEL;
	line10a.h = 4*PIXEL;

	line11a.x = 11*PIXEL + posX;
	line11a.y = 2*PIXEL + posY;
	line11a.w = PIXEL;
	line11a.h = 3*PIXEL;

	line12a.x = 12*PIXEL + posX;
	line12a.y = 3*PIXEL + posY;
	line12a.w = PIXEL;
	line12a.h = 2*PIXEL;

	line5b.x = 5*PIXEL + posX;
	line5b.y = 8*PIXEL + posY;
	line5b.w = PIXEL;
	line5b.h = 6*PIXEL;

	line6b.x = 6*PIXEL + posX;
	line6b.y = 8*PIXEL + posY;
	line6b.w = PIXEL;
	line6b.h = 6*PIXEL;

	line7b.x = 7*PIXEL + posX;
	line7b.y = 8*PIXEL + posY;
	line7b.w = PIXEL;
	line7b.h = 6*PIXEL;

	line8b.x = 8*PIXEL + posX;
	line8b.y = 9*PIXEL + posY;
	line8b.w = PIXEL;
	line8b.h = 5*PIXEL;

	line9b.x = 9*PIXEL + posX;
	line9b.y = 9*PIXEL + posY;
	line9b.w = PIXEL;
	line9b.h = 5*PIXEL;

	line10b.x = 10*PIXEL + posX;
	line10b.y = 9*PIXEL + posY;
	line10b.w = PIXEL;
	line10b.h = 4*PIXEL;

	line11b.x = 11*PIXEL + posX;
	line11b.y = 10*PIXEL + posY;
	line11b.w = PIXEL;
	line11b.h = 3*PIXEL;

	line12b.x = 12*PIXEL + posX;
	line12b.y = 10*PIXEL + posY;
	line12b.w = PIXEL;
	line12b.h = 2*PIXEL;

	Render();
	SDL_Delay(4000);
}

void Render() {
	SDL_SetRenderDrawColor(renderer, 127,127,127,255);
	SDL_RenderClear(renderer);
	SDL_SetRenderDrawColor(renderer,255,255,0,255);
	
	SDL_RenderFillRect(renderer, &line1);
	SDL_RenderFillRect(renderer, &line2);
	SDL_RenderFillRect(renderer, &line3);
	SDL_RenderFillRect(renderer, &line4);
	SDL_RenderFillRect(renderer, &line5a);
	SDL_RenderFillRect(renderer, &line6a);
	SDL_RenderFillRect(renderer, &line7a);
	SDL_RenderFillRect(renderer, &line8a);
	SDL_RenderFillRect(renderer, &line9a);
	SDL_RenderFillRect(renderer, &line10a);
	SDL_RenderFillRect(renderer, &line11a);
	SDL_RenderFillRect(renderer, &line12a);

	SDL_RenderFillRect(renderer, &line5b);
	SDL_RenderFillRect(renderer, &line6b);
	SDL_RenderFillRect(renderer, &line7b);
	SDL_RenderFillRect(renderer, &line8b);
	SDL_RenderFillRect(renderer, &line9b);
	SDL_RenderFillRect(renderer, &line10b);
	SDL_RenderFillRect(renderer, &line11b);
	SDL_RenderFillRect(renderer, &line12b);
	
	SDL_RenderPresent(renderer);

}

bool InitEverything()
{
	if ( !InitSDL() )
		return false;

	if ( !CreateWindow() )
		return false;

	if ( !CreateRenderer() )
		return false;

	SetupRenderer();

	return true;
}
bool InitSDL()
{
	if ( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
	{
		std::cout << " Failed to initialize SDL : " << SDL_GetError() << std::endl;
		return false;
	}

	return true;
}
bool CreateWindow()
{
	window = SDL_CreateWindow( "Server", posX, posY, sizeX, sizeY, 0 );

	if ( window == NULL )
	{
		std::cout << "Failed to create window : " << SDL_GetError();
		return false;
	}

	return true;
}
bool CreateRenderer()
{
	renderer = SDL_CreateRenderer( window, -1, 0 );

	if ( renderer == NULL )
	{
		std::cout << "Failed to create renderer : " << SDL_GetError();
		return false;
	}

	return true;
}
void SetupRenderer()
{
	// Set size of renderer to the same as window
		SDL_RenderSetLogicalSize( renderer, sizeX, sizeY );
	// Set color of renderer to black
		SDL_SetRenderDrawColor( renderer, 0, 0, 0, 255 );
}
