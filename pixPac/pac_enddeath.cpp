#include <SDL2/SDL.h>
#include <iostream>
#include <SDL_ttf.h>

#define PIXEL 5

int posX = 300;
int posY= 300;
int sizeX = 600;
int sizeY = 600;

SDL_Window* window;
SDL_Renderer* renderer;

bool InitEverything();
bool InitSDL();
bool CreateWindow();
bool CreateRenderer();
void SetupRenderer();

void Render();

SDL_Rect line1,line2,line3,line4,line5,line6,line7,line8,line9,line10;
SDL_Rect line11,line12,line13;

int main(int argc, char* args[] ) {
	if (!InitEverything() ) {
		return -1;
	}

	int centerx = 8*PIXEL + posX;
	int centery = 10*PIXEL + posX;

	line1.x = centerx + 2*PIXEL;
	line1.y = centery - 2*PIXEL;
	line1.w = PIXEL;
	line1.h = PIXEL;

	line2.x = centerx + 3*PIXEL;
	line2.y = centery + 3*PIXEL;
	line2.w = PIXEL;
	line2.h = PIXEL;

	line3.x = centerx + 2*PIXEL;
	line3.y = centery + 2*PIXEL;
	line3.w = PIXEL;
	line3.h = PIXEL;

	line4.x = centerx + PIXEL;
	line4.y = centery - PIXEL;
	line4.w = PIXEL;
	line4.h = PIXEL;

	line5.x = centerx - PIXEL;
	line5.y = centery + PIXEL;
	line5.w = PIXEL;
	line5.h = 1*PIXEL;

	line6.x = centerx + PIXEL;
	line6.y = centery + PIXEL;
	line6.w = PIXEL;
	line6.h = PIXEL;

	line7.x = centerx - PIXEL; 
	line7.y = centery - PIXEL;
	line7.w = PIXEL;
	line7.h = PIXEL;

	line8.x = centerx + 3*PIXEL;
	line8.y = centery - 3*PIXEL; 
	line8.w = PIXEL;
	line8.h = 1*PIXEL;

	line9.x = centerx - 2*PIXEL;
	line9.y = centery + 2*PIXEL; 
	line9.w = PIXEL;
	line9.h = 1*PIXEL;

	line10.x = centerx - 3*PIXEL;
	line10.y = centery + 3*PIXEL; 
	line10.w = PIXEL;
	line10.h = 1*PIXEL;

	line11.x = centerx - 2*PIXEL;
	line11.y = centery - 2*PIXEL; 
	line11.w = PIXEL;
	line11.h = 1*PIXEL;

	line12.x = centerx - 3*PIXEL;
	line12.y = centery - 3*PIXEL; 
	line12.w = PIXEL;
	line12.h = 1*PIXEL;

	line13.x = 14*PIXEL + posX;
	line13.y = 5*PIXEL + posY;
	line13.w = PIXEL;
	line13.h = 0*PIXEL; 

	Render();
	SDL_Delay(4000);
}

void Render() {
	SDL_SetRenderDrawColor(renderer, 127,127,127,255);
	SDL_RenderClear(renderer);
	SDL_SetRenderDrawColor(renderer,255,255,0,255);
	
	SDL_RenderFillRect(renderer, &line1);
	SDL_RenderFillRect(renderer, &line2);
	SDL_RenderFillRect(renderer, &line3);
	SDL_RenderFillRect(renderer, &line4);
	SDL_RenderFillRect(renderer, &line5);
	SDL_RenderFillRect(renderer, &line6);
	SDL_RenderFillRect(renderer, &line7);
	SDL_RenderFillRect(renderer, &line8);
	SDL_RenderFillRect(renderer, &line9);
	SDL_RenderFillRect(renderer, &line10);
	SDL_RenderFillRect(renderer, &line11);
	SDL_RenderFillRect(renderer, &line12);
	SDL_RenderFillRect(renderer, &line13);

	
	SDL_RenderPresent(renderer);

}

bool InitEverything()
{
	if ( !InitSDL() )
		return false;

	if ( !CreateWindow() )
		return false;

	if ( !CreateRenderer() )
		return false;

	SetupRenderer();

	return true;
}
bool InitSDL()
{
	if ( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
	{
		std::cout << " Failed to initialize SDL : " << SDL_GetError() << std::endl;
		return false;
	}

	return true;
}
bool CreateWindow()
{
	window = SDL_CreateWindow( "Server", posX, posY, sizeX, sizeY, 0 );

	if ( window == NULL )
	{
		std::cout << "Failed to create window : " << SDL_GetError();
		return false;
	}

	return true;
}
bool CreateRenderer()
{
	renderer = SDL_CreateRenderer( window, -1, 0 );

	if ( renderer == NULL )
	{
		std::cout << "Failed to create renderer : " << SDL_GetError();
		return false;
	}

	return true;
}
void SetupRenderer()
{
	// Set size of renderer to the same as window
		SDL_RenderSetLogicalSize( renderer, sizeX, sizeY );
	// Set color of renderer to black
		SDL_SetRenderDrawColor( renderer, 0, 0, 0, 255 );
}
