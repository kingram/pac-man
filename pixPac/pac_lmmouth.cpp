#include <SDL2/SDL.h>
#include <iostream>

#define PIXEL 5

int posX = 300;
int posY= 300;
int sizeX = 600;
int sizeY = 600;

SDL_Window* window;
SDL_Renderer* renderer;

bool InitEverything();
bool InitSDL();
bool CreateWindow();
bool CreateRenderer();
void SetupRenderer();

void Render();

SDL_Rect line1,line2,line3,line4,line5,line6,line7,line8,line9,line10;
SDL_Rect line11,line12,line13;

int main(int argc, char* args[] ) {
	if (!InitEverything() ) {
		return -1;
	}

	line1.x = 8*PIXEL + posX;
	line1.y = PIXEL + posY;
	line1.w = -5*PIXEL;
	line1.h = PIXEL;

	line2.x = 10*PIXEL + posX;
	line2.y = 2*PIXEL + posY;
	line2.w = -9*PIXEL;
	line2.h = PIXEL;

	line3.x = 11*PIXEL + posX;
	line3.y = 3*PIXEL + posY;
	line3.w = -11*PIXEL;
	line3.h = PIXEL;

	line4.x = 11*PIXEL + posX;
	line4.y = 4*PIXEL + posY;
	line4.w = -11*PIXEL;
	line4.h = PIXEL;

	line5.x = 12*PIXEL + posX;
	line5.y = 5*PIXEL + posY;
	line5.w = -12*PIXEL;
	line5.h = PIXEL;

	line6.x = 12*PIXEL + posX;
	line6.y = 6*PIXEL + posY;
	line6.w = -10*PIXEL;
	line6.h = PIXEL;

	line7.x = 12*PIXEL + posX;
	line7.y = 7*PIXEL + posY;
	line7.w = -4*PIXEL;
	line7.h = PIXEL;

	line8.x = 12*PIXEL + posX;
	line8.y = 8*PIXEL + posY;
	line8.w = -10*PIXEL;
	line8.h = PIXEL;

	line9.x = 12*PIXEL + posX;
	line9.y = 9*PIXEL + posY;
	line9.w = -12*PIXEL;
	line9.h = PIXEL;

	line10.x = 11*PIXEL + posX;
	line10.y = 10*PIXEL + posY;
	line10.w = -11*PIXEL;
	line10.h = PIXEL;

	line11.x = 11*PIXEL + posX;
	line11.y = 11*PIXEL + posY;
	line11.w = -11*PIXEL;
	line11.h = PIXEL;

	line12.x = 10*PIXEL + posX;
	line12.y = 12*PIXEL + posY;
	line12.w = -9*PIXEL;
	line12.h = PIXEL;

	line13.x = 8*PIXEL + posX;
	line13.y = 13*PIXEL + posY;
	line13.w = -5*PIXEL;
	line13.h = PIXEL;

	Render();
	SDL_Delay(4000);
}

void Render() {
	SDL_SetRenderDrawColor(renderer, 127,127,127,255);
	SDL_RenderClear(renderer);
	SDL_SetRenderDrawColor(renderer,255,255,0,255);
	
	SDL_RenderFillRect(renderer, &line1);
	SDL_RenderFillRect(renderer, &line2);
	SDL_RenderFillRect(renderer, &line3);
	SDL_RenderFillRect(renderer, &line4);
	SDL_RenderFillRect(renderer, &line5);
	SDL_RenderFillRect(renderer, &line6);
	SDL_RenderFillRect(renderer, &line7);
	SDL_RenderFillRect(renderer, &line8);
	SDL_RenderFillRect(renderer, &line9);
	SDL_RenderFillRect(renderer, &line10);
	SDL_RenderFillRect(renderer, &line11);
	SDL_RenderFillRect(renderer, &line12);
	SDL_RenderFillRect(renderer, &line13);

	
	SDL_RenderPresent(renderer);

}

bool InitEverything()
{
	if ( !InitSDL() )
		return false;

	if ( !CreateWindow() )
		return false;

	if ( !CreateRenderer() )
		return false;

	SetupRenderer();

	return true;
}
bool InitSDL()
{
	if ( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
	{
		std::cout << " Failed to initialize SDL : " << SDL_GetError() << std::endl;
		return false;
	}

	return true;
}
bool CreateWindow()
{
	window = SDL_CreateWindow( "Server", posX, posY, sizeX, sizeY, 0 );

	if ( window == NULL )
	{
		std::cout << "Failed to create window : " << SDL_GetError();
		return false;
	}

	return true;
}
bool CreateRenderer()
{
	renderer = SDL_CreateRenderer( window, -1, 0 );

	if ( renderer == NULL )
	{
		std::cout << "Failed to create renderer : " << SDL_GetError();
		return false;
	}

	return true;
}
void SetupRenderer()
{
	// Set size of renderer to the same as window
		SDL_RenderSetLogicalSize( renderer, sizeX, sizeY );
	// Set color of renderer to black
		SDL_SetRenderDrawColor( renderer, 0, 0, 0, 255 );
}
