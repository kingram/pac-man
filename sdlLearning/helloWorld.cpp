#include "SDL/SDL.h"

int main(int argc, char* args[]) {
	// The Images
	SDL_Surface* hello = NULL;
	SDL_Surface* screen = NULL;
	
	// Start SDL
	SDL_Init(SDL_INIT_EVERYTHING);

	// Set Up Screen
	screen = SDL_SetVideoMode(640, 480, 32, SDL_SWSURFACE);
		// 640 wide, 480 high, 32 bits/pixel
	
	// Load Image
	hello = SDL_LoadBMP("hello.bmp");

	// Apply Image to Screen
	SDL_BlitSurface(hello, NULL, screen, NULL);

	// Update Screen
	SDL_Flip(screen);
	
	// Pause
	SDL_Delay(2000);

	// Free the Loaded Images
	SDL_FreeSurface(hello);

	// Quit SDL
	SDL_Quit();

	return 0;
}
