#include "SDL/SDL.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>

bool loadMedia();

SDL_Surface* loadSurface( std::string path ) {
	//Load image at specified path 
	SDL_Surface* loadedSurface = SDL_LoadBMP( path.c_str() ); 
	if( loadedSurface == NULL ) { 
		printf( "Unable to load image %s! SDL Error: %s\n", path.c_str(), SDL_GetError() ); 
	} 
	
	return loadedSurface; 
}

int main(int argc, char* args[]) {
	SDL_Surface* hello = NULL;
	SDL_Surface* screen = NULL;

	SDL_Init(SDL_INIT_EVERYTHING);

	screen = SDL_SetVideoMode(640,480,32,SDL_SWSURFACE);

	hello = SDL_LoadBMP("hello.bmp");

	bool quit = false;
	SDL_Event e;

	while (!quit) {
		while (SDL_PollEvent( &e) != 0) {
			if (e.type == SDL_QUIT) {
				quit = true;
			} else if (e.type == SDL_KEYDOWN ) {
				switch (e.key.keysym.sym) {

					case SDLK_UP:
					hello = SDL_LoadBMP("up.bmp");
					break;

					case SDLK_DOWN:
					hello = SDL_LoadBMP("down.bmp");
					break;

					case SDLK_LEFT:
					hello = SDL_LoadBMP("left.bmp");
					break;

					case SDLK_RIGHT:
					hello = SDL_LoadBMP("right.bmp");
					break;

					default:
					hello = SDL_LoadBMP("hello.bmp");
					break;
 
	
				}
			}
		}

		SDL_BlitSurface(hello, NULL, screen, NULL);

		SDL_Flip(screen);
	}

	SDL_FreeSurface(hello);

	SDL_Quit();

	return 0;
}
