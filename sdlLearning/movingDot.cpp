#include "SDL/SDL.h"

/* Global Variables */

// Size of the window
const int SCR_WIDTH = 640, SCR_HEIGHT = 480, BPP = 32;

// Surfaces and Dot Speed
SDL_Surface *screen = NULL, *dot = NULL;
int DOT_SPEED = 5;

SDL_Event event;

/* Functions */

bool init() {
	// Initilize SDL, return false if it failed.
	if (SDL_Init(SDL_INIT_EVERYTHING) == -1)
		return false;
	
	// Generate screen, return false if it failed.
	screen = SDL_SetVideoMode(SCR_WIDTH, SCR_HEIGHT, BPP, SDL_SWSURFACE);
	if (screen == NULL)
		return false;
	
	SDL_WM_SetCaption("Moving Dot", NULL);

	dot = SDL_LoadBMP("dot.bmp");

	return true;
}

void apply_surface(int x, int y, SDL_Surface* source, SDL_Surface* destination = NULL) {
	// Generate temporary rectangle and give offsets to rectangle.
	SDL_Rect offset;
	offset.x = x;
	offset.y = y;

	// Blit the surface.
	SDL_BlitSurface( source, NULL, destination, &offset);
}

/* Dot Class */
class Dot {
	public:
		// Constructor
		Dot();

		void handle_input();
		void move();
		void show();

	private:
		int x, y; // Location of the dot.
		int dx, dy; // Speed of the dot.
};

Dot::Dot() { // Initilize the dot (Places dot in upper left corner).
	x = 0;
	y = 0;
	dx = 0;
	dy = 0;
}

void Dot::handle_input() {
	if(event.type == SDL_KEYDOWN) {
		// Changes the velocity when the key is pressed.
		switch(event.key.keysym.sym) {
			case SDLK_UP:
				dy -= DOT_SPEED;
				break;
			case SDLK_DOWN:
				dy += DOT_SPEED;
				break;
			case SDLK_LEFT:
				dx -= DOT_SPEED;
				break;
			case SDLK_RIGHT:
				dx += DOT_SPEED;
				break;
		}
	} else if (event.type == SDL_KEYUP) {
		switch(event.key.keysym.sym) {
			case SDLK_UP:
				dy += DOT_SPEED;
				break;
			case SDLK_DOWN:
				dy -= DOT_SPEED;
				break;
			case SDLK_LEFT:
				dx += DOT_SPEED;
				break;
			case SDLK_RIGHT:
				dx -= DOT_SPEED;
				break;
		}
	}
}

void Dot::move() {
	x += dx;
	y += dy;
}

void Dot::show() {
	apply_surface(x, y, dot, screen);
}

int main(int argc, char* args[]) {
	bool quit = false;
	Dot dt;
	if(!init())
		return 1;

	while(!quit) {
		while(SDL_PollEvent(&event) != 0) {
			// Runs until user quits the program.
			if(event.type == SDL_QUIT)
				quit = true;

			dt.handle_input();
		}
		
		dt.move();

		SDL_FillRect(screen, &screen->clip_rect, SDL_MapRGB(screen->format, 0xFF, 0xFF, 0xFF));

		dt.show();

		if(SDL_Flip(screen) == -1)
			return 1;
	}

	return 0;
}
