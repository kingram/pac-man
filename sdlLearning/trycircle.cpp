#include <SDL2/SDL.h>
#include <iostream>
using namespace std;

#define WIDTH 500
#define HEIGHT 500

SDL_Window* window;
SDL_Renderer* renderer;

void waitUntilQuit();
void init();
void quit();

int main() {
	init();

	SDL_SetRenderDrawColor(renderer, 127, 127, 127, 255); //renderer, r,g,b,alpha
	SDL_RenderClear(renderer);
	
	SDL_SetRenderDrawColor(renderer,255,255,0,255);

	int x0 = WIDTH/2;
	int y0 = HEIGHT/2;
	int y = 0;
	int radius = 80;
	int x;
	int err = 0;

	for (int i = radius; i> 0; i--) {
		x = i;
		y = 0;
		err = 0;
		while (x>=y) {
			SDL_RenderDrawLine(renderer, x0,y0,x0+x,y0+y);
			SDL_RenderDrawLine(renderer, x0,y0,x0+y,y0+x);
			SDL_RenderDrawLine(renderer, x0,y0,x0-y,y0+x);
			SDL_RenderDrawLine(renderer, x0,y0,x0-x,y0+y);
			SDL_RenderDrawLine(renderer, x0,y0,x0-x,y0-y);
			SDL_RenderDrawLine(renderer, x0,y0,x0-y,y0-x);
			SDL_RenderDrawLine(renderer, x0,y0,x0+y,y0-x);
			SDL_RenderDrawLine(renderer, x0,y0,x0+x,y0-y);

			SDL_SetRenderDrawColor(renderer, 127, 127, 127, 255);
			SDL_RenderDrawLine(renderer, x0,y0,x0+x,y0-y);
			SDL_RenderDrawLine(renderer, x0,y0,x0+x,y0+y);
			SDL_SetRenderDrawColor(renderer,255,255,0,255);
			
			if (err <= 0) {
				y += 1;
				err += 2*y + 1;
			} 
			if (err > 0) {
				x -= 1;
				err -= 2*x + 1;
			}
			

		}
	}	

//	SDL_RenderDrawPoint(renderer, x,y);

	//make it all visible
	SDL_RenderPresent(renderer);

	waitUntilQuit();
	quit();
	return 0;
}

void init() {
	SDL_Init(SDL_INIT_VIDEO);
	window = SDL_CreateWindow("circle", 0,0,WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

}

void quit() {
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
}

void waitUntilQuit() {
	bool running = true;
	while(running) {
		SDL_Event* event = new SDL_Event;
		while(SDL_PollEvent(event) != 0) {
			if (event->type == SDL_QUIT) {
				running = false;
				break;
			}
		}

	}

}
