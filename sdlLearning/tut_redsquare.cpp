#include <SDL2/SDL.h>

#define WIDTH 500
#define HEIGHT 500

SDL_Window* window;
SDL_Renderer* renderer;

void waitUntilQuit();
void init();
void quit();

int main() {
	init();

	SDL_SetRenderDrawColor(renderer, 127, 127, 127, 255); //renderer, r,g,b,alpha
	SDL_RenderClear(renderer);
	
	//Draw a Square
	SDL_SetRenderDrawColor(renderer, 255,0,0,255);
	SDL_Rect* rect = new SDL_Rect;
	rect->x = WIDTH/4;
	rect->y = HEIGHT/4;
	rect->w = WIDTH/2;
	rect->h = HEIGHT/2;
	SDL_RenderFillRect(renderer, rect);

	SDL_RenderPresent(renderer);

	waitUntilQuit();
	quit();
	return 0;
}

void init() {
	SDL_Init(SDL_INIT_VIDEO);
	window = SDL_CreateWindow("yooo", 0,0,WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

}

void quit() {
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
}

void waitUntilQuit() {
	bool running = true;
	while(running) {
		SDL_Event *event = new SDL_Event;
		while(SDL_PollEvent(event) != 0) {
			if (event-> type == SDL_QUIT) {
				running = false;
				break;
			}
		}

	}

}
